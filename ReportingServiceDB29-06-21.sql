USE [master]
GO
/****** Object:  Database [ReportingServiceLive]    Script Date: 6/29/2021 18:30:51 ******/
CREATE DATABASE [ReportingServiceLive]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ReportingServiceLive', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\ReportingServiceLive.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ReportingServiceLive_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\ReportingServiceLive_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [ReportingServiceLive] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ReportingServiceLive].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ReportingServiceLive] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET ARITHABORT OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ReportingServiceLive] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ReportingServiceLive] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ReportingServiceLive] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ReportingServiceLive] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET RECOVERY FULL 
GO
ALTER DATABASE [ReportingServiceLive] SET  MULTI_USER 
GO
ALTER DATABASE [ReportingServiceLive] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ReportingServiceLive] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ReportingServiceLive] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ReportingServiceLive] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ReportingServiceLive] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ReportingServiceLive', N'ON'
GO
ALTER DATABASE [ReportingServiceLive] SET QUERY_STORE = OFF
GO
USE [ReportingServiceLive]
GO
/****** Object:  User [user_reportingService_Live]    Script Date: 6/29/2021 18:30:54 ******/
CREATE USER [user_reportingService_Live] FOR LOGIN [user_reportingService_Live] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [rs]    Script Date: 6/29/2021 18:30:54 ******/
CREATE USER [rs] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[STRING_SPLIT]    Script Date: 6/29/2021 18:30:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[STRING_SPLIT]
(    
      @Input NVARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Item NVARCHAR(1000)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Item)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END

GO
/****** Object:  Table [dbo].[Game]    Script Date: 6/29/2021 18:30:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Game](
	[GameID] [int] IDENTITY(1,1) NOT NULL,
	[GameCode] [nchar](8) NULL,
	[Name] [varchar](50) NULL,
	[ImagePath] [nvarchar](500) NULL,
	[Description] [varchar](1500) NULL,
	[Rules] [varchar](max) NULL,
	[Slug] [varchar](150) NULL,
	[GameStartSec] [int] NULL,
	[TimePerCardSec] [int] NULL,
	[StreamingUrl] [varchar](500) NULL,
	[SocketServerUrl] [varchar](150) NULL,
	[GameRates] [varchar](1200) NULL,
	[IsLive] [bit] NOT NULL,
	[IsStop] [bit] NOT NULL,
	[IsActive] [bit] NULL,
	[IsDelete] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ActiveDealerId] [int] NULL,
	[DisplayOrder] [smallint] NULL,
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[GameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[login]    Script Date: 6/29/2021 18:30:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[login](
	[userid] [bigint] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](max) NULL,
	[userpassword] [nvarchar](max) NULL,
	[usertype] [int] NULL,
	[isactive] [bit] NULL,
	[isdelete] [bit] NULL,
	[createdby] [bigint] NULL,
	[createdbydatetime] [datetime] NULL,
	[modifiedby] [bigint] NULL,
	[modifiedbydatetime] [datetime] NULL,
 CONSTRAINT [PK_login] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PageInfo]    Script Date: 6/29/2021 18:30:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageInfo](
	[Pid] [int] IDENTITY(1,1) NOT NULL,
	[Pname] [varchar](100) NULL,
	[PViewname] [varchar](100) NULL,
 CONSTRAINT [PK_PageInfo] PRIMARY KEY CLUSTERED 
(
	[Pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[platform]    Script Date: 6/29/2021 18:30:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[platform](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[isdelete] [bit] NULL,
	[LinkServerName] [varchar](100) NULL,
	[DBName] [varchar](100) NULL,
 CONSTRAINT [PK__platform__3213E83F0D8BBC8F] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SatSportPlatform]    Script Date: 6/29/2021 18:30:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SatSportPlatform](
	[SSID] [int] IDENTITY(1,1) NOT NULL,
	[SSName] [varchar](100) NULL,
	[SSLinkServerName] [varchar](100) NULL,
	[SSDBName] [varchar](100) NULL,
 CONSTRAINT [PK_SatSportPlatform] PRIMARY KEY CLUSTERED 
(
	[SSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserPermission]    Script Date: 6/29/2021 18:30:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPermission](
	[UpId] [int] IDENTITY(1,1) NOT NULL,
	[PageIds] [varchar](max) NULL,
	[UserId] [bigint] NULL,
 CONSTRAINT [PK_UserPermission] PRIMARY KEY CLUSTERED 
(
	[UpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[login] ON 

INSERT [dbo].[login] ([userid], [username], [userpassword], [usertype], [isactive], [isdelete], [createdby], [createdbydatetime], [modifiedby], [modifiedbydatetime]) VALUES (1, N'admin', N'b3gyd3RzbXo0N3BWQ2hpcXRkOHZTUT09', 1, 1, 0, 1, CAST(N'2021-05-25T11:52:42.040' AS DateTime), 1, CAST(N'2021-06-23T13:37:50.783' AS DateTime))
INSERT [dbo].[login] ([userid], [username], [userpassword], [usertype], [isactive], [isdelete], [createdby], [createdbydatetime], [modifiedby], [modifiedbydatetime]) VALUES (10, N'jt', N'bHVpbWdEMWtYUHlTSUNMdXFPWVJCQT09', 1, 1, 0, 1, CAST(N'2021-06-26T19:00:19.543' AS DateTime), 1, CAST(N'2021-06-26T19:00:19.543' AS DateTime))
INSERT [dbo].[login] ([userid], [username], [userpassword], [usertype], [isactive], [isdelete], [createdby], [createdbydatetime], [modifiedby], [modifiedbydatetime]) VALUES (11, N'as', N'VU5ZRzdzQWRoeE1LdVZuUU9ZSk5Mdz09', 1, 1, 0, 1, CAST(N'2021-06-26T19:01:32.780' AS DateTime), 1, CAST(N'2021-06-26T19:01:32.780' AS DateTime))
INSERT [dbo].[login] ([userid], [username], [userpassword], [usertype], [isactive], [isdelete], [createdby], [createdbydatetime], [modifiedby], [modifiedbydatetime]) VALUES (12, N'ak', N'M2dXT09ZVk9zeW4zWjRzUDhPZ3JFdz09', 1, 1, 0, 1, CAST(N'2021-06-26T19:39:01.247' AS DateTime), 1, CAST(N'2021-06-26T19:39:01.247' AS DateTime))
INSERT [dbo].[login] ([userid], [username], [userpassword], [usertype], [isactive], [isdelete], [createdby], [createdbydatetime], [modifiedby], [modifiedbydatetime]) VALUES (13, N'ig', N'YnNuNnNia1VZNk11Tmd4SGt6SC9NUT09', 1, 1, 0, 1, CAST(N'2021-06-26T19:57:37.467' AS DateTime), 1, CAST(N'2021-06-26T19:57:37.467' AS DateTime))
SET IDENTITY_INSERT [dbo].[login] OFF
GO
SET IDENTITY_INSERT [dbo].[PageInfo] ON 

INSERT [dbo].[PageInfo] ([Pid], [Pname], [PViewname]) VALUES (1, N'Card View', N'Home/Index')
INSERT [dbo].[PageInfo] ([Pid], [Pname], [PViewname]) VALUES (2, N'Linechart Dashboard', N'Home/SecondDashboard')
INSERT [dbo].[PageInfo] ([Pid], [Pname], [PViewname]) VALUES (3, N'Piechart Dashboard', N'Home/ThirdDashboard')
INSERT [dbo].[PageInfo] ([Pid], [Pname], [PViewname]) VALUES (4, N'Agent Wise PL', N'Home/AgentPLReport')
INSERT [dbo].[PageInfo] ([Pid], [Pname], [PViewname]) VALUES (5, N'SatSport Running Analysis', N'RunningAnalysis/SatSport')
INSERT [dbo].[PageInfo] ([Pid], [Pname], [PViewname]) VALUES (6, N'Manage User', N'Home/AddUser')
INSERT [dbo].[PageInfo] ([Pid], [Pname], [PViewname]) VALUES (7, N'Manage User Role', N'Home/AddUserRole')
INSERT [dbo].[PageInfo] ([Pid], [Pname], [PViewname]) VALUES (8, N'Withdrawal Request', N'Home/WithdrawalRequest')
SET IDENTITY_INSERT [dbo].[PageInfo] OFF
GO
SET IDENTITY_INSERT [dbo].[platform] ON 

INSERT [dbo].[platform] ([id], [name], [isdelete], [LinkServerName], [DBName]) VALUES (1, N'Supernowa Dealer', 0, N'LNK_Veronica_live', N'Veronica_Live')
INSERT [dbo].[platform] ([id], [name], [isdelete], [LinkServerName], [DBName]) VALUES (2, N'Supernowa Virtual', 0, N'LNK_SuperNowa_Virtual', N'Supernowa_Virtual')
INSERT [dbo].[platform] ([id], [name], [isdelete], [LinkServerName], [DBName]) VALUES (3, N'Power Games', 0, NULL, NULL)
INSERT [dbo].[platform] ([id], [name], [isdelete], [LinkServerName], [DBName]) VALUES (4, N'Satsport 247', 1, NULL, NULL)
INSERT [dbo].[platform] ([id], [name], [isdelete], [LinkServerName], [DBName]) VALUES (5, N'Binary', 1, NULL, NULL)
INSERT [dbo].[platform] ([id], [name], [isdelete], [LinkServerName], [DBName]) VALUES (6, N'King Ratan', 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[platform] OFF
GO
SET IDENTITY_INSERT [dbo].[SatSportPlatform] ON 

INSERT [dbo].[SatSportPlatform] ([SSID], [SSName], [SSLinkServerName], [SSDBName]) VALUES (1, N'SS247', N'LNK_SATSPORT247', N'Satsport247_Live')
INSERT [dbo].[SatSportPlatform] ([SSID], [SSName], [SSLinkServerName], [SSDBName]) VALUES (2, N'MPCsport', N'LNK_MPCSPORT', N'MPC_Sport')
INSERT [dbo].[SatSportPlatform] ([SSID], [SSName], [SSLinkServerName], [SSDBName]) VALUES (3, N'MPC555', N'LNK_MPC555', N'MPC555')
INSERT [dbo].[SatSportPlatform] ([SSID], [SSName], [SSLinkServerName], [SSDBName]) VALUES (4, N'Jeeto365', NULL, NULL)
INSERT [dbo].[SatSportPlatform] ([SSID], [SSName], [SSLinkServerName], [SSDBName]) VALUES (5, N'BookRadar', N'LNK_BOOKRADAR', N'BookRadar_Live')
INSERT [dbo].[SatSportPlatform] ([SSID], [SSName], [SSLinkServerName], [SSDBName]) VALUES (6, N'Kite', NULL, NULL)
SET IDENTITY_INSERT [dbo].[SatSportPlatform] OFF
GO
SET IDENTITY_INSERT [dbo].[UserPermission] ON 

INSERT [dbo].[UserPermission] ([UpId], [PageIds], [UserId]) VALUES (1, N'1,2,3,4,5,6,7,8', 1)
INSERT [dbo].[UserPermission] ([UpId], [PageIds], [UserId]) VALUES (6, N'1,2,3,4', 10)
INSERT [dbo].[UserPermission] ([UpId], [PageIds], [UserId]) VALUES (7, N'1,2,3,4', 11)
INSERT [dbo].[UserPermission] ([UpId], [PageIds], [UserId]) VALUES (8, N'1,2,3,4', 12)
INSERT [dbo].[UserPermission] ([UpId], [PageIds], [UserId]) VALUES (9, N'1,2,3,4', 13)
SET IDENTITY_INSERT [dbo].[UserPermission] OFF
GO
ALTER TABLE [dbo].[Game] ADD  CONSTRAINT [DF_Game_IsLive]  DEFAULT ((0)) FOR [IsLive]
GO
ALTER TABLE [dbo].[Game] ADD  CONSTRAINT [DF_Game_IsStop]  DEFAULT ((0)) FOR [IsStop]
GO
ALTER TABLE [dbo].[Game] ADD  CONSTRAINT [DF_Game_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Game] ADD  CONSTRAINT [DF_Game_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_login] FOREIGN KEY([UserId])
REFERENCES [dbo].[login] ([userid])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_login]
GO
/****** Object:  StoredProcedure [dbo].[Get_AgentProfitLoss_DateWise]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author      : Mitesh Chuahan  
-- Create date : 2021 March, 09  
-- Description : sp_getagentwiseprofitloss  
-- TEST        : [dbo].[Get_AgentProfitLoss_DateWise]		@PlatFormname = N'[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]',		@GameId = 1,		@AgentUserID = 5062,		@FromDate = N'2021-01-01',		@ToDate = N'2021-04-01'
-- =============================================  
CREATE PROCEDURE [dbo].[Get_AgentProfitLoss_DateWise]
	@PlatFormname varchar(max),
    @GameId INT = NULL,
    @AgentUserID AS INT = NULL,
    @Search VARCHAR(250) = NULL,
    @FromDate DATETIME,
    @ToDate DATETIME
AS
SET NOCOUNT ON;

BEGIN
    IF @Search = ''
    BEGIN
        SET @Search = NULL;
    END;

    DECLARE @lstRec TABLE
    (
        UserID INT,
        AgentName VARCHAR(160),
        ProfitLoss NUMERIC(18, 2),
        recordCnt INT
    );
	
	-- Declare Platform Table
	DECLARE @Plateform_Table TABLE  (id INT identity(1,1),Platformname  NVARCHAR(1000)) 

	-- Insert statement
	INSERT INTO @Plateform_Table
	SELECT * FROM  STRING_SPLIT(@platformname, ',')  
	----QUERY-----
	DECLARE  @query NVARCHAR(max)='';
	DECLARE @cnt INT = 0 ;
	SET @cnt =(SELECT COUNT(1) FROM @Plateform_Table)

	while(@cnt > 0)
	BEGIN
		DECLARE @DBPLATFORM VARCHAR(MAX)=(select Platformname from @Plateform_Table where ID=@cnt)
		IF @query <> ''
		BEGIN
			SET @query = @query + ' UNION ALL ';
		END		
				
		SET @query =@query  + ' SELECT tblSettle.UserID,
           tblUser.Username AS AgentName,
           SUM(ISNULL(tblSettle.Debit_Dena, 0) - ISNULL(tblSettle.Credit_Lena, 0)) AS PROFITLOSS,
           1 AS REC
    FROM '+@DBPLATFORM+'.[tblSettlement] tblSettle
        INNER JOIN '+@DBPLATFORM+'.[User] tblUser ON tblUser.UserID = tblSettle.UserID
        INNER JOIN'+@DBPLATFORM+'.[RoundSummary] tblRound ON tblRound.RoundSummaryID = tblSettle.RoundSummaryID
    WHERE tblUser.RoleID = 3
          AND tblRound.CreatedDate BETWEEN '+cast(CONVERT(datetime,@FromDate,101) as varchar)+ ' AND '+cast(CONVERT(datetime,@ToDate,101) as varchar)+ '
          AND (''+ cast(@GameId AS VARCHAR) +'' IS NULL OR tblRound.GameID = ''+ cast(@GameId AS VARCHAR) +'')
          AND (''+ cast(@AgentUserID AS VARCHAR) +'' IS NULL OR tblUser.UserID = ''+ cast(@AgentUserID AS VARCHAR) +'')
          AND tblRound.IsDelete = 0
          AND tblRound.IsActive = 1
          AND tblRound.IsSettled = 1
          AND tblSettle.IsActive = 1
          AND tblSettle.IsDelete = 0
    GROUP BY tblSettle.UserID,
             tblUser.Username,
             tblSettle.IsActive';
	
		SET @cnt  = @cnt  - 1

	END    

	DECLARE @FINAL_QUERY NVARCHAR(MAX)=''
	SET @FINAL_QUERY= 'SELECT RESULT.UserID,
            RESULT.AgentName,
          RESULT.PROFITLOSS,
          RESULT.REC FROM ('+@query+') AS RESULT'
Print @FINAL_QUERY ;
	EXEC (@FINAL_QUERY)
 
END;
GO
/****** Object:  StoredProcedure [dbo].[Get_AgentProfitLoss_DateWise_V1]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author      : Mitesh Chuahan  
-- Create date : 2021 March, 09  
-- Description : sp_getagentwiseprofitloss  
-- TEST        : [dbo].[Get_AgentProfitLoss_DateWise]		@PlatFormname = N'[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]',		@GameId = 1,		@AgentUserID = 5062,		@FromDate = N'2021-01-01',		@ToDate = N'2021-04-01'
-- =============================================  
CREATE   PROCEDURE [dbo].[Get_AgentProfitLoss_DateWise_V1]
	@PlatFormname varchar(max),
    @GameId INT = NULL,
    @AgentUserID AS INT = NULL,
    @Search VARCHAR(250) = NULL,
    @FromDate DATETIME,
    @ToDate DATETIME
AS
SET NOCOUNT ON;

BEGIN
    IF @Search = ''
    BEGIN
        SET @Search = NULL;
    END;

    --DECLARE @lstRec TABLE
    --(
    --    UserID INT,
    --    AgentName VARCHAR(160),
    --    ProfitLoss NUMERIC(18, 2),
    --    recordCnt INT
    --);
	
	-- Declare Platform Table
	DECLARE @Plateform_Table TABLE  (id INT identity(1,1),Platformname  NVARCHAR(1000)) 

	-- Insert statement
	INSERT INTO @Plateform_Table
	SELECT * FROM  STRING_SPLIT(@platformname, ',')  
	----QUERY-----
	DECLARE  @query NVARCHAR(max)='';
	DECLARE @cnt INT = 0 ;
	SET @cnt =(SELECT COUNT(1) FROM @Plateform_Table)

	WHILE(@cnt > 0)
	BEGIN
		DECLARE @DBPLATFORM VARCHAR(MAX)= '';

		SELECT @DBPLATFORM = '[' + LinkServerName + '].[' +DBName+'].[dbo]'
		FROM platform 
		where LinkServerName = ( select @platformname from @Plateform_Table where ID=@cnt)

		IF @query <> ''
		BEGIN
			SET @query = @query + ' UNION ALL ';
		END		
				
		SET @query = @query  + ' SELECT tblSettle.UserID,
									    tblUser.Username AS AgentName,
									    SUM(ISNULL(tblSettle.Debit_Dena, 0) - ISNULL(tblSettle.Credit_Lena, 0)) AS PROFITLOSS,
									    1 AS REC
									FROM '+@DBPLATFORM+'.[tblSettlement] tblSettle
										INNER JOIN '+@DBPLATFORM+'.[User] tblUser ON tblUser.UserID = tblSettle.UserID
										INNER JOIN '+@DBPLATFORM+'.[RoundSummary] tblRound ON tblRound.RoundSummaryID = tblSettle.RoundSummaryID
									WHERE tblUser.RoleID = 3
										  AND tblRound.CreatedDate BETWEEN ''' + CAST(@FromDate as varchar) + ''' AND ''' + CAST(CONVERT(DATETIME,@ToDate,101) AS VARCHAR)+ '''
										  AND (''' + CAST(@GameId AS VARCHAR) + ''' IS NULL OR tblRound.GameID = '+ CAST(@GameId AS VARCHAR) +')
										  AND (''' + CAST(@AgentUserID AS VARCHAR) + ''' IS NULL OR tblUser.UserID = ' + CAST(@AgentUserID AS VARCHAR) +')
										  AND tblRound.IsDelete = 0
										  AND tblRound.IsActive = 1
										  AND tblRound.IsSettled = 1
										  AND tblSettle.IsActive = 1
										  AND tblSettle.IsDelete = 0
									GROUP BY tblSettle.UserID,
											 tblUser.Username,
											 tblSettle.IsActive ';
	
		SET @cnt  = @cnt  - 1

	END    

	DECLARE @FINAL_QUERY NVARCHAR(MAX)=''
	SET @FINAL_QUERY= 'SELECT RESULT.UserID,
							  RESULT.AgentName,
							  RESULT.PROFITLOSS,
							  RESULT.REC FROM (' + @query + ') AS RESULT'
	Print @FINAL_QUERY ;
	EXEC (@FINAL_QUERY)
 
END;
GO
/****** Object:  StoredProcedure [dbo].[GetBetListData]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author  	: Nishant Jariwala 
-- Create date	: 12-Jan-2020
-- Description	: Get BetList Data
-- Test Case	: EXEC GetBetListData 1,1,null,'2020-05-01','2020-10-08',null,null,null,null,null,null,null,null,null,null,null,1,50,null,0,0
-- =============================================
create PROCEDURE [dbo].[GetBetListData] -- Add the parameters for the stored procedure here 
    @Userid INT,
	@UsernameSearchText AS VARCHAR(100) = '',
    @UserType INT,
    @Date DATETIME,
	@StartDate    DATETIME,
	@EndDate      DATETIME,	
    @IsFancy BIT,
	@IsSettled BIT,
    @SportID INT,
    @TournamentID INT,
    @MatchID VARCHAR(100),
    @MarketID INT,
	@SuperAdminID INT,
	@AdminID INT,
	@SuperMasterID INT,
    @MasterID INT,
	@PlayerID INT,
    @Page INT,
    @Size INT,
    @IsBetListLive BIT,
    @BetFrom INT,
    @TotalCount INT OUTPUT,
	@OrderByColumn AS VARCHAR(25) = NULL,
	@OrderByDirection AS VARCHAR(5) = NULL,
	@SettledMarketTotalPL INT = 0 OUTPUT
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN
	BEGIN TRY
		IF(@UserType>6)
		BEGIN
			SELECT @UserType = UserType FROM dbo.[User] WHERE UserID = @Userid AND IsActive = 1 AND IsDelete = 0;
		END
		SET @OrderByDirection = IIF(ISNULL(@OrderByDirection,'') = '', 'ASC', @OrderByDirection);
		SET @OrderByColumn = IIF(ISNULL(@OrderByColumn,'') = '', 'Username', @OrderByColumn);

		DROP TABLE IF EXISTS #BetList;
		CREATE TABLE #BetList  (
			BetID INT NOT NULL,
			CreatedDate DATETIME NULL,
			IsMatchBet BIT NULL,
			BetTaken DATETIME NULL,
			Selection VARCHAR(100) NULL,
			IsBack BIT NULL,
			--Exposure DECIMAL(18,2) NULL,
			Run DECIMAL(5, 1) NULL,
			Rate DECIMAL(18,2) NULL,
			SuperAdminID INT NULL,
			AdminID INT NULL,
			SuperMasterID INT NULL,
			MasterID INT NULL,
			PlayerID INT NULL,
			Stake INT NULL,
			IsDelete BIT NULL,
			IsBetWon BIT NULL,
			BetPL VARCHAR(10) NULL,
			MarketName VARCHAR(100) NULL,
			IsFancy BIT NULL,
			IsSettled BIT NULL,
			SportName VARCHAR(100) NULL,
			MatchName VARCHAR(100) NULL,
			OpenDate DATETIME NULL,
			TournamentName VARCHAR(100) NULL,
			MarketID INT NULL,
			MarketType VARCHAR(50) NULL,
			CentralID INT NULL,
			MatchID VARCHAR(100) NULL,
			TournamentID INT NULL,
			SportID INT NULL,
			Winner VARCHAR(100) NULL,
			PlayerPoint INT NULL,
			INR DECIMAL(18,2) NULL,
			Users VARCHAR(500) NULL,
			Info VARCHAR(350)
		)

		INSERT INTO #BetList
		(
			BetID,
			CreatedDate,
			IsMatchBet,
			BetTaken,
			Selection,
			IsBack,
			--Exposure,
			Run,
			Rate,
			SuperAdminID,
			AdminID,
			SuperMasterID,
			MasterID,
			PlayerID,
			Stake,
			IsDelete,
			IsBetWon,
			BetPL,
			MarketName,
			IsFancy,
			IsSettled,
			SportName,
			MatchName,
			OpenDate,
			TournamentName,
			MarketID,
			MarketType,
			CentralID,
			MatchID,
			TournamentID,
			SportID,
			Info,
			Winner,
			PlayerPoint,
			INR,
			Users
		)
		SELECT 
			dum.BetID,
			dum.CreatedDate,
			dum.IsMatchBet,
			dum.BetTaken,
			dum.Selection,
			dum.IsBack,
			--dum.Exposure,
			dum.Run,
			dum.Rate,
			dum.SuperAdminID,
			dum.AdminID,
			dum.SuperMasterID,
			dum.MasterID,
			dum.PlayerID,
			dum.Stake,
			ISNULL(dum.IsDelete, 0),
			dum.IsBetWon,
			dum.BetPL,
			dum.MarketName,
			dum.IsFancy,
			dum.IsSettled,
			dum.SportName,
			dum.MatchName,
			dum.OpenDate,
			dum.TournamentName,
			dum.MarketID,
			dum.MarketType,
			dum.CentralID,
			dum.MatchID,
			dum.TournamentID,
			dum.SportID,
			dum.Info,
			dum.Winner,
			dum.PlayerPoint,
			dum.INR,
			dum.Users
		FROM
		(
			 SELECT  
					B.BetID,
					B.CreatedDate,
					'1' AS IsMatchBet,
               
					B.CreatedDate AS BetTaken,
					B.Runner AS Selection,
					B.IsBack,
					--userexposure.Exposure, 
					B.Run,
					B.Rate,
					B.SuperAdminID,
					B.AdminID,
					B.SuperMasterID,
					B.MasterID,
					B.PlayerID,
					B.Stake,
					B.IsDelete,
					B.IsBetWon,
					ISNULL(CAST(B.BetPL AS VARCHAR(10)), '0') AS BetPL,
					Market.Name AS MarketName,
					Market.IsFancy,
					Market.IsSettled,
					Sport.Name AS SportName,
					M.Name AS MatchName,
					M.OpenDate AS OpenDate,
					Tournament.Name AS TournamentName,
					Market.MarketID,
					Market.MarketType,
					market.CentralID,
					CAST(M.MatchID AS VARCHAR(100)) AS MatchID,
					Tournament.TournamentID,
					Sport.SportID,
					BI.Info,
					--dbo.GetResultOfMarket(Market.Result, Market.MarketType) AS Winner,
					IIF(ISNULL(market.Result, '') = '','Not Settled/ Not Declaired', IIF(market.Result = '-999','No Result/Cancel', IIF(market.Result = '-888','Match Tie',IIF(market.MarketType NOT IN ('advancesession','session', 'line'),ISNULL(Run.Name,'No Result'), market.Result)))) Winner,
					downline.PlayerPoint,
					(B.BetPL * downline.PlayerPoint) AS INR,
					--ISNULL(superadmin1.Username + '/', '') + ISNULL(admin1.Username + '/', '')
					--+ ISNULL(supermaster1.Username + '/', '') + ISNULL(master1.Username + '/', '')
					--+ ISNULL(player1.Username, '') AS Users
					(ISNULL(U.Username + '/','') + P.Username) AS Users
			 FROM dbo.Bet B
				 LEFT JOIN dbo.BetInfo BI ON BI.BetId = B.BetID AND BI.isUnmatchedBet=0
				 INNER JOIN dbo.Market ON B.MarketID = Market.MarketID
				 INNER JOIN dbo.[Match] M ON Market.MatchID = M.MatchID
				 INNER JOIN dbo.Tournament ON M.TournamentID = Tournament.TournamentID
				 INNER JOIN dbo.Sport ON Tournament.SportID = Sport.SportID
				 --LEFT JOIN [User] superadmin1 ON superadmin1.UserID = Bet.SuperAdminID
				 --LEFT JOIN [User] admin1 ON admin1.UserID = Bet.AdminID
				 --LEFT JOIN [User] supermaster1 ON supermaster1.UserID = Bet.SuperMasterID
				 --LEFT JOIN [User] master1 ON master1.UserID = Bet.MasterID
				 --LEFT JOIN [User] player1 ON player1.UserID = Bet.PlayerID
				 --LEFT JOIN Runner runner ON Market.Result = runner.BfRunnerID
				 INNER JOIN dbo.Downline downline ON B.PlayerID = downline.PlayerID
				 INNER JOIN dbo.[User] U ON U.UserID = ISNULL(downline.MasterID, ISNULL(downline.SuperMasterID, ISNULL(downline.AdminID, downline.SuperAdminID)))
				 INNER JOIN dbo.[User] P ON P.UserID = B.PlayerID
				 OUTER APPLY (SELECT TOP(1) R.Name,R.BfRunnerID  FROM dbo.Runner R WHERE R.BfRunnerID = Market.Result AND R.IsActive = 1  AND R.IsDelete = 0) Run
				 --LEFT JOIN UserExposure userexposure ON Bet.PlayerID = userexposure.UserId
			 WHERE (
					(@UserType = 1 AND B.OwnerID = @Userid)
					OR(@UserType = 2 AND B.SuperAdminID = @Userid)
					OR(@UserType = 3 AND B.AdminID = @Userid)
					OR(@UserType = 4 AND B.SuperMasterID = @Userid)
					OR(@UserType = 5 AND B.MasterID = @Userid)
					)
				   AND (@BetFrom = 0 OR @BetFrom = 1)
				  -- AND Bet.IsActive = 1
				   --AND Bet.IsDelete = 0
				   AND (@SuperAdminID IS NULL OR B.SuperAdminID = @SuperAdminID)
				   AND (@AdminID IS NULL OR B.AdminID = @AdminID)
				   AND (@SuperMasterID IS NULL OR B.SuperMasterID = @SuperMasterID)
				   AND (@MasterID IS NULL OR B.MasterID = @MasterID)
				   AND (@PlayerID IS NULL OR B.PlayerID = @PlayerID)
					AND (@SportID IS NULL OR Tournament.SportID = @SportID)
				   AND (@TournamentID IS NULL OR Tournament.TournamentID = @TournamentID)
				   AND (@MatchID IS NULL OR M.MatchID = (CASE WHEN ISNUMERIC(@MatchID) = 1 THEN @MatchID ELSE 0 END))
				   AND (@MarketID IS NULL OR Market.MarketID = @MarketID)
				   AND (B.CreatedDate >= @StartDate OR (@StartDate IS NULL OR @StartDate = ''))
				   AND (B.CreatedDate <= @EndDate OR (@EndDate IS NULL OR @EndDate=''))
				   AND ((B.IsDelete = 1 AND B.CreatedBy <> B.ModifiedBy) OR (B.IsDelete = 0 AND B.IsActive = 1))
				   AND(
						--Bet List which is placed in Live Market
						@IsBetListLive IS NULL
						OR
						(
							@IsBetListLive = 1
							AND (
									(Market.IsFancy = 0 AND  (ISNULL(Market.MarketStatus,1) IN (1,6))) 
									OR (ISNULL(Market.IsFancy,0) = 1 AND  (ISNULL(Market.MarketStatus,1) NOT IN (4,5)))
								)
							AND Market.MarketStatusForAdmin = 1
						 ) 
						 OR
						 --Bet List which is placed in past Market
						 (
							@IsBetListLive = 0
							AND (
									(Market.IsFancy = 0 AND  (ISNULL(Market.MarketStatus,1) NOT IN (1,6))) 
									OR (ISNULL(Market.IsFancy,0) = 1 AND  (ISNULL(Market.MarketStatus,1) IN (4,5)))
								)
							AND Market.MarketStatusForAdmin = 1
						 )
						)
				   AND (@Date IS NULL OR B.CreatedDate >= @Date)
				   AND (@IsFancy IS NULL OR Market.IsFancy = @IsFancy)
				   AND (@IsSettled IS NULL OR Market.IsSettled = @IsSettled)
				   AND (ISNULL(@UsernameSearchText,'') = '' 
						--OR (ISNULL(U.Username + '/','') + P.Username) LIKE '%' + @UsernameSearchText + '%'
						)
				   --AND Bet.MarketID=userexposure.MarketId
			UNION ALL
			SELECT 
					UB.UnMatchedBetID AS BetID,
					UB.CreatedDate,
					'0' AS IsMatchBet,
               
					UB.CreatedDate AS BetTaken,
					UB.Runner AS Selection,
					UB.IsBack,
					--userexposure.Exposure,
					0 AS Run,
					UB.Rate,
					UB.SuperAdminID,
					UB.AdminID,
					UB.SuperMasterID,
					UB.MasterID,
					UB.PlayerID,
					UB.Stake,
					UB.IsDelete,
					UB.IsBetWon,
					ISNULL(CAST(UB.BetPL AS VARCHAR(10)), '0') AS BetPL,
					Market.Name AS MarketName,
					Market.IsFancy,
					Market.IsSettled,
					Sport.Name AS SportName,
					M.Name AS MatchName,
					M.OpenDate AS OpenDate,
					Tournament.Name AS TournamentName,
					Market.MarketID,
					Market.MarketType,
					market.CentralID,
					CAST(M.MatchID AS VARCHAR(100)) AS MatchID,
					Tournament.TournamentID,
					Sport.SportID,
					BI.Info,
					--dbo.GetResultOfMarket(Market.Result, Market.MarketType) AS Winner--,
					IIF(ISNULL(market.Result, '') = '','Not Settled/ Not Declaired', IIF(market.Result = '-999','No Result/Cancel', IIF(market.Result = '-888','Match Tie',IIF(market.MarketType NOT IN ('advancesession','session', 'line'),ISNULL(Run.Name,'No Result'), market.Result)))) Winner,
					downline.PlayerPoint,
					(UB.BetPL * downline.PlayerPoint) AS INR,
					--ISNULL(superadmin1.Username + '/', '') + ISNULL(admin1.Username + '/', '')
					--+ ISNULL(supermaster1.Username + '/', '') + ISNULL(master1.Username + '/', '')
					--+ ISNULL(player1.Username, '') AS Users
					(ISNULL(U.Username + '/','') + P.Username) AS Users
			 FROM dbo.UnMatchedBet UB
				 LEFT JOIN dbo.BetInfo BI ON BI.BetId = UB.UnMatchedBetID AND BI.isUnmatchedBet=1
				 INNER JOIN dbo.Market ON UB.MarketID = Market.MarketID 
				 INNER JOIN dbo.[Match] M ON Market.MatchID = M.MatchID
				 INNER JOIN dbo.Tournament ON M.TournamentID = Tournament.TournamentID
				 INNER JOIN dbo.Sport ON Tournament.SportID = Sport.SportID
				 --LEFT JOIN [User] superadmin1 ON superadmin1.UserID = UB.SuperAdminID
				 --LEFT JOIN [User] admin1 ON admin1.UserID = UB.AdminID
				 --LEFT JOIN [User] supermaster1 ON supermaster1.UserID = UB.SuperMasterID
				 --LEFT JOIN [User] master1 ON master1.UserID = UB.MasterID
				 --LEFT JOIN [User] player1 ON player1.UserID = UB.PlayerID
				 --LEFT JOIN Runner runner ON Market.Result = runner.BfRunnerID
				 INNER JOIN dbo.Downline downline ON UB.PlayerID = downline.PlayerID
				 INNER JOIN dbo.[User] U ON U.UserID = ISNULL(downline.MasterID, ISNULL(downline.SuperMasterID, ISNULL(downline.AdminID, downline.SuperAdminID)))
				 INNER JOIN dbo.[User] P ON P.UserID = UB.PlayerID
				 OUTER APPLY (SELECT TOP(1) R.Name FROM dbo.Runner R WHERE R.BfRunnerID = Market.Result AND R.IsActive = 1  AND R.IsDelete = 0) Run
				  --LEFT JOIN UserExposure userexposure ON UnMatchedBet.PlayerID = userexposure.UserId
			 WHERE
				(
						   (@UserType = 1 AND UB.OwnerID = @Userid)
						   OR (@UserType = 2 AND UB.SuperAdminID = @Userid)
						   OR (@UserType = 3 AND UB.AdminID = @Userid)
						   OR (@UserType = 4 AND UB.SuperMasterID = @Userid)
						   OR (@UserType = 5 AND UB.MasterID = @Userid)
						)
				   AND 
				(@BetFrom = 0 OR @BetFrom = 2)
				AND (@SuperAdminID IS NULL OR UB.SuperAdminID = @SuperAdminID)
				   AND (@AdminID IS NULL OR UB.AdminID = @AdminID)
				   AND (@SuperMasterID IS NULL OR UB.SuperMasterID = @SuperMasterID)
				   AND (@MasterID IS NULL OR UB.MasterID = @MasterID)
				   AND (@PlayerID IS NULL OR UB.PlayerID = @PlayerID)
				   AND (@SportID IS NULL OR Tournament.SportID = @SportID)
				   AND (@TournamentID IS NULL OR Tournament.TournamentID = @TournamentID)
				   AND (@MatchID IS NULL OR M.MatchID = (CASE WHEN ISNUMERIC(@MatchID) = 1 THEN @MatchID ELSE 0 END))
				   AND (@MarketID IS NULL OR Market.MarketID = @MarketID)
				   AND (UB.CreatedDate >= @StartDate OR (@StartDate IS NULL OR @StartDate = ''))
				   AND (UB.CreatedDate <= @EndDate OR (@EndDate IS NULL OR @EndDate=''))
				   AND UB.IsActive = 1
				   AND UB.IsDelete = 0
				   AND (
						--Bet List which is placed in Live Market
						 @IsBetListLive IS NULL
						OR
						(
							@IsBetListLive = 0
							AND (
									(Market.IsFancy = 0 AND  (ISNULL(Market.MarketStatus,1) IN (1,6))) 
									OR (ISNULL(Market.IsFancy,0) = 1 AND  (ISNULL(Market.MarketStatus,1) NOT IN (4,5)))
								)
							AND Market.MarketStatusForAdmin = 1
						 ) 
						 OR
						 --Bet List which is placed in past Market
						 (
							@IsBetListLive = 1
							AND (
									(Market.IsFancy = 0 AND  (ISNULL(Market.MarketStatus,1) NOT IN (1,6))) 
									OR (ISNULL(Market.IsFancy,0) = 1 AND  (ISNULL(Market.MarketStatus,1) IN (4,5)))
								)
							AND Market.MarketStatusForAdmin = 1
						 )
						)
				   AND (@Date IS NULL OR UB.CreatedDate >= @Date)
				   AND (@IsFancy IS NULL OR Market.IsFancy = @IsFancy)
				   AND (@IsSettled IS NULL OR Market.IsSettled = @IsSettled)
				   AND (ISNULL(@UsernameSearchText,'') = '' 
						OR (ISNULL(U.Username + '/','') + P.Username) LIKE '%' + @UsernameSearchText + '%'
						)
					--AND UnMatchedBet.MarketID=userexposure.MarketId
			--UNION ALL
			--SELECT  DISTINCT
			--		EFT.ExchGameFundTransferId AS BetID,
			--		EFT.CreatedDate,
	  --              '1' AS IsMatchBet,
               
	  --              EFT.CreatedDate AS BetTaken,
	  --              EFT.Runnername AS Selection,
	  --              (CASE WHEN EFT.BetType = 'Back' THEN 1 ELSE 0 END) AS IsBack,
	  --              0 AS Exposure,
	  --              EFT.Rate,
	  --              superadmin.UserID AS SuperAdminID,
	  --              [admin].UserID AS AdminID,
	  --              supermaster.UserID AS SuperMasterID,
	  --              [master].UserID AS MasterID,
	  --              player.UserID AS PlayerID,
	  --              EFT.Stake,
	  --              0 AS IsBetWon,
	  --              '-' AS BetPL,
	  --              'Plays' AS MarketName,
	  --              0 AS IsFancy,
	  --              0 AS IsSettled,
	  --              'ExchGame' AS SportName,
	  --              ('Round' + CAST(EFT.RoundID AS VARCHAR(50))) AS MatchName,
			--		null AS OpenDate,
	  --              'VeronicaCasinoGames' AS TournamentName,
	  --              -1 AS MarketID,
	  --              CAST(-EFT.RoundID AS VARCHAR(100)) AS MatchID,
	  --              -1 AS TournamentID,
	  --              -1 AS SportID,
	  --              '-' AS Winner,
	  --              downline.PlayerPoint,
	  --              (1 * downline.PlayerPoint) AS INR,
	  --              ISNULL(superadmin.Username + '/', '') + ISNULL([admin].Username + '/', '')
	  --              + ISNULL(supermaster.Username + '/', '') + ISNULL([master].Username + '/', '')
	  --              + ISNULL(player.Username, '') AS Users
	  --       FROM dbo.ExchGameFundTransfer EFT
			--	 LEFT JOIN dbo.ExchGameSettlement EGS ON EGS.RoundID = EFT.RoundID AND EFT.TransactionCode = 'settle_game'
	  --           LEFT JOIN dbo.Downline downline ON EFT.UserId = downline.PlayerID
			--	 LEFT JOIN dbo.[User] superadmin  ON superadmin.UserID = downline.SuperAdminID
			--	 LEFT JOIN dbo.[User] [admin]  ON [admin].UserID = downline.AdminID
			--	 LEFT JOIN dbo.[User] supermaster  ON supermaster.UserID = downline.SuperMasterID
			--	 LEFT JOIN dbo.[User] [master]  ON [master].UserID = downline.MasterID
			--	 LEFT JOIN dbo.[User] player  ON player.UserID = downline.PlayerID
	  --       WHERE (@BetFrom = 0 OR @BetFrom = 2)
	  --             AND (
			--			   (@UserType = 1 AND downline.OwnerID = @Userid)
			--			   OR (@UserType = 2 AND downline.SuperAdminID = @Userid)
			--			   OR (@UserType = 3 AND downline.AdminID = @Userid)
			--			   OR (@UserType = 4 AND downline.SuperMasterID = @Userid)
			--			   OR (@UserType = 5 AND downline.MasterID = @Userid)
			--			)
	  --             AND (@IsBetListLive IS NULL OR (Market.IsSettled = 0 AND Market.MarketStatus != 4))
	  --             AND (@Date IS NULL OR EFT.CreatedDate >= @Date)
			--	       AND (CAST(EFT.CreatedDate AS DATE) >= @StartDate
			--					        OR( @StartDate IS NULL or @StartDate=''))
			--		               AND (CAST(EFT.CreatedDate AS DATE) <= @EndDate
			--					        OR( @EndDate IS NULL or @EndDate=''))
	  --             AND (@IsFancy IS NULL OR Market.IsFancy = @IsFancy)
	  --             AND (@SportID IS NULL OR @SportID = -1)
	  --             AND (@TournamentID IS NULL OR @TournamentID = -1)
	  --             AND (@MatchID IS NULL OR  (-1 * (CASE WHEN ISNUMERIC(@MatchID) = 1 THEN @MatchID ELSE 0 END)) = EFT.RoundID)
	  --             AND (@MarketID IS NULL OR @MarketID = -1)
	  --             AND (@SuperAdminID IS NULL OR downline.SuperAdminID = @SuperAdminID)
	  --             AND (@AdminID IS NULL OR downline.AdminID = @AdminID)
	  --             AND (@SuperMasterID IS NULL OR downline.SuperMasterID = @SuperMasterID)
	  --             AND (@MasterID IS NULL OR downline.MasterID = @MasterID)
	  --             AND (@PlayerID IS NULL OR downline.PlayerID = @PlayerID)
			--UNION ALL
			--SELECT DISTINCT
			--		SSGFT.SSGFundTransferHistoryId AS BetID,
			--		SSGFT.CreatedDate,
	  --              '1' AS IsMatchBet,
              
	  --              SSGFT.CreatedDate AS BetTaken,
	  --              '-' AS Selection,
	  --              1 AS IsBack,
	  --              0 AS Exposure,
	  --              1 AS Rate,
	  --              superadmin.UserID AS SuperAdminID,
	  --              [admin].UserID AS AdminID,
	  --              supermaster.UserID AS SuperMasterID,
	  --              [master].UserID AS MasterID,
	  --              player.UserID AS PlayerID,
	  --              SSGFT.Amount AS Stake,
	  --              0 AS IsBetWon,
	  --              '-' AS BetPL,
	  --              'Plays' AS MarketName,
	  --              0 AS IsFancy,
	  --              0 AS IsSettled,
	  --              'ExchGame' AS SportName,
	  --              ('Round-' + CAST(SSGFT.SessionId AS VARCHAR(50))) AS MatchName,
			--		null AS OpenDate,
	  --              'SuperSpadeGame' AS TournamentName,
	  --              -1 AS MarketID,
	  --              SSGFT.SessionId AS MatchID,
	  --              -1 AS TournamentID,
	  --              -1 AS SportID,
	  --              '-' AS Winner,
	  --              downline.PlayerPoint,
	  --              (1 * downline.PlayerPoint) AS INR,
	  --              ISNULL(superadmin.Username + '/', '') + ISNULL([admin].Username + '/', '')
	  --              + ISNULL(supermaster.Username + '/', '') + ISNULL([master].Username + '/', '')
	  --              + ISNULL(player.Username, '') AS Users
	  --       FROM dbo.SuperSpadeGameFundTransfer SSGFT
			--	 LEFT JOIN dbo.ExchGameSettlement EGS ON EGS.RoundID = EFT.RoundID AND EFT.TransactionCode = 'settle_game'
	  --           LEFT JOIN dbo.Downline downline ON SSGFT.UserId = downline.PlayerID
			--	 LEFT JOIN dbo.[User] superadmin  ON superadmin.UserID = downline.SuperAdminID
			--	 LEFT JOIN dbo.[User] [admin]  ON [admin].UserID = downline.AdminID
			--	 LEFT JOIN dbo.[User] supermaster  ON supermaster.UserID = downline.SuperMasterID
			--	 LEFT JOIN dbo.[User] [master]  ON [master].UserID = downline.MasterID
			--	 LEFT JOIN dbo.[User] player  ON player.UserID = downline.PlayerID
	  --       WHERE SSGFT.TxnSubTypeId = 300 
			--	   AND (@BetFrom = 0 OR @BetFrom = 2)
	  --             AND (
			--			   (@UserType = 1 AND downline.OwnerID = @Userid)
			--			   OR (@UserType = 2 AND downline.SuperAdminID = @Userid)
			--			   OR (@UserType = 3 AND downline.AdminID = @Userid)
			--			   OR (@UserType = 4 AND downline.SuperMasterID = @Userid)
			--			   OR (@UserType = 5 AND downline.MasterID = @Userid)
			--			)
	  --             AND (@IsBetListLive IS NULL OR (Market.IsSettled = 0 AND Market.MarketStatus != 4))
	  --             AND (@Date IS NULL OR SSGFT.CreatedDate >= @Date)
			--	     AND (CAST(SSGFT.CreatedDate AS DATE) >= @StartDate
			--					        OR( @StartDate IS NULL or @StartDate=''))
			--		               AND (CAST(SSGFT.CreatedDate AS DATE) <= @EndDate
			--					        OR( @EndDate IS NULL or @EndDate=''))
	  --             AND (@IsFancy IS NULL OR Market.IsFancy = @IsFancy)
	  --             AND (@SportID IS NULL OR @SportID = -1)
	  --             AND (@TournamentID IS NULL OR @TournamentID = -2)
	  --             AND (@MatchID IS NULL OR  (CASE WHEN ISNUMERIC(@MatchID) = 1 THEN '' ELSE @MatchID END) = SSGFT.SessionId)
	  --             AND (@MarketID IS NULL OR @MarketID = -2)
	  --             AND (@SuperAdminID IS NULL OR downline.SuperAdminID = @SuperAdminID)
	  --             AND (@AdminID IS NULL OR downline.AdminID = @AdminID)
	  --             AND (@SuperMasterID IS NULL OR downline.SuperMasterID = @SuperMasterID)
	  --             AND (@MasterID IS NULL OR downline.MasterID = @MasterID)
	  --             AND (@PlayerID IS NULL OR downline.PlayerID = @PlayerID)
		) dum

	

		SELECT  
			@TotalCount = COUNT(0)
		FROM
		   #BetList;

		SELECT 
			BetID,
			CreatedDate,
			IsMatchBet,
			BetTaken,
			Selection,
			IsBack,
			--Exposure,
			Run,
			Rate,
			SuperAdminID,
			AdminID,
			SuperMasterID,
			MasterID,
			PlayerID,
			Stake,
			IsDelete,
			IsBetWon,
			BetPL,
			MarketName,
			IsFancy,
			IsSettled,
			SportName,
			MatchName,
			OpenDate,
			TournamentName,
			MarketID,
			MarketType,
			CentralID,
			MatchID,
			TournamentID,
			SportID,
			Info,
			Winner,
			PlayerPoint,
			INR,
			Users
		FROM 
			#BetList
			
		ORDER BY 
		CreatedDate DESC,
		CASE WHEN (@OrderByColumn = 'BetID' AND @OrderByDirection = 'ASC') THEN BetID ELSE NULL END ASC,
		CASE WHEN (@OrderByColumn = 'Username' AND @OrderByDirection = 'ASC') THEN Users ELSE NULL END ASC,
		CASE WHEN (@OrderByColumn = 'BetPL' AND @OrderByDirection = 'ASC') THEN BetPL ELSE NULL END ASC,
		CASE WHEN (@OrderByColumn = 'INR' AND @OrderByDirection = 'ASC') THEN INR ELSE NULL END ASC,
		
		CASE WHEN (@OrderByColumn = 'BetID' AND @OrderByDirection = 'DESC') THEN BetID ELSE NULL END DESC,
		CASE WHEN (@OrderByColumn = 'Username' AND @OrderByDirection = 'DESC') THEN Users ELSE NULL END DESC,
		CASE WHEN (@OrderByColumn = 'BetPL' AND @OrderByDirection = 'DESC') THEN BetPL ELSE NULL END DESC,
		CASE WHEN (@OrderByColumn = 'INR' AND @OrderByDirection = 'DESC') THEN INR ELSE NULL END DESC
		
		OFFSET ((@Page - 1) * @Size) ROWS 
		FETCH NEXT @Size ROWS ONLY;

		SELECT 
			@SettledMarketTotalPL = ISNULL(SUM(IIF(IsBetWon = 1, CAST(BetPL AS DECIMAL(18,2)), (-1 * CAST(BetPL AS DECIMAL(18,2))))),0)
		FROM 
			#BetList 
		WHERE 
			BetPL IS NOT NULL 
			AND IsBetWon IS NOT NULL 
			AND IsSettled = 1 
			AND IsDelete = 0;
	-- Insert statements for procedure here

	DROP TABLE IF EXISTS #BetList;
	END TRY
	BEGIN CATCH
		DROP TABLE IF EXISTS #BetList;	
	END CATCH
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_changepassword]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mitesh Chauahan
-- Create date: 22 Feb 2021
-- Description:	Change Password
-- =============================================
CREATE PROCEDURE [dbo].[sp_changepassword] 
	-- Add the parameters for the stored procedure here
	  @userid int,
	  @useroldpwd NVARCHAR(max),
      @userpassword NVARCHAR(max),
	  @status int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS(SELECT * FROM [dbo].[login] WHERE userid=@userid and userpassword=@useroldpwd)
		BEGIN 
			UPDATE [dbo].[login] set userpassword=@userpassword where userid=@userid
			SET @status=1
		END
		ELSE
			
		BEGIN
		set @status=0
		END
END
Return @status
GO
/****** Object:  StoredProcedure [dbo].[sp_GetGameList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Vikas Singh
-- Create date: 19-10-2020
-- Description:	Game List
-- Test case : exec [agent_user].[GetGameList]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetGameList]
AS
BEGIN
    SELECT GameID,
           GameCode,
           Name AS GameName,
           ImagePath,
           Description,
           IsLive,
           IsStop,
           IsActive,
           IsDelete,
           CreatedDate,
           CreatedBy
      FROM dbo.Game WHERE IsActive = 1 AND IsDelete = 0
	  ORDER BY GameName ASC;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_getgamewisepl]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Mitesh Chuahan
-- Create date : 2021 March, 09
-- Description : sp_getgamewisepl
-- TEST        : EXEC	[dbo].[sp_getgamewisepl] @platformname = N'[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]',@gameid = N'0',@fromdate = N'2021-03-01',@todate = N'2021-03-23'
-- =============================================
CREATE PROCEDURE [dbo].[sp_getgamewisepl]
	@platformname varchar(max),
	@gameid varchar(max)='0',
	@fromdate DATETime,
	@todate DATETime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	
	------FROM DATE AND TO DATE------
    IF @fromdate = ''
    BEGIN
        SET @fromdate = CAST(GETDATE() AS DATETIME);
    END;

	IF @todate = ''
	BEGIN
		SET @todate=CAST(GETDATE() AS DATETIME);
	END

	DECLARE @str varchar(max)
	IF @gameid = '0'
	BEGIN
		SET @str =''
	END
	ELSE
	BEGIN
		SET @str = ' AND B.GAMEID IN('+@gameid+')'
	END
	  
	
	--=========================================================================================================
	   
	-- Declare Platform Table
	DECLARE @Plateform_Table TABLE  (id INT identity(1,1),Platformname  NVARCHAR(1000)) 

	-- Insert statement
	INSERT INTO @Plateform_Table
	SELECT * FROM  STRING_SPLIT(@platformname, ',')  

	----QUERY-----
	DECLARE  @query NVARCHAR(max)='';
	DECLARE @cnt INT = 0 ;
	SET @cnt =(SELECT COUNT(1) FROM @Plateform_Table)
	while(@cnt > 0)
	BEGIN
		DECLARE @DBPLATFORM VARCHAR(MAX)=(select Platformname from @Plateform_Table where ID=@cnt)
		IF @query <> ''
		BEGIN
			SET @query = @query + ' UNION ALL ';
		END		
				
		SET @query =@query  + 'SELECT CAST(COALESCE(SUM(IIF(B.IsBetWon = 0,-B.BetPL,B.BetPL)),0) AS DECIMAL(18,0))                     AS [BETPL], G.Name as GameName
							FROM  '+@DBPLATFORM+'.[Bet] B
							INNER JOIN '+@DBPLATFORM+'.[RoundSummary] R ON R.RoundSummaryID = B.RoundSummaryID
							INNER JOIN '+@DBPLATFORM+'.[Game] G ON G.GameID = B.GameID
							WHERE R.IsSettled=1 AND B.ISDELETE=0 AND R.ISDELETE=0 AND G.ISDELETE=0  AND B.ISDELETE=0 AND R.ISDELETE=0 AND G.ISDELETE=0 AND B.ISACTIVE=1 AND R.ISACTIVE=1 AND G.ISDELETE=0 AND G.ISACTIVE = 1
							AND (CONVERT(varchar(10), B.CreatedDate,126) >= CAST(''' + cast(CONVERT(datetime,@fromdate,101) as varchar)+ ''' AS DATETIME)   
					AND CONVERT(varchar(10), B.CreatedDate,126) <= CAST(''' + cast(CONVERT(datetime,@todate,101) as varchar)+ ''' AS DATETIME) ) '+@str+'
							GROUP BY G.Name';
		
		SET @cnt  = @cnt  - 1
	END
    
	
	DECLARE @FINAL_QUERY NVARCHAR(MAX)=''
	SET @FINAL_QUERY= 'SELECT SUM(RESULT.BETPL) AS [BETPL],RESULT.GameName FROM ('+@query+') AS RESULT  GROUP BY RESULT.GameName'

	--PRINT @query;
	--PRINT '=====================================================';
	--PRINT  @FINAL_QUERY
	EXEC (@FINAL_QUERY)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_getgamewisepl_V1]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Mitesh Chuahan
-- Create date : 2021 March, 09
-- Description : sp_getgamewisepl
-- TEST        : EXEC	[dbo].[sp_getgamewisepl] @platformname = N'[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]',@gameid = N'0',@fromdate = N'2021-03-01',@todate = N'2021-03-23'
-- =============================================
create   PROCEDURE [dbo].[sp_getgamewisepl_V1]
	@platformname varchar(max),
	@gameid varchar(max)='0',
	@fromdate DATETime,
	@todate DATETime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	
	------FROM DATE AND TO DATE------
    IF @fromdate = ''
    BEGIN
        SET @fromdate = CAST(GETDATE() AS DATETIME);
    END;

	IF @todate = ''
	BEGIN
		SET @todate=CAST(GETDATE() AS DATETIME);
	END

	DECLARE @str varchar(max)
	IF @gameid = '0'
	BEGIN
		SET @str =''
	END
	ELSE
	BEGIN
		SET @str = ' AND B.GAMEID IN('+@gameid+')'
	END
	  
	
	--=========================================================================================================
	   
	-- Declare Platform Table
	DECLARE @Plateform_Table TABLE  (id INT identity(1,1),Platformname  NVARCHAR(1000)) 

	-- Insert statement
	INSERT INTO @Plateform_Table
	SELECT * FROM  STRING_SPLIT(@platformname, ',')  

	----QUERY-----
	DECLARE  @query NVARCHAR(max)='';
	DECLARE @cnt INT = 0 ;
	SET @cnt =(SELECT COUNT(1) FROM @Plateform_Table)
	while(@cnt > 0)
	BEGIN

		--DECLARE @DBPLATFORM VARCHAR(MAX)=(select Platformname from @Plateform_Table where ID=@cnt)

		DECLARE @DBPLATFORM VARCHAR(MAX)= '';

		SELECT @DBPLATFORM = '[' + LinkServerName + '].[' +DBName+'].[dbo]'
		FROM dbo.[platform]
		where LinkServerName = ( select @platformname from @Plateform_Table where ID=@cnt)

		IF @query <> ''
		BEGIN
			SET @query = @query + ' UNION ALL ';
		END		
				
		SET @query =@query  + 'SELECT CAST(COALESCE(SUM(IIF(B.IsBetWon = 0,-B.BetPL,B.BetPL)),0) AS DECIMAL(18,0))                     AS [BETPL], G.Name as GameName
							FROM  '+@DBPLATFORM+'.[Bet] B
							INNER JOIN '+@DBPLATFORM+'.[RoundSummary] R ON R.RoundSummaryID = B.RoundSummaryID
							INNER JOIN '+@DBPLATFORM+'.[Game] G ON G.GameID = B.GameID
							WHERE R.IsSettled=1 AND B.ISDELETE=0 AND R.ISDELETE=0 AND G.ISDELETE=0  AND B.ISDELETE=0 AND R.ISDELETE=0 AND G.ISDELETE=0 AND B.ISACTIVE=1 AND R.ISACTIVE=1 AND G.ISDELETE=0 AND G.ISACTIVE = 1
							AND (CONVERT(varchar(10), B.CreatedDate,126) >= CAST(''' + cast(CONVERT(datetime,@fromdate,101) as varchar)+ ''' AS DATETIME)   
					AND CONVERT(varchar(10), B.CreatedDate,126) <= CAST(''' + cast(CONVERT(datetime,@todate,101) as varchar)+ ''' AS DATETIME) ) '+@str+'
							GROUP BY G.Name';
		
		SET @cnt  = @cnt  - 1
	END
    
	
	DECLARE @FINAL_QUERY NVARCHAR(MAX)=''
	SET @FINAL_QUERY= 'SELECT SUM(RESULT.BETPL) AS [BETPL],RESULT.GameName FROM ('+@query+') AS RESULT  GROUP BY RESULT.GameName'

	--PRINT @query;
	--PRINT '=====================================================';
	--PRINT  @FINAL_QUERY
	EXEC (@FINAL_QUERY)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetPlatformList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Mitesh Chauhan
-- Create date: 19-10-2020
-- Description:	Platform List
-- Test case : 
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetPlatformList]
AS
BEGIN
    SELECT id, Name AS PlatName,LinkServerName, DBName        
      FROM [dbo].[platform] WHERE  IsDelete = 0
	  
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_getreportdata]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Mitesh Chuahan
-- Create date : 2021 March, 09
-- Description : sp_getreportdata
-- TEST        : EXEC	[dbo].[sp_getreportdata] @platformname = N'[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]',@gameid = N'0',@fromdate = N'2021-03-01',@todate = N'2021-03-23'
-- =============================================
CREATE PROCEDURE [dbo].[sp_getreportdata]
	@platformname varchar(max),
	@gameid varchar(max)='0',
	@fromdate DATETime,
	@todate DATETime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	
	------FROM DATE AND TO DATE------
    IF @fromdate = ''
    BEGIN
        SET @fromdate = CAST(GETDATE() AS DATETIME);
    END;

	IF @todate = ''
	BEGIN
		SET @todate=CAST(GETDATE() AS DATETIME);
	END

	DECLARE @str varchar(max)
	IF @gameid = '0'
	BEGIN
		SET @str =''
	END
	ELSE
	BEGIN
		SET @str = ' AND B.GAMEID IN('+@gameid+')'
	END
	  
	--=========================================================================================================
	   
	-- Declare Platform Table
	DECLARE @Plateform_Table TABLE  (id INT identity(1,1),Platformname  NVARCHAR(1000)) 

	-- Insert statement
	INSERT INTO @Plateform_Table
	SELECT * FROM  STRING_SPLIT(@platformname, ',')  

	----QUERY-----
	DECLARE  @query NVARCHAR(max)='';
	DECLARE @cnt INT = 0 ;
	SET @cnt =(SELECT COUNT(1) FROM @Plateform_Table)
	while(@cnt > 0)
	BEGIN
		DECLARE @DBPLATFORM VARCHAR(MAX)=(select Platformname from @Plateform_Table where ID=@cnt)
		IF @query <> ''
		BEGIN
			SET @query = @query + ' UNION ALL ';
		END		
				
		SET @query =@query  + 'SELECT CAST(COALESCE(SUM(IIF(B.IsBetWon = 0,-B.BetPL,B.BetPL)),0) AS DECIMAL(18,0)) AS [BETPL],
					CAST(COUNT(B.BetID) AS DECIMAL(18,0)) AS [#BET],
					CAST(COUNT(DISTINCT B.ClientUserID) AS DECIMAL(18,0)) AS [#PLAYER],
					CAST(COALESCE(SUM(B.BETAMOUNT),0) AS DECIMAL(18,0)) AS [VOLUME], 
					CAST(COALESCE(SUM(B.BETAMOUNT)/COUNT(DISTINCT B.ClientUserID),0) AS DECIMAL(18,0)) AS [AVG_VOL_PLAYER],
					CAST(COALESCE(SUM(B.BETAMOUNT)/ COUNT(B.BetID),0) AS DECIMAL(18,0)) AS [AVG_VOL_BET]
					FROM  '+@DBPLATFORM+'.[Bet] B
					INNER JOIN '+@DBPLATFORM+'.[RoundSummary] R ON R.RoundSummaryID = B.RoundSummaryID
					INNER JOIN '+@DBPLATFORM+'.[Game] G ON G.GameID = B.GameID
					WHERE R.IsSettled=1  AND B.ISDELETE=0 AND R.ISDELETE=0 AND G.ISDELETE=0 AND B.ISACTIVE=1 AND R.ISACTIVE=1 AND G.ISDELETE=0 AND G.ISACTIVE = 1
					AND (CONVERT(varchar(10), B.CreatedDate,126) >= CAST(''' + cast(CONVERT(datetime,@fromdate,101) as varchar)+ ''' AS DATETIME)   
					AND CONVERT(varchar(10), B.CreatedDate,126) <= CAST(''' + cast(CONVERT(datetime,@todate,101) as varchar)+ ''' AS DATETIME) )
					'+@str;
		
		SET @cnt  = @cnt  - 1
	END
    PRINT @cnt ;
	
	DECLARE @FINAL_QUERY NVARCHAR(MAX)=''
	SET @FINAL_QUERY= 'SELECT SUM(RESULT.BETPL) AS [BETPL],SUM(RESULT.#BET) AS [#BET],SUM(RESULT.#PLAYER)  AS [#PLAYER],SUM(RESULT.VOLUME) AS [VOLUME],SUM(RESULT.AVG_VOL_PLAYER) AS [AVG_VOL_PLAYER],SUM(RESULT.AVG_VOL_BET) AS [AVG_VOL_BET] FROM ('+@query+') AS RESULT'

	--PRINT @query;
	--PRINT '=====================================================';
	--PRINT  @FINAL_QUERY
	EXEC (@FINAL_QUERY)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_getreportdata_v2]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Mitesh Chuahan
-- Create date : 2021 March, 09
-- Description : sp_getreportdata
-- TEST        : EXEC	[dbo].[sp_getreportdata] @platformname = N'[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]',@gameid = N'0',@fromdate = N'2021-03-01',@todate = N'2021-03-23'
-- =============================================
CREATE PROCEDURE [dbo].[sp_getreportdata_v2]
	@platformname varchar(max),
	@gameid varchar(max)='0',
	@fromdate DATETime,
	@todate DATETime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	
	------FROM DATE AND TO DATE------
    IF @fromdate = ''
    BEGIN
        SET @fromdate = CAST(GETDATE() AS DATETIME);
    END;

	IF @todate = ''
	BEGIN
		SET @todate=CAST(GETDATE() AS DATETIME);
	END

	DECLARE @str varchar(max)
	IF @gameid = '0'
	BEGIN
		SET @str =''
	END
	ELSE
	BEGIN
		SET @str = ' AND B.GAMEID IN('+@gameid+')'
	END
	  
	print @str
	--=========================================================================================================
	   
	-- Declare Platform Table
	DECLARE @Plateform_Table TABLE  (id INT identity(1,1),Platformname  NVARCHAR(1000)) 

	-- Insert statement
	INSERT INTO @Plateform_Table
	SELECT * FROM  STRING_SPLIT(@platformname, ',')  

	----QUERY-----
	DECLARE  @query NVARCHAR(max)='';
	DECLARE @cnt INT = 0 ;
	SET @cnt =(SELECT COUNT(1) FROM @Plateform_Table)
	while(@cnt > 0)
	BEGIN
		DECLARE @DBPLATFORM VARCHAR(MAX)=(select Platformname from @Plateform_Table where ID=@cnt)
		IF @query <> ''
		BEGIN
			SET @query = @query + ' UNION ALL ';
		END		

		IF @DBPLATFORM='[LNK_SuperNowa].[dev_casino].[dbo]' OR @DBPLATFORM='[LNK_SuperNowa].[dev_virtual_casino].[dbo]'
		BEGIN
			SET @query =@query  + 'SELECT CAST(COALESCE(SUM(IIF(B.IsBetWon = 0,-B.BetPL,B.BetPL)),0) AS DECIMAL(18,0)) AS [BETPL],
					CAST(COUNT(B.BetID) AS DECIMAL(18,0)) AS [#BET],
					COUNT(DISTINCT B.ClientUserID) AS [#PLAYER],
					COALESCE(SUM(B.BETAMOUNT),0) AS [VOLUME], 
					COALESCE(SUM(B.BETAMOUNT)/COUNT(B.BetID),0) AS [AVG_VOL_PLAYER],
					COALESCE(SUM(B.BETAMOUNT)/ COUNT(B.BetID),0) AS [AVG_VOL_BET]
					FROM  '+@DBPLATFORM+'.[Bet] B
					INNER JOIN '+@DBPLATFORM+'.[RoundSummary] R ON R.RoundSummaryID = B.RoundSummaryID
					INNER JOIN '+@DBPLATFORM+'.[Game] G ON G.GameID = B.GameID
					WHERE R.IsSettled=1 
					AND (CONVERT(varchar(10), B.CreatedDate,126) >= CAST(''' + cast(CONVERT(datetime,@fromdate,101) as varchar)+ ''' AS DATETIME)   
					AND CONVERT(varchar(10), B.CreatedDate,126) <= CAST(''' + cast(CONVERT(datetime,@todate,101) as varchar)+ ''' AS DATETIME) )
					'+@str;
		END
		ELSE IF @DBPLATFORM ='LNK_MYSQL'
		BEGIN
			SET @query=@query + 'SELECT *
					FROM OPENQUERY(LNK_MYSQL,''SELECT ROUND(sum(if(tg.isBetWon = 0, `AdminShare`, 0)), 2) - round(sum(if(tg.isBetWon = 1, `AdminShare`, 0)), 2) as ProfitLoss
					FROM `games` `tg`
					LEFT JOIN `users` `tu` ON `tu`.`id` = `tg`.`AgentUserID` and `tu`.`IsDelete` = 0 and `tu`.`id`!=68
					LEFT JOIN `clients` `tcc` ON `tcc`.`ClientUserID` = `tg`.`ClientUserID` and `tcc`.`IsDelete` = 0 and `tcc`.`IsDemo`=0 and `tcc`.`ClientUserID`!=70
					WHERE `tg`.`status` = 1
					AND `tg`.`IsDeleted` = 0
					AND `tg`.`IsDemo` = 0
					AND `tg`.`IsThirdPartySettled` = 1
					AND `tg`.`IsCancelled` = 0
					AND `tg`.`created_at` BETWEEN Cast("' + cast(CONVERT(datetime,@fromdate,101) as varchar)+ '" as date) AND cast( "' + cast(CONVERT(datetime,@todate,101) as varchar)+ '" as date)
					ORDER BY `tu`.`username` DESC'')
					';
		END
		
		
		SET @cnt  = @cnt  - 1
	END
    PRINT @cnt ;
	
	DECLARE @FINAL_QUERY NVARCHAR(MAX)=''
	SET @FINAL_QUERY= 'SELECT SUM(RESULT.BETPL) AS [BETPL],SUM(RESULT.#BET) AS [#BET],SUM(RESULT.#PLAYER)  AS [#PLAYER],SUM(RESULT.VOLUME) AS [VOLUME],SUM(RESULT.AVG_VOL_PLAYER) AS [AVG_VOL_PLAYER],SUM(RESULT.AVG_VOL_BET) AS [AVG_VOL_BET] FROM ('+@query+') AS RESULT'

	--PRINT @query;
	--PRINT '=====================================================';
	--PRINT  @FINAL_QUERY
	EXEC (@FINAL_QUERY)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_getreportdata_V3]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Mitesh Chuahan
-- Create date : 2021 March, 09
-- Description : sp_getreportdata
-- TEST        : EXEC	[dbo].[sp_getreportdata] @platformname = N'[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]',@gameid = N'0',@fromdate = N'2021-03-01',@todate = N'2021-03-23'
-- =============================================
create   PROCEDURE [dbo].[sp_getreportdata_V3]
	@platformname varchar(max),
	@gameid varchar(max)='0',
	@fromdate DATETime,
	@todate DATETime
AS
set nocount on
set transaction isolation level read committed
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	
	------FROM DATE AND TO DATE------
    IF @fromdate = ''
    BEGIN
        SET @fromdate = CAST(GETDATE() AS DATETIME);
    END;

	IF @todate = ''
	BEGIN
		SET @todate=CAST(GETDATE() AS DATETIME);
	END

	DECLARE @str varchar(max)
	IF @gameid = '0'
	BEGIN
		SET @str =''
	END
	ELSE
	BEGIN
		SET @str = ' AND B.GAMEID IN('+@gameid+')'
	END
	  
	--=========================================================================================================
	   
	-- Declare Platform Table
	DECLARE @Plateform_Table TABLE  (id INT identity(1,1),Platformname  NVARCHAR(1000)) 

	-- Insert statement
	INSERT INTO @Plateform_Table
	SELECT * FROM  STRING_SPLIT(@platformname, ',')  

	----QUERY-----
	DECLARE  @query NVARCHAR(max)='';
	DECLARE @cnt INT = 0 ;
	SET @cnt =(SELECT COUNT(1) FROM @Plateform_Table)
	while(@cnt > 0)
	BEGIN
		DECLARE @DBPLATFORM VARCHAR(MAX); -- =(select Platformname from @Plateform_Table where ID=@cnt)
		SELECT @DBPLATFORM = '[' + LinkServerName + '].[' +DBName+'].[dbo]'
		FROM dbo.[platform] 
		where LinkServerName = ( select @platformname from @Plateform_Table where ID=@cnt)

		IF @query <> ''
		BEGIN
			SET @query = @query + ' UNION ALL ';
		END		
				
		SET @query =@query  + 'SELECT CAST(COALESCE(SUM(IIF(B.IsBetWon = 0,-B.BetPL,B.BetPL)),0) AS DECIMAL(18,0)) AS [BETPL],
					CAST(COUNT(B.BetID) AS DECIMAL(18,0)) AS [#BET],
					CAST(COUNT(DISTINCT B.ClientUserID) AS DECIMAL(18,0)) AS [#PLAYER],
					CAST(COALESCE(SUM(B.BETAMOUNT),0) AS DECIMAL(18,0)) AS [VOLUME], 
					CAST(COALESCE(SUM(B.BETAMOUNT)/COUNT(DISTINCT B.ClientUserID),0) AS DECIMAL(18,0)) AS [AVG_VOL_PLAYER],
					CAST(COALESCE(SUM(B.BETAMOUNT)/ COUNT(B.BetID),0) AS DECIMAL(18,0)) AS [AVG_VOL_BET]
					FROM  '+@DBPLATFORM+'.[Bet] B
					INNER JOIN '+@DBPLATFORM+'.[RoundSummary] R ON R.RoundSummaryID = B.RoundSummaryID
					INNER JOIN '+@DBPLATFORM+'.[Game] G ON G.GameID = B.GameID
					WHERE R.IsSettled=1  AND B.ISDELETE=0 AND R.ISDELETE=0 AND G.ISDELETE=0 AND B.ISACTIVE=1 AND R.ISACTIVE=1 AND G.ISDELETE=0 AND G.ISACTIVE = 1
					AND (CONVERT(varchar(10), B.CreatedDate,126) >= CAST(''' + cast(CONVERT(datetime,@fromdate,101) as varchar)+ ''' AS DATETIME)   
					AND CONVERT(varchar(10), B.CreatedDate,126) <= CAST(''' + cast(CONVERT(datetime,@todate,101) as varchar)+ ''' AS DATETIME) )
					'+@str;
		
		SET @cnt  = @cnt  - 1
	END
    --PRINT @cnt ;
	
	DECLARE @FINAL_QUERY NVARCHAR(MAX)=''
	SET @FINAL_QUERY= 'set nocount on;
set transaction isolation level read committed;
SELECT SUM(RESULT.BETPL) AS [BETPL],SUM(RESULT.#BET) AS [#BET],SUM(RESULT.#PLAYER)  AS [#PLAYER],SUM(RESULT.VOLUME) AS [VOLUME],SUM(RESULT.AVG_VOL_PLAYER) AS [AVG_VOL_PLAYER],SUM(RESULT.AVG_VOL_BET) AS [AVG_VOL_BET] FROM ('+@query+') AS RESULT;'

	--PRINT @query;
	--PRINT '=====================================================';
	--PRINT  @FINAL_QUERY
	EXEC (@FINAL_QUERY)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_login]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Name:		Mitesh Chuahan
-- Create date: 02-Feb-2021
-- Description:	Login Store Procedure>
-- =============================================
CREATE  PROCEDURE [dbo].[sp_login]
      @username NVARCHAR(max),
      @userpassword NVARCHAR(max)
AS
BEGIN
      SET NOCOUNT ON;
     
     select L.userid,L.username,UP.PageIds from login as L left join UserPermission as UP ON L.userid=UP.UserId where LOWER(username)=LOWER(@username) and userpassword=@userpassword;
        
END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteUser]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteUser]
@ID int
AS
BEGIN

SET NOCOUNT ON;

Delete FROM UserPermission WHERE UserId=@ID;

DELETE FROM login WHERE userid=@ID;

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAgentPLList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAgentPLList] 
      @LinkServerName VARCHAR(100)='LNK_SuperNowa_Virtual',
      @DBName VARCHAR(100)='Supernowa_Virtual',
	  @SelectedOption VARCHAR(100)='ProfitLoss',
	  @FROMDATE datetime,
	  @TODATE datetime
AS
BEGIN

SET NOCOUNT ON;

DECLARE  @FINAL_QUERY VARCHAR(max)='';

IF(@SelectedOption = 'PowerGame')
	BEGIN

	--SET @FROMDATE=CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME);
    --SET @TODATE=DATEADD(SECOND,-1,DATEADD(DAY,+1,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)));

		set  @FINAL_QUERY='SELECT * FROM OPENQUERY(LNK_PowerGames_Live,''SELECT G.AgentUserID AS UserID,GU.username as AgentName, 
					(ROUND(SUM(CASE
					WHEN G.IsBetWon = 0 THEN G.AdminShare
					ELSE 0
				END),2) - ROUND(SUM(CASE
					WHEN G.isBetWon = 1 THEN G.AdminShare
					ELSE 0
				END),2)) AS PROFITLOSS
			 FROM games AS G 
			 LEFT JOIN users AS GU ON GU.id=G.AgentUserID
			 LEFT JOIN clients AS C ON C.ClientUserID=G.ClientUserID
			 WHERE GU.IsDelete=0 AND GU.id!=68 AND C.IsDelete=0 AND C.IsDemo=0 AND C.ClientUserID!=70
			 AND G.status=1 AND G.IsDeleted=0 AND G.IsDemo=0 AND G.IsBot=0 AND G.IsThirdPartySettled=1
			 AND G.IsCancelled=0 
		 	 AND (G.created_at BETWEEN '''''+CONVERT(VARCHAR,@FROMDATE,120)+''''' AND '''''+CONVERT(VARCHAR,@TODATE,120)+''''' ) 
	         GROUP BY G.AgentUserID,GU.username'')'


END
ELSE IF(@SelectedOption = 'SuperNowa')
	BEGIN

	--SET @FROMDATE =DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME));
   -- SET @TODATE =DATEADD(SECOND,-1,DATEADD(DAY,+1,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME))));


	   /*set @FINAL_QUERY='SELECT tblSettle.UserID,
           tblUser.Username AS AgentName,
           SUM(ISNULL(tblSettle.Debit_Dena, 0) - ISNULL(tblSettle.Credit_Lena, 0)) AS PROFITLOSS
    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.[tblSettlement] tblSettle
        INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.[User] tblUser ON tblUser.UserID = tblSettle.UserID
        INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.[RoundSummary] tblRound ON tblRound.RoundSummaryID = tblSettle.RoundSummaryID
    WHERE tblUser.RoleID = 3
	  	  AND (tblRound.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATE,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATE,101) as varchar) +''' AS DATETIME) ) 
          AND tblRound.IsDelete = 0
          AND tblRound.IsActive = 1
          AND tblRound.IsSettled = 1
          AND tblSettle.IsActive = 1
          AND tblSettle.IsDelete = 0
    GROUP BY tblSettle.UserID,
             tblUser.Username,
             tblSettle.IsActive;'*/


	 /*set @FINAL_QUERY='SELECT * FROM OPENQUERY('+@LinkServerName+',''SELECT tblSettle.UserID,
           tblUser.Username AS AgentName,
           SUM(ISNULL(tblSettle.Debit_Dena, 0) - ISNULL(tblSettle.Credit_Lena, 0)) AS PROFITLOSS
    FROM tblSettlement AS tblSettle
        INNER JOIN [User] AS tblUser ON tblUser.UserID = tblSettle.UserID
        INNER JOIN RoundSummary AS tblRound ON tblRound.RoundSummaryID = tblSettle.RoundSummaryID
    WHERE tblUser.RoleID = 3
	  	  AND (tblRound.CreatedDate BETWEEN '''''+CONVERT(VARCHAR,@FROMDATE,120)+''''' AND '''''+CONVERT(VARCHAR,@TODATE,120)+''''' ) 
          AND tblRound.IsDelete = 0
          AND tblRound.IsActive = 1
          AND tblRound.IsSettled = 1
          AND tblSettle.IsActive = 1
          AND tblSettle.IsDelete = 0
    GROUP BY tblSettle.UserID,
             tblUser.Username,
             tblSettle.IsActive'')'*/

			 set @FINAL_QUERY='SELECT *
				FROM
				(
					SELECT result.UserID,
						   Username AS AgentName,
						   result.PROFITLOSS
					FROM
					(
						SELECT tblSettle.UserID,
							   SUM(ISNULL(tblSettle.Debit_Dena, 0) - ISNULL(tblSettle.Credit_Lena, 0)) AS PROFITLOSS
						FROM ['+ @LinkServerName +'].['+ @DBName +'].[dbo].tblSettlement AS tblSettle
							INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].[dbo].RoundSummary AS tblRound
								ON tblRound.RoundSummaryID = tblSettle.RoundSummaryID
						WHERE tblSettle.UserID <> 1
							  AND tblSettle.IsActive = 1
							  AND tblSettle.IsDelete = 0
							  AND (tblRound.CreatedDate
							  BETWEEN '''+CONVERT(VARCHAR,@FROMDATE,120)+''' AND '''+CONVERT(VARCHAR,@TODATE,120)+'''
								  )
							  AND tblRound.IsDelete = 0
							  AND tblRound.IsActive = 1
							  AND tblRound.IsSettled = 1
						GROUP BY tblSettle.UserID
					) result
						LEFT JOIN
						(
							SELECT UserID,
								   Username
							FROM ['+ @LinkServerName +'].['+ @DBName +'].[dbo].[User]
							WHERE RoleID = 3
								  AND Username IS NOT NULL
						) tblUser
							ON tblUser.UserID = result.UserID
				) x
				WHERE x.PROFITLOSS IS NOT NULL
				ORDER BY x.UserID;'
	END
	ELSE
	BEGIN
	     set @FINAL_QUERY = 'SELECT 0 UserID, '' AgentName, 0 PROFITLOSS;'
	END

EXEC (@FINAL_QUERY)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetGamesList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetGamesList] 
      @LinkServerName VARCHAR(100)='LNK_SuperNowa_Virtual',
      @DBName VARCHAR(100)='Supernowa_Virtual',
	  @SelectedOption VARCHAR(100)='else'
AS
BEGIN

SET NOCOUNT ON;

--AND CONVERT(VARCHAR(10),DATEADD(Mi,+330,R.CreatedDate), 105) = CONVERT(VARCHAR(10), getdate(), 105) 
--AND (R.CreatedDate BETWEEN DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)) AND DATEADD(DAY,+1,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME))) )

DECLARE  @FINAL_QUERY VARCHAR(max)='';

DECLARE @FROMDATEUTC datetime=DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME));
DECLARE @TODATEUTC datetime=DATEADD(SECOND,-1,DATEADD(DAY,+1,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME))));

IF(@SelectedOption = 'PowerGame')
	BEGIN

	DECLARE @FROMDATE datetime=CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME);
    DECLARE @TODATE datetime=DATEADD(SECOND,-1,DATEADD(DAY,+1,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)));

		 set @FINAL_QUERY='SELECT * FROM OPENQUERY(LNK_PowerGames_Live,''
		SELECT
		G.gameable_type as Name,
		'''' as GameCode,
		0 as GameID,
		'''' as ImagePath,
		COUNT(*) as TotalBet,
		cast(SUM(G.bet * G.PointValue) as decimal(38,2)) as TotalBetAmount,
		(ROUND(SUM(CASE
			WHEN G.IsBetWon = 0 THEN G.AdminShare
			ELSE 0
		END),2) - ROUND(SUM(CASE
			WHEN G.isBetWon = 1 THEN G.AdminShare
			ELSE 0
		END),2)) AS TotalProfitLoss
		from games as G where G.status=1 AND G.IsDemo=0 AND G.IsCancelled=0 AND G.IsBot=0 
		AND (G.created_at BETWEEN '''''+CONVERT(VARCHAR,@FROMDATE,120)+''''' AND '''''+CONVERT(VARCHAR,@TODATE,120)+''''') 
		GROUP BY G.gameable_type
		ORDER BY TotalProfitLoss DESC'')'


END
ELSE IF(@SelectedOption = 'ProfitLoss')
	BEGIN

	    set @FINAL_QUERY= 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,
			G.GameCode,
			G.IsLive,
			G.IsActive,
			G.IsStop,
			G.ImagePath,
	        0 AS TotalBet,  
	        0 AS TotalBetAmount,
	        SUM(ISNULL(S.Credit_Lena, 0) - ISNULL(S.Debit_Dena, 0)) AS TotalProfitLoss
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.tblSettlement S ON S.RoundSummaryID = R.RoundSummaryID AND S.IsDelete = 0 AND S.UserID = 1
	    WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) ) 
	    GROUP BY G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.TotalProfitLoss DESC, #TempTable.GameID;'
		
	END
	ELSE IF(@SelectedOption = 'BetAmount')
	BEGIN

	     set @FINAL_QUERY = 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,
			G.GameCode,
			G.IsLive,
			G.IsActive,
			G.IsStop,
			G.ImagePath,
	        0 AS TotalBet,  
	        SUM(ISNULL(B.BetAmount, 0)) * -1 AS TotalBetAmount,
	        0 AS TotalProfitLoss
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) )
	    GROUP BY G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.TotalBetAmount DESC, #TempTable.GameID;'

	END
	ELSE IF(@SelectedOption = 'NoOfBetCount')
	BEGIN

	     set @FINAL_QUERY = 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,
			G.GameCode,
			G.IsLive,
			G.IsActive,
			G.IsStop,
			G.ImagePath,
	        COUNT(B.BetID) AS TotalBet,
	        0 AS TotalBetAmount,
	        0 AS TotalProfitLoss
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) )
	    GROUP BY G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.TotalBet DESC, #TempTable.GameID;'

	END
	ELSE
	BEGIN
	     set @FINAL_QUERY = 'SELECT 0 GameID, '''' Name, '''' GameCode, 0 IsLive, 0 IsActive, 1 IsStop, '''' ImagePath, 0 TotalBet, 0 TotalBetAmount, 0 TotalProfitLoss;'
	END

EXEC (@FINAL_QUERY)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetGamesListForChart]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetGamesListForChart] 
      @LinkServerName VARCHAR(100)='LNK_SuperNowa_Virtual',
      @DBName VARCHAR(100)='Supernowa_Virtual',
	  @SelectedOption VARCHAR(100)='else'
AS
BEGIN

SET NOCOUNT ON;

DECLARE  @FINAL_QUERY VARCHAR(max)='';

IF(@SelectedOption = 'PowerGame')
	BEGIN

		  set @FINAL_QUERY='SELECT * FROM OPENQUERY(LNK_PowerGames_Live, ''
		  SELECT DISTINCT G.gameable_type as Name
		  FROM games as G
		  where G.status=1 
		  AND G.IsDemo=0 
		  AND G.IsCancelled=0
		  AND G.IsBot=0'')';

END
	ELSE IF(@SelectedOption = 'SuperNowa')
	BEGIN

	     set @FINAL_QUERY = 'SELECT
			G.GameID,
			G.Name,
			G.GameCode		
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	   	WHERE G.IsActive=1 
		AND G.IsDelete=0';

	END
	ELSE
	BEGIN
	     set @FINAL_QUERY = 'SELECT 0 GameID, '''' Name, '''' GameCode;'
	END

EXEC (@FINAL_QUERY)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLineChartData]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetLineChartData] 
      @LinkServerName VARCHAR(100)='LNK_SuperNowa_Virtual',
      @DBName VARCHAR(100)='Supernowa_Virtual',
	  @GameID VARCHAR(100)='17',
	  @SelectedOption VARCHAR(100)='else'
AS
BEGIN

SET NOCOUNT ON;

DECLARE  @FINAL_QUERY VARCHAR(max)='';

DECLARE @FROMDATEUTC datetime=DATEADD(DAY,-5,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)));
DECLARE @TODATEUTC datetime=DATEADD(SECOND,-1,DATEADD(DAY,+1,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME))));

IF(@SelectedOption = 'PowerGame')
	BEGIN

	DECLARE @FROMDATE datetime=DATEADD(DAY,-6,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME));
    DECLARE @TODATE datetime=DATEADD(SECOND,-1,DATEADD(DAY,+1,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)));

       SET  @FINAL_QUERY='SELECT * FROM OPENQUERY(LNK_PowerGames_Live,''
		SELECT
		G.gameable_type as Name,
		'''' as GameCode,
		0 as GameID,
		'''' as ImagePath,
		COUNT(*) as TotalBet,
		cast(SUM(G.bet * G.PointValue) as decimal(38,2)) as TotalBetAmount,
		(ROUND(SUM(CASE
			WHEN G.IsBetWon = 0 THEN G.AdminShare
			ELSE 0
		END),2) - ROUND(SUM(CASE
			WHEN G.isBetWon = 1 THEN G.AdminShare
			ELSE 0
		END),2)) AS TotalProfitLoss,
		CAST(G.created_at AS DATE) AS CreatedDate
		from games as G where G.status=1 AND G.IsDemo=0 AND G.IsCancelled=0 AND G.IsBot=0 
		AND (G.created_at BETWEEN '''''+CONVERT(VARCHAR,@FROMDATE,120)+''''' AND '''''+CONVERT(VARCHAR,@TODATE,120)+''''') 
		GROUP BY G.gameable_type,CAST(G.created_at AS DATE)
		ORDER BY CreatedDate,TotalProfitLoss DESC'') WHERE NAME='''+@GameID+''''


END
ELSE IF(@SelectedOption = 'ProfitLoss')
	BEGIN

	    set @FINAL_QUERY= 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,
			G.GameCode,
			G.IsLive,
			G.IsActive,
			G.IsStop,
			G.ImagePath,
	        0 AS TotalBet,  
	        0 AS TotalBetAmount,
	        SUM(ISNULL(S.Credit_Lena, 0) - ISNULL(S.Debit_Dena, 0)) AS TotalProfitLoss,
			CAST(DATEADD(MINUTE,+330,R.CreatedDate) AS DATE) AS CreatedDate
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.tblSettlement S ON S.RoundSummaryID = R.RoundSummaryID AND S.IsDelete = 0 AND S.UserID = 1
	    WHERE G.GameID='+@GameID+' AND G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) ) 
	    GROUP BY CAST(DATEADD(MINUTE,+330,R.CreatedDate) AS DATE),G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.CreatedDate,#TempTable.TotalProfitLoss DESC, #TempTable.GameID;'
		
	END
	ELSE IF(@SelectedOption = 'BetAmount')
	BEGIN

	     set @FINAL_QUERY = 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,
			G.GameCode,
			G.IsLive,
			G.IsActive,
			G.IsStop,
			G.ImagePath,
	        0 AS TotalBet,  
	        SUM(ISNULL(B.BetAmount, 0)) * -1 AS TotalBetAmount,
	        0 AS TotalProfitLoss,
			CAST(DATEADD(MINUTE,+330,R.CreatedDate) AS DATE) AS CreatedDate
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.GameID='+@GameID+' AND G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) )
	    GROUP BY CAST(DATEADD(MINUTE,+330,R.CreatedDate) AS DATE),G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.CreatedDate,#TempTable.TotalBetAmount DESC, #TempTable.GameID;'

	END
	ELSE IF(@SelectedOption = 'NoOfBetCount')
	BEGIN

	     set @FINAL_QUERY = 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,
			G.GameCode,
			G.IsLive,
			G.IsActive,
			G.IsStop,
			G.ImagePath,
	        COUNT(B.BetID) AS TotalBet,
	        0 AS TotalBetAmount,
	        0 AS TotalProfitLoss,
			CAST(DATEADD(MINUTE,+330,R.CreatedDate) AS DATE) AS CreatedDate
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.GameID='+@GameID+' AND G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) )
	    GROUP BY CAST(DATEADD(MINUTE,+330,R.CreatedDate) AS DATE),G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.CreatedDate,#TempTable.TotalBet DESC, #TempTable.GameID;'

	END
	ELSE
	BEGIN
	     set @FINAL_QUERY = 'SELECT 0 GameID, '''' Name, '''' GameCode, 0 IsLive, 0 IsActive, 1 IsStop, '''' ImagePath, 0 TotalBet, 0 TotalBetAmount, 0 TotalProfitLoss;'
	END

EXEC (@FINAL_QUERY)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetPageList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[USP_GetPageList] 
AS
BEGIN

SET NOCOUNT ON;

SELECT * FROM PageInfo;

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetPieChartDataPlatformWise]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPieChartDataPlatformWise] 
	  @SelectedOption VARCHAR(100)='else'
AS
BEGIN

SET NOCOUNT ON;

--AND CONVERT(VARCHAR(10),DATEADD(Mi,+330,R.CreatedDate), 105) = CONVERT(VARCHAR(10), getdate(), 105) 
--AND (R.CreatedDate BETWEEN DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)) AND DATEADD(DAY,+1,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME))) )

DECLARE  @FINAL_QUERY VARCHAR(max)='';

DECLARE @FROMDATEUTC datetime=DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME));
DECLARE @TODATEUTC datetime=DATEADD(SECOND,-1,DATEADD(DAY,+1,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME))));
DECLARE @FROMDATE datetime=CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME);
DECLARE @TODATE datetime=DATEADD(SECOND,-1,DATEADD(DAY,+1,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)));

IF(@SelectedOption = 'AvgByBet')
	BEGIN

	   DECLARE @TmpAvgByBet TABLE (Platform varchar(100),AvgByBet varchar(MAX));

		  set @FINAL_QUERY='SELECT * FROM OPENQUERY(LNK_PowerGames_Live,''
		SELECT
		''''PowerGame'''' AS Platform,
		CAST((SUM(G.bet * G.PointValue)/COUNT(*)) as decimal(38,2)) AS AvgByBet
		from games as G where G.status=1 AND G.IsDemo=0 AND G.IsCancelled=0 AND G.IsBot=0 
		AND (G.created_at BETWEEN '''''+CONVERT(VARCHAR,@FROMDATE,120)+''''' AND '''''+CONVERT(VARCHAR,@TODATE,120)+''''')'')'

		 insert into @TmpAvgByBet EXEC(@FINAL_QUERY)

		 
		  set @FINAL_QUERY = 'SELECT ''Supernowa Virtual'' AS Platform,
	        ((SUM(ISNULL(B.BetAmount, 0)) * -1)/COUNT(B.BetID)) AS AvgByBet
	    FROM [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.Game G
	    INNER JOIN [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) );'

	 insert into @TmpAvgByBet EXEC(@FINAL_QUERY)

	   set @FINAL_QUERY = 'SELECT ''Supernowa Dealer'' AS Platform,
	    ((SUM(ISNULL(B.BetAmount, 0)) * -1)/COUNT(B.BetID)) AS AvgByBet
	    FROM [LNK_Veronica_live].[Veronica_Live].dbo.Game G
	    INNER JOIN [LNK_Veronica_live].[Veronica_Live].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN [LNK_Veronica_live].[Veronica_Live].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) );'

		 insert into @TmpAvgByBet EXEC(@FINAL_QUERY)


		 SELECT * FROM @TmpAvgByBet;
		
	END
	ELSE IF(@SelectedOption = 'AvgByPlayer')
	BEGIN


		DECLARE @TmpAvgByPlayer TABLE (Platform varchar(100),AvgByPlayer varchar(MAX));

		  set @FINAL_QUERY='SELECT * FROM OPENQUERY(LNK_PowerGames_Live,''
		SELECT
		''''PowerGame'''' AS Platform,
		CAST((SUM(G.bet * G.PointValue)/COUNT(DISTINCT G.ClientUserID)) as decimal(38,2)) AS AvgByPlayer
		from games as G where G.status=1 AND G.IsDemo=0 AND G.IsCancelled=0 AND G.IsBot=0 
		AND (G.created_at BETWEEN '''''+CONVERT(VARCHAR,@FROMDATE,120)+''''' AND '''''+CONVERT(VARCHAR,@TODATE,120)+''''')'')'

		 insert into @TmpAvgByPlayer EXEC(@FINAL_QUERY)

		 
		  set @FINAL_QUERY = 'SELECT ''Supernowa Virtual'' AS Platform,
	        ((SUM(ISNULL(B.BetAmount, 0)) * -1)/COUNT(DISTINCT B.ClientUserID)) AS AvgByPlayer
	    FROM [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.Game G
	    INNER JOIN [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) );'

	 insert into @TmpAvgByPlayer EXEC(@FINAL_QUERY)

	   set @FINAL_QUERY = 'SELECT ''Supernowa Dealer'' AS Platform,
	    ((SUM(ISNULL(B.BetAmount, 0)) * -1)/COUNT(DISTINCT B.ClientUserID)) AS AvgByPlayer
	    FROM [LNK_Veronica_live].[Veronica_Live].dbo.Game G
	    INNER JOIN [LNK_Veronica_live].[Veronica_Live].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN [LNK_Veronica_live].[Veronica_Live].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) );'

		 insert into @TmpAvgByPlayer EXEC(@FINAL_QUERY)


		 SELECT * FROM @TmpAvgByPlayer;

	END
	ELSE IF(@SelectedOption = 'NoOfBetCount')
	BEGIN

	    
		 DECLARE @TmpBetNumber TABLE (Platform varchar(100),TotalBet varchar (250))

		  set @FINAL_QUERY='SELECT * FROM OPENQUERY(LNK_PowerGames_Live,''
		SELECT ''''PowerGame'''' AS Platform,
		COUNT(*) as TotalBet
		from games as G where G.status=1 AND G.IsDemo=0 AND G.IsCancelled=0 AND G.IsBot=0 
		AND (G.created_at BETWEEN '''''+CONVERT(VARCHAR,@FROMDATE,120)+''''' AND '''''+CONVERT(VARCHAR,@TODATE,120)+''''')'')'

		 insert into @TmpBetNumber EXEC(@FINAL_QUERY)


	     set @FINAL_QUERY = 'SELECT ''Supernowa Virtual'' AS Platform,
	        COUNT(B.BetID) AS TotalBet
	    FROM [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.Game G
	    INNER JOIN [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN [LNK_SuperNowa_Virtual].[Supernowa_Virtual].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) );'
	
		 insert into @TmpBetNumber EXEC(@FINAL_QUERY)

		 	     set @FINAL_QUERY = 'SELECT ''Supernowa Dealer'' AS Platform,
	        COUNT(B.BetID) AS TotalBet
	    FROM [LNK_Veronica_live].[Veronica_Live].dbo.Game G
	    INNER JOIN [LNK_Veronica_live].[Veronica_Live].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN [LNK_Veronica_live].[Veronica_Live].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) );'
	
		 insert into @TmpBetNumber EXEC(@FINAL_QUERY)

		 select * from @TmpBetNumber
	END
	ELSE
	BEGIN
	     set @FINAL_QUERY = 'SELECT  '''' Platform, 0 TotalBet, 0 AvgByPlayer, 0 AvgByBet;'
		 EXEC (@FINAL_QUERY)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetPieChartGamesWise]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPieChartGamesWise] 
      @LinkServerName VARCHAR(100)='LNK_SuperNowa_Virtual',
      @DBName VARCHAR(100)='Supernowa_Virtual',
	  @SelectedOption VARCHAR(100)='else'
AS
BEGIN

SET NOCOUNT ON;

--AND CONVERT(VARCHAR(10),DATEADD(Mi,+330,R.CreatedDate), 105) = CONVERT(VARCHAR(10), getdate(), 105) 
--AND (R.CreatedDate BETWEEN DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)) AND DATEADD(DAY,+1,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME))) )

DECLARE  @FINAL_QUERY VARCHAR(max)='';

DECLARE @FROMDATEUTC datetime=DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME));
DECLARE @TODATEUTC datetime=DATEADD(SECOND,-1,DATEADD(DAY,+1,DATEADD(MINUTE,-330,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME))));

IF(@SelectedOption = 'PowerGame')
	BEGIN

	DECLARE @FROMDATE datetime=CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME);
    DECLARE @TODATE datetime=DATEADD(SECOND,-1,DATEADD(DAY,+1,CAST(CONVERT(VARCHAR,GETDATE(),102) AS DATETIME)));

		 set @FINAL_QUERY='SELECT * FROM OPENQUERY(LNK_PowerGames_Live,''
		SELECT
		G.gameable_type as Name,
		COUNT(*) as TotalBet,
		CAST((SUM(G.bet * G.PointValue)/COUNT(DISTINCT G.ClientUserID)) as decimal(38,2)) AS AvgByPlayer,
        CAST((SUM(G.bet * G.PointValue)/COUNT(*)) as decimal(38,2)) AS AvgByBet
		from games as G where G.status=1 AND G.IsDemo=0 AND G.IsCancelled=0 AND G.IsBot=0 
		AND (G.created_at BETWEEN '''''+CONVERT(VARCHAR,@FROMDATE,120)+''''' AND '''''+CONVERT(VARCHAR,@TODATE,120)+''''') 
		GROUP BY G.gameable_type
		ORDER BY AvgByBet DESC'')'


END
ELSE IF(@SelectedOption = 'AvgByBet')
	BEGIN

	     set @FINAL_QUERY = 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,  
	        ((SUM(ISNULL(B.BetAmount, 0)) * -1)/COUNT(B.BetID)) AS AvgByBet
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) )
	    GROUP BY G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.AvgByBet DESC, #TempTable.GameID;'
		
	END
	ELSE IF(@SelectedOption = 'AvgByPlayer')
	BEGIN

	     set @FINAL_QUERY = 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,
	        ((SUM(ISNULL(B.BetAmount, 0)) * -1)/COUNT(DISTINCT B.ClientUserID)) AS AvgByPlayer
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) )
	    GROUP BY G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.AvgByPlayer DESC, #TempTable.GameID;'

	END
	ELSE IF(@SelectedOption = 'NoOfBetCount')
	BEGIN

	     set @FINAL_QUERY = 'SELECT * FROM (
		SELECT
			G.GameID,
			G.Name,
	        COUNT(B.BetID) AS TotalBet
	    FROM ['+ @LinkServerName +'].['+ @DBName +'].dbo.Game G
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.RoundSummary R ON R.GameID = G.GameID AND R.IsDelete = 0 AND R.IsRoundOver = 1 AND R.IsSettled = 1 AND R.IsActive = 1
	    INNER JOIN ['+ @LinkServerName +'].['+ @DBName +'].dbo.Bet B ON B.RoundSummaryID = R.RoundSummaryID AND B.IsDelete = 0 AND B.IsActive = 1  AND B.IsDemoBet = 0
		WHERE G.IsDelete = 0 AND (R.CreatedDate BETWEEN CAST(''' + cast(CONVERT(datetime,@FROMDATEUTC,101) as varchar)  +''' AS DATETIME) AND CAST(''' + cast(CONVERT(datetime,@TODATEUTC,101) as varchar) +''' AS DATETIME) )
	    GROUP BY G.Name,G.GameCode,G.GameID,G.IsLive,G.IsActive,G.IsStop,G.ImagePath) #TempTable
		ORDER BY #TempTable.TotalBet DESC, #TempTable.GameID;'

	END
	ELSE
	BEGIN
	     set @FINAL_QUERY = 'SELECT 0 GameID, '''' Name, 0 TotalBet, 0 AvgByPlayer, 0 AvgByBet;'
	END

EXEC (@FINAL_QUERY)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSatSportBetList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetSatSportBetList] 
      @LinkServerName VARCHAR(100)='LNK_SATSPORT247',
      @DBName VARCHAR(100)='Satsport247_Live',
	  @MarketID varchar(100)='9308'
AS
BEGIN

DECLARE  @FINAL_QUERY VARCHAR(max)='';
/*set @FINAL_QUERY=' SELECT BT.BetID,
				BT.CreatedDate as BetTaken,
				BT.Runner as Selection,
				BT.IsBack,
				BT.IsMobileBet,
				BT.Exposure,
				BT.IsFancy,
				BT.IsBetWon,
				MAR.IsSettled,
				S.[Name] as SportName,
				MAR.MarketType,
				MAR.[Name] as MarketName,
				MAT.[Name] as MatchName,
				T.[Name] as TournamentName,
				U.Username as Users,
				BT.IsDelete,
				BT.Stake,
				BT.Rate,
				BT.SuperAdminID,
				BT.AdminID,
				BT.SuperMasterID,
				BT.MasterID,
				BT.PlayerID,
				BT.MarketID,
				MAT.MatchID,
				MAT.TournamentID,
				S.SportID,
				BT.IpAddress,
				MAR.CentralID
				FROM ['+@LinkServerName+'].['+@DBName+'].[dbo].[Sport] AS S
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Tournament] AS T
				ON S.SportID=T.SportID
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Match] AS MAT
				ON T.TournamentID=MAT.TournamentID
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Market] AS MAR 
				ON MAT.MatchID=MAR.MatchID 
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Bet] AS BT
				ON BT.MarketID=MAR.MarketID 
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[User] AS U
				ON BT.PlayerID=U.UserID
				WHERE MAR.MarketID='+@MarketID;*/


				set @FINAL_QUERY=' SELECT BT.BetID,
				BT.CreatedDate as BetTaken,
				BT.Runner as Selection,
				BT.IsBack,
				BT.IsMobileBet,
				BT.Exposure,
				MAR.IsFancy,
				BT.IsBetWon,
				MAR.IsSettled,
				BT.BetPL,
				S.[Name] as SportName,
				MAR.MarketType,
				MAR.[Name] as MarketName,
				MAT.[Name] as MatchName,
				T.[Name] as TournamentName,
				U.Username as Users,
				BT.Run,
				BT.IsDelete,
				BT.Stake,
				BT.Rate,
				BT.SuperAdminID,
				BT.AdminID,
				BT.SuperMasterID,
				BT.MasterID,
				BT.PlayerID,
				BT.MarketID,
				MAT.MatchID,
				MAT.TournamentID,
				S.SportID,
				BT.RunnerPL,
				BT.RunnerId,
				BT.IpAddress,
				CAST(''1'' as BIT) AS IsMatchBet,
				IIF(ISNULL(MAR.Result, '''') = '''',''Not Settled/ Not Declaired'', IIF(MAR.Result = ''-999'',''No Result/Cancel'', IIF(MAR.Result = ''-888'',''Match Tie'',IIF(MAR.MarketType NOT IN (''advancesession'',''session'', ''line''),ISNULL(Run.[Name],''No Result''), MAR.Result)))) Winner,
				downline.PlayerPoint,
				(BT.BetPL * downline.PlayerPoint) AS INR,
				MAT.OpenDate AS OpenDate,
				MAR.CentralID
				FROM ['+@LinkServerName+'].['+@DBName+'].[dbo].[Sport] AS S
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Tournament] AS T
				ON S.SportID=T.SportID
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Match] AS MAT
				ON T.TournamentID=MAT.TournamentID
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Market] AS MAR 
				ON MAT.MatchID=MAR.MatchID 
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Bet] AS BT
				ON BT.MarketID=MAR.MarketID 
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Downline] downline 
				ON BT.PlayerID = downline.PlayerID
				INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[User] AS U
				ON BT.PlayerID=U.UserID
				OUTER APPLY (SELECT TOP(1) R.Name,R.BfRunnerID  FROM ['+@LinkServerName+'].['+@DBName+'].[dbo].[Runner] R WHERE R.BfRunnerID = MAR.Result AND R.IsActive = 1  AND R.IsDelete = 0) as Run
				WHERE BT.IsDelete=0 AND BT.IsActive=1 AND MAR.MarketID='+@MarketID+'ORDER BY BT.CreatedDate DESC';

EXEC (@FINAL_QUERY)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSatSportMarketList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetSatSportMarketList] 
      @LinkServerName VARCHAR(100)='LNK_SATSPORT247',
      @DBName VARCHAR(100)='Satsport247_Live'
AS
BEGIN

DECLARE  @FINAL_QUERY VARCHAR(max)='';

set @FINAL_QUERY='SELECT M.MarketID,
         M.MatchID,
		 M.CentralID,
		 M.[Name] AS MarketName,
		 MT.[Name] As MatchName,
		 M.IsInPlay
	     FROM ['+@LinkServerName+'].['+@DBName+'].[dbo].[Market] AS M INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Match] AS MT
		 ON M.MatchID=MT.MatchID
	     WHERE M.IsInPlay=1 AND M.IsActive=1 AND M.IsDelete=0
		 ORDER BY M.IsInPlay DESC';


EXEC (@FINAL_QUERY)

END

--AND M.IsInPlay=1 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSatSportPlatformList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[USP_GetSatSportPlatformList]
AS
BEGIN 
	  Select SSID,SSName,SSLinkServerName,SSDBName from SatSportPlatform;
END;
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSatSportRunnerList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetSatSportRunnerList] 
      @LinkServerName VARCHAR(100)='LNK_SATSPORT247',
      @DBName VARCHAR(100)='Satsport247_Live',
	  @MarketID varchar(100)='9308'
AS
BEGIN

DECLARE  @FINAL_QUERY VARCHAR(max)='';
set @FINAL_QUERY='SELECT R.RunnerID,R.[Name],R.BfRunnerID,M.MarketType 
                  FROM ['+@LinkServerName+'].['+@DBName+'].[dbo].[RUNNER] AS R
				  INNER JOIN ['+@LinkServerName+'].['+@DBName+'].[dbo].[Market] AS M
				  ON R.MarketID=M.MarketID
				  WHERE R.IsActive=1 
				  AND R.IsDelete=0
				  AND R.MarketID='+@MarketID;
EXEC (@FINAL_QUERY)
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSatSportWithdrawalRequestList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetSatSportWithdrawalRequestList] 
      @LinkServerName VARCHAR(100)='LNK_SATSPORT247',
      @DBName VARCHAR(100)='Satsport247_Live'
AS
BEGIN

DECLARE  @FINAL_QUERY VARCHAR(max)='';

set @FINAL_QUERY='SELECT 
			UW.UserWithdrawalId,
			UW.BankAccNo,
			UW.BankName,
			U.Username,
			UW.UserId,
			(SELECT SUM(W.CreditAmount) - SUM(W.DebitAmount) FROM ['+@LinkServerName+'].['+@DBName+'].dbo.Wallet W WHERE W.UserID =  UW.UserId) as Balance,
			UW.WithdrawalAmount,
			UW.Description,
			UW.AccountHolderName,
			UW.BankIFSC,
			UW.MobileNo,
            CASE
			WHEN UW.WithdrawalType = 1 THEN ''Bank''
			WHEN UW.WithdrawalType = 2 THEN ''Paytm''
			ELSE ''''
			END AS WithdrawalType,
			CASE
			WHEN UW.Status = 1 THEN ''Pending''
			WHEN UW.Status = 2 THEN ''Approved''
			WHEN UW.Status = 3 THEN ''Declined''
			WHEN UW.Status = 4 THEN ''Deleted''
			WHEN UW.Status = 5 THEN ''TpPending''
			ELSE ''''
			END AS Status,
			UW.DeclineReason,
			UW.CreatedDate ,
			IIF(UW.[Status] = 1, NULL, UW.ModifiedDate) as ModifiedDate
		FROM 
			['+@LinkServerName+'].['+@DBName+'].dbo.UserWithdrawal UW
			INNER JOIN ['+@LinkServerName+'].['+@DBName+'].dbo.[User] U ON U.UserID = UW.UserId AND U.IsDelete = 0
			INNER JOIN ['+@LinkServerName+'].['+@DBName+'].dbo.Downline D ON UW.UserId = D.PlayerID AND D.IsDelete = 0 AND D.IsActive = 1
		WHERE  D.AdminID IS NULL
			AND D.SuperMasterID IS NULL
			AND D.MasterID IS NULL
			AND UW.IsActive = 1 
			AND UW.IsDelete = 0';

EXEC (@FINAL_QUERY)
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetUserPermissionList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[USP_GetUserPermissionList] 
@userid int
AS
BEGIN

SET NOCOUNT ON;

SELECT * FROM UserPermission where UserID=@userid;

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetUsersList]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetUsersList] 
AS
BEGIN

SET NOCOUNT ON;

SELECT * FROM login WHERE isdelete=0;

END
GO
/****** Object:  StoredProcedure [dbo].[USP_INUPUser]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[USP_INUPUser]
	@userid bigint=0,
	@username   nvarchar(MAX),
	@userpassword nvarchar(MAX),
	@usertype int,
	@createdby int
	AS
BEGIN

    if exists (Select 1 from login where userid=@userid)
    begin

		update login set username=@username,userpassword=@userpassword,usertype=@usertype,modifiedbydatetime=GETDATE(),modifiedby=@createdby where userid=@userid;
   
   End
    Else
    Begin

		insert into login (username,userpassword,usertype,isactive,isdelete,createdby,createdbydatetime,modifiedby,modifiedbydatetime) values (@username,@userpassword,@usertype,1,0,@createdby,GETDATE(),@createdby,GETDATE());  --fire insert query

     End

End
GO
/****** Object:  StoredProcedure [dbo].[USP_INUPUserPermission]    Script Date: 6/29/2021 18:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_INUPUserPermission]
	@UserId bigint=0,
	@PageIds varchar(MAX)
	AS
BEGIN

    if exists (Select 1 from UserPermission where UserId=@UserId)
    begin
	 
	    UPDATE UserPermission SET PageIds=@PageIds WHERE UserId=@UserId;
		
   End
    Else
    Begin
	 
	   INSERT INTO UserPermission(PageIds,UserId) VALUES(@PageIds,@UserId);
		
     End

End
GO
USE [master]
GO
ALTER DATABASE [ReportingServiceLive] SET  READ_WRITE 
GO
