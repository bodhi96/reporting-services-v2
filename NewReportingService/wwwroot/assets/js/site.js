﻿
$(() => {
   
    window.LoadBetData = function () {
        debugger;
        $('#datatable_body').empty();
        $('#RunnerDiv').empty();

        var URL = '/RunningAnalysis/GetSatSportBetList';

        $('#datatable').dataTable({
            "processing": false,
            "serverSide": false,
            "bDestroy": true,
            "bJQueryUI": true,
            "pageLength": 20,
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "sEmptyTable": "Loading data from server",
            "dom": 'lBfrtip',
            "buttons": [
                'csv', 'excel'
            ],
            "ajax": {
                'type': 'POST',
                'url': URL,
                'data': {
                    "PlatformId": $("#ddlPlatform option:selected").val(),
                    "PlatformName": $("#ddlPlatform option:selected").text(),
                    "MarketID": $("#ddlMarket option:selected").val()
                },
            },
            "columns": [
                {
                    "render": function (data, type, row, meta) {
                        return (row[0])
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        return (row[1])
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        return (row[2])
                    }
                },
                {
                    "visible": false,
                    "render": function (data, type, row, meta) {
                        return (row[3])
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        return (row[4])
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        return (row[5])
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        return (row[6])
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        return (row[7])
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        return (row[8])
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        if (data >= 0)
                            return "<span class='green'>" + data + "</span>";
                        else
                            return "<span class='red'>" + data + "</span>";
                    }
                },
                {
                    "render": function (data, type, row, meta) {
                        return (row[10])
                    }
                },
            ],
            "initComplete": function () {

                var fdata = new FormData();
                fdata.append("PlatformId", $("#ddlPlatform option:selected").val());
                fdata.append("PlatformName", $("#ddlPlatform option:selected").text());
                fdata.append("MarketID", $("#ddlMarket option:selected").val());

                $.ajax({
                    url: '/RunningAnalysis/GetSatSportRunnerPLList',
                    type: "POST",
                    contentType: false, // Not to set any content header
                    processData: false, // Not to process data
                    data: fdata,
                    success: function (response) {

                        if (response != "0" && response != null) {
                            var Div = "";

                            $.each(response, function () {
                                var clr = "";
                                (parseFloat(this.runnerPL) >= 0) ? clr = "green" : clr = "red";

                                Div = Div + '\
                                                    <div class="col-md-12">\
                                                <div class="card card-border card-purple widget-s-1">\
                                                           <div class="card-header"> </div>\
                                                            <div class="card-body">\
                                                            <div class="h2 '+ clr + ' mt-2">' + this.runnerPL + '</div>\
                                                            <span class="text-muted">Profit/Loss</span>\
                                                            <div class="text-purple fa-2x">'+ this.name + '\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>';

                            });
                            $('#RunnerDiv').append(Div);
                        }
                    },
                    error: function (err) {
                        debugger;
                        alert(err.statusText);
                    }
                });
            }
        });
    }
   
})
