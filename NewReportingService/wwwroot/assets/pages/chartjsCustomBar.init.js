/**
* Theme: Moltran - Responsive Bootstrap 4 Admin Dashboard
* Author: Coderthemes
* Chart Js Page
* 
*/

!function ($) {
    "use strict";

    var ChartJs = function () { };

    ChartJs.prototype.respChart = function respChart(selector, type, data, options) {

        var ctx = selector.get(0).getContext("2d");

        if (type == "Bar") {
            var myChart = new Chart(ctx, {
                type: 'bar',
                "data": data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: false
                        }
                    }
                }
            });
        }
        else if (type == "Line") {
            var myChart = new Chart(ctx, {
                type: 'line',
                "data": data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: false
                        }
                    }
                }
            });
        }
        else if (type == "Pie") {
            var myChart = new Chart(ctx, {
                type: 'pie',
                "data": data,
                options : {
                    "responsive": true,
                    "maintainAspectRatio": false
                }
            });
        }

        

    },
        $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs
}(window.jQuery);

//initializing 
/*function($) {
    "use strict";
    $.ChartJs.respChart($("#lineChart"), 'Line', '')
}(window.jQuery);*/