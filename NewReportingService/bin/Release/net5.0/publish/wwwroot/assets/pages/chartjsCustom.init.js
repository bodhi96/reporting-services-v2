/**
* Theme: Moltran - Responsive Bootstrap 4 Admin Dashboard
* Author: Coderthemes
* Chart Js Page
* 
*/

!function ($) {
    "use strict";

    var ChartJs = function () { };

    ChartJs.prototype.respChart = function respChart(selector, type, data, options) {
        // get selector by context

        var ctx = selector.get(0).getContext("2d");

        // pointing parent container to make chart js inherit its width
      //  var container = $(selector).parent();

        // enable resizing matter
       // $(window).resize(generateChart);

        // this function produce the responsive Chart JS
        function generateChart() {
            // make chart width fit with its container
          //  var ww = selector.attr('width', $(container).width());
            switch (type) {
                case 'Line':
                    new Chart(ctx).Line(data, options);
                    break;
                case 'Doughnut':
                    new Chart(ctx).Doughnut(data, options);
                    break;
                case 'Pie':
                    new Chart(ctx).Pie(data, options);
                    break;
                case 'Bar':
                    new Chart(ctx).Bar(data, options);
                    break;
                case 'Radar':
                    new Chart(ctx).Radar(data, options);
                    break;
                case 'PolarArea':
                    new Chart(ctx).PolarArea(data, options);
                    break;
            }
            // Initiate new chart or Redraw

        };
        // run function - render chart at first load
       generateChart();
    },
        $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs
}(window.jQuery);

//initializing 
/*function($) {
    "use strict";
    $.ChartJs.respChart($("#lineChart"), 'Line', '')
}(window.jQuery);*/