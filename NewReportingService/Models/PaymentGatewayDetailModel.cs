﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class PaymentGatewayDetailModel
    {
        
       public int Id { get; set; }
        public string AgentName { get; set; }
        
        public int TotalTransaction { get; set; }
        public decimal TotalDepositAmount { get; set; }
        public decimal TotalFailedAmount { get; set; }
        public int TotalDepositTransaction { get; set; }
        public int TotalFailedTransaction { get; set; }





    }
}
