﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class MatchModel
    {
        public int MatchID { get; set; }
        public int TournamentID { get; set; }
        public int BfMatchID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public DateTime? OpenDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string TournamentName { get; set; }
        public int SportID { get; set; }
        public string SportName { get; set; }

        //public int? MinStake { get; set; }
        //public int? MaxStake { get; set; }
        //public decimal? MaxProfit { get; set; }
        public string StreamingUrl { get; set; }

    }
}
