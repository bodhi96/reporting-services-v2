﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class SupernowaBetList
    {
        public int BetID { get; set; }

        public string GameCode { get; set; }
        public bool IsSettled { get; set; }
        public int RoundSummaryID { get; set; }
        public double Rate { get; set; }
        public int Number { get; set; }
        public int Stake { get; set; }
        public bool IsBack  {get;set;}

           public double CR { get; set; }
           public double DR { get; set; }
           public bool IsBetWon { get; set; }
           public string Runner { get; set; }
           public DateTime CreatedDate { get; set; }
            public string GameName { get; set; }
		  public string betDetails { get; set; }
         public double  BetPL { get; set; }
          public string ClientName { get; set; }
           public string AgentName { get; set; }
           public int AgentUserId { get; set; }
            public string  Cards { get; set; }
           public string Winner { get; set; }
           public string WinningHand { get; set; }
           public DateTime RoundCreatedDate { get; set; }
           public DateTime RoundEndDate { get; set; }
            public string IP4 { get; set; }
		   public int ClientUserID { get; set; }

        public string WinOrLoss { get; set; }
    }
}
