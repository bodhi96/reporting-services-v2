﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class GameDdlModel
    {

        public int GameID { get; set; }

        public string GameCode { get; set; }

        public string Name { get; set; }
    }
}
