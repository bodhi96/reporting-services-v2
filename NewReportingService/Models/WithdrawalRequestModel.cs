﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
	public  class WithdrawalRequestModel
	{
	public string userWithdrawalId { get; set; }
		public string bankAccNo { get; set; }
		public string bankName { get; set; }
		public string username { get; set; }
		public string userId { get; set; }
		public string balance { get; set; }
		public string WithdrawalAmount { get; set; }
		public string description { get; set; }
		public string accountHolderName { get; set; }
		public string bankIFSC { get; set; }
		public string mobileNo { get; set; }
		public string withdrawalType { get; set; }
		public string status { get; set; }
		public string declineReason { get; set; }
		
		
		public DateTime createdDate { get; set; }
		public DateTime modifiedDate { get; set; }
	}
}
