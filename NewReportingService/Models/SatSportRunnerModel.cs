﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class SatSportRunnerModel
    {
        public int RunnerID { get; set; }
        public string Name { get; set; }
        public decimal RunnerPL { get; set; }
        public int BfRunnerID { get; set; }

        public string MarketType { get; set; }
        
    }
}
