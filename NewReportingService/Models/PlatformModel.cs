﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class PlatformModel
    {
        public int id { get; set; }
        public string PlatName { get; set; }
        public bool isdelete { get; set; }
        public string LinkServerName { get; set; }
        public string DBName { get; set; }

    }
}

