﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class DailyReportMeta
    {

        public decimal CreditAmount { get; set; }
        public decimal DebitAmount { get; set; }

        public int UserCount { get; set; }
    }
}
