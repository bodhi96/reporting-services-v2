﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class InvoiceViewModel
    {

        public List<InvoicePLModel> invoice { get; set; }
        public string invoicedate { get; set; }
    }
}
