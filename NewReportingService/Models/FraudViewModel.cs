﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class FraudViewModel
    {
        public List<FraudDetail> details { get; set; }

        public FraudDetectionClass FraudDetectionClass { get; set; }

        public List<FraudDetail1> details1 { get; set; }
    }
}
