﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class GroupListModel
    {
       public int  GroupMasterId { get; set; }
        public string GroupName { get; set; }
       
        public string AgentUserNames { get; set; }
        public string AgentUserIds { get; set; }
        

         
    }
}
