﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class UserPermissionModel
    {
        public int UpId { get; set; }
        public string PageIds { get; set; }
        public int UserId { get; set; }
    }
}
