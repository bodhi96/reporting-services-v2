﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class MatchlistFulltozModel
    {
        public int MatchID { get; set; }
        public string collection_name { get; set; }
        public string MatchName { get; set; }
    }
    public class SportListFulltozModel
    {
      public int  SportsID { get; set; }
      public string SportName { get; set; }

    }
}
