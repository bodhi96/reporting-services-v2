﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class PartnerModel
    {
        public int PartnerID { get; set; }
        public string Name { get; set; }
    }
}
