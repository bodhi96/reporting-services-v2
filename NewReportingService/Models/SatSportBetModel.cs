﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class SatSportBetModel
    {
		public int BetID { get; set; }
		public DateTime BetTaken { get; set; }
		public string Selection { get; set; }
		
		public bool IsBack { get; set; }
		public bool IsMobileBet { get; set; }
		public string SSName { get; set; }
		public string Exposure { get; set; }

		public bool IsFancy { get; set; }
		public bool IsBetWon { get; set; }

		public bool IsSettled { get; set; }

		public string SportName { get; set; }

		public string MarketType { get; set; }
		public string MarketName { get; set; }
		public string MatchName { get; set; }
		public string TournamentName { get; set; }
		public string Users { get; set; }

		public bool IsDelete { get; set; }

		public string Stake { get; set; }
		public string Rate { get; set; }

		public int SuperAdminID { get; set; }

		public int AdminID { get; set; }

		public int SuperMasterID { get; set; }

		public int MasterID { get; set; }

		public int PlayerID { get; set; }

		public int MarketID { get; set; }

		public int MatchID { get; set; }

		public int TournamentID { get; set; }

		public int SportID { get; set; }

		public string IpAddress { get; set; }

		public int CentralID { get; set; }

		public string BetPL { get; set; }

		public string Run { get; set; }

		public bool IsMatchBet { get; set; }
		public string Winner { get; set; }
		public string PlayerPoint { get; set; }
		public string INR { get; set; }
		public DateTime OpenDate { get; set; }
		public string RunnerPL { get; set; }
		public int RunnerId { get; set; }

	}
	public class SatSportBetListModel
	{
		public int BetID { get; set; }
		public DateTime BetTaken { get; set; }
		public string Selection { get; set; }

		public string SSName { get; set; }
		public string Users { get; set; }
		public bool IsBack { get; set; }
		public bool IsFancy { get; set; }
		public string MarketType { get; set; }
		public string Stake { get; set; }
		public string Rate { get; set; }
		public string MatchName { get; set; }
		public int MarketID { get; set; }
		public string RunnerPL { get; set; }
		public int RunnerId { get; set; }

		public string PL { get; set; } = string.Empty;
		public string FormattedBetTaken { get; set; } = string.Empty;

	}

	public class SatSportRunnerPLModel
    {
		public int RunnerId { get; set; } = 0;
		public decimal RunnerPL { get; set; } = 0;
		public string UnmatchedPL { get; set; } = string.Empty;
		public decimal YesPL { get; set; } = 0;
		public decimal NoPL { get; set; } = 0;

	}
}
