﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class FraudDetectionClass
    {
 public int appCheatBetID { get; set; }
public int appSiteID { get; set; }
public int appBetID { get; set; }
public double appBFCentralID { get; set; }
public int appSportId { get; set; }
public int publiappBFSportID { get; set; }
public int appTournamentID { get; set; }
public int appMatchID { get; set; }
public int appBFMatchID { get; set; }
public int appMarketId { get; set; }
public int appBFMarketID { get; set; }
public string appMarketType { get; set; }
public string appSportName { get; set; }
public string appTournamentName { get; set; }
public string appMarketName { get; set; }
public string appMatchName { get; set; }
public int appRunnerID { get; set; }
public int appBFRunnerID { get; set; }
public string appRunnerName { get; set; }
public DateTime appBetDate { get; set; }
public bool appIsBack { get; set; }
public double appRate { get; set; }
public double appStakeAmount { get; set; }
public int appTimeVariationUpper { get; set; }
public int appTimeVariationLower { get; set; }
public double appCheckBestRate { get; set; }
 public double  appAllowedVariationLimit { get; set; }
public double appMaxVariationFound { get; set; }
public double appBeforeVariationFound { get; set; }
public double appAfterVariationFound { get; set; }
public bool appIsValid { get; set; }
public int appValidType { get; set; }
    }
}
