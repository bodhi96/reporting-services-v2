﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class AgentPLModel
    {
        public int UserID { get; set; }
        public string AgentName { get; set; }
        public string PROFITLOSS { get; set; }

        public string BetAmount { get; set; }
    }

    public class AgentPLAModel
    {
       
        public string Partner { get; set; }
        public double GGR { get; set; }

       // public double BetAmount { get; set; }
    }
    public class AgentPLViewModel
    {
        public List<AgentPLAModel> agentPLModelList { get; set; }
        public double tprofit { get; set; }
        public double tloss { get; set; }
        public double netpl { get; set; }

        public string type { get; set; }
    }
    public class AgentPLModelMetadata
    {
        public int TotalRecords { get; set; }
        public double TotaProfiTotalProfitLoss { get; set; }
        public double TotalProfit { get; set; }
        public double TotalLoss { get; set; }
        public double NetPL { get; set; }


    }
}
