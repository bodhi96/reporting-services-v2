﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class GroupListViewModel
    {
        public List<GroupListModel> groupLists { get; set; }

        public DateTime invoicedate { get; set; }
    }
}
