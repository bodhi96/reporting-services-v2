﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class ReportViewModel
    {
        public List<ReportingDataViewModel> AList { get; set; }
        public List<ReportingDataViewModel> MList { get; set; }

        public string profit { get; set; }
    }
    public class ReportingDataViewModel
    {
        public string CN { get; set; }
        public string RUrl { get; set; }
        public string Status { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
