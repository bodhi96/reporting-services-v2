﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class PaymentGViewModel
    {
        public List<PaymentGatewayDetailModel> pgmodel { get; set; }
    }
}
