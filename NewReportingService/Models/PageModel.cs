﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class PageModel
    {
        public int Pid { get; set; }
        public string Pname { get; set; }
        public string PViewname { get; set; }
    }
}
