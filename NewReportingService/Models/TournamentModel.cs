﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class TournamentModel
    {
        public int TournamentID { get; set; }
        public int SportID { get; set; }
        public string SportName { get; set; }
        public int BfTournamentID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int MatchoddsMinStake { get; set; }
        public int MatchoddsMaxStake { get; set; }
        public decimal MatchoddsMaxProfit { get; set; }
        public int BookmakersMinStake { get; set; }
        public int BookmakersMaxStake { get; set; }
        public decimal BookmakersMaxProfit { get; set; }
        public int FancyMinStake { get; set; }
        public int FancyMaxStake { get; set; }
        public decimal FancyMaxProfit { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
