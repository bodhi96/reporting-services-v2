﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class InvoicePLModel
    {
      public string AgentName { get; set; }
       public int     RevShare { get; set; }
         public int TotalBet { get; set; }
        public double TotalBetAmount { get; set; }
          public double TotalProfitLoss { get; set; }
    }
}
