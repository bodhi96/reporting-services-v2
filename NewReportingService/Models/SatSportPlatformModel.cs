﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class SatSportPlatformModel
    {
        public int SSID { get; set; }
        public string SSName { get; set; }
        public string SSLinkServerName { get; set; }
        public string SSDBName { get; set; }
    }

    public class SignalRPlatformModel
    {
        public int Id { get; set; }
        public int SSID { get; set; }
        public string SignalrUrl { get; set; }
        public string SignalrMethod { get; set; }
    }
}
