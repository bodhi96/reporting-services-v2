﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class FraudDetail
    {
        public string RateID { get; set; }
        public string   CenterID { get; set; }
     
public int RunnerID { get; set; }
public string BackRateJson{ get; set; }
public string LayRateJson { get; set; }
public DateTime RateDate { get; set; }

        public string backrate1 { get; set; }
        public string backrate2 { get; set; }
        public string backrate3 { get; set; }

        public string backvolume1 { get; set; }
        public string backvolume2 { get; set; }
        public string backvolume3 { get; set; }

        public string layrate1 { get; set; }
        public string layrate2 { get; set; }
        public string layrate3 { get; set; }

        public string layvolume1 { get; set; }
        public string layvolume2 { get; set; }
        public string layvolume3 { get; set; }


    }

    public class FraudDetail1
    {

public int centerID { get; set; }
public int  runnerID { get; set; }
public bool appIsBack { get; set; }
public DateTime createdDate { get; set; }
public int appRank { get; set; }
public double appVariation { get; set; }

    }

   
}
