﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class UserModel
    {

        public int UserID { get; set; }
        public string Username { get; set; }
    }

    public class UserViewModel
    {
        public string label { get; set; }
        public int val { get; set; }
    }
}
