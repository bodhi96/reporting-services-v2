﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class SatSportMarketModel
    {
        public int MarketID { get; set; }
        public string BfMarketID { get; set; }
        public int MatchID { get; set; }
        public int CentralID { get; set; }
        public string MatchName { get; set; }
        public bool IsInPlay { get; set; }
        public string MarketName { get; set; }
    }
}
