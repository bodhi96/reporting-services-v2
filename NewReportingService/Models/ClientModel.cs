﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class ClientModel
    {
        public int UserID { get; set; }

        public int AgentUserID { get; set; }
        public string ClientName { get; set; }
    }
}
