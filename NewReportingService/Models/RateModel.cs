﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class RateModel
    {
       public List<BackRate> BackRate { get; set; }
        public List<LayRate> LayRate { get; set; }
    }
    public class BackRate
    {
        public double rate { get; set; }
        public double volume { get; set; }
    }
    public class LayRate
    {
        public double rate { get; set; }
        public double volume { get; set; }
    }
}
