﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class LoginModel
    {
        public int userid { get; set; }
        public string username { get; set; }

        public string userpassword { get; set; }

        public int usertype { get; set; }

        public bool isactive { get; set; }
        public bool isdelete { get; set; }

        public int createdby { get; set; }

        public int modifiedby { get; set; }

        public string PageIds { get; set; }

        public DateTime createdbydatetime { get; set; }

        public DateTime modifiedbydatetime { get; set; }

    }
}
