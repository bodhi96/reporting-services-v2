﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class DailyReportModel
    {

       
        //public int sportID { get; set; }
        public string sportname { get; set; }
        public int betcount { get; set; }
        public int betvolume { get; set; }
        public int usercount { get; set; }
        public double pL { get; set; }
        //public double pLINR { get; set; }
        public string createdDate { get; set; }

    }

    public class DailyReportViewModel
    {
       public List<DailyReportModel> dailylist { get; set; }
        public double creditamount { get; set; }
        public double debitamount { get; set; }

        public int usercount { get; set; }
    }
}

