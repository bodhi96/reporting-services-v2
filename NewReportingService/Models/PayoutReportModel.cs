﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class PayoutReportModel
    {
       
        public string Partnername { get; set; }
        public double GameVolume { get; set; }

        public double PartnerAmount { get; set; }
        public double AdminAmount { get; set; }
    }
}
