﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class GameModel
    {
        public int GameID { get; set; }
        public string GameCode { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }

        public int TotalBet { get; set; }

        public string TotalBetAmount { get; set; }

        public string TotalProfitLoss { get; set; }

        public string CreatedDate { get; set; }

        public string Platform { get; set; }

        public string AvgByPlayer { get; set; }

        public string AvgByBet { get; set; }

    }
}
