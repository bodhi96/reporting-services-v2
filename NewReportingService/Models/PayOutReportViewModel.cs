﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Models
{
    public class PayOutReportViewModel
    {

        public List<PayoutReportModel> payoutreport { get; set; }
    }
}
