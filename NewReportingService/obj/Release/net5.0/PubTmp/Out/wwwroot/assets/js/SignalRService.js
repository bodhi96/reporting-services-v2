﻿export class SignalRService {
  signalRConnection: signalR.HubConnection;

  connectSignalR(userId?): Promise<boolean> {
    return new Promise<boolean>((success, error) => {
      if (!this.signalRConnection) {
        this.signalRConnection =
          new signalR.HubConnectionBuilder()
            .withUrl(environment.signalUrl + (userId ? ('?UserId=' + userId) : ''))
            .withAutomaticReconnect([500, 1000, 2500, 5000])
            .build();
      }
      this.signalRConnection.start()
        .then(() => {
          this.onClose();
          success(true);
        }).catch((e) => {
          setTimeout(() => {
            this.connectSignalR(userId).then(() => { success(true); }).catch(() => {
              setTimeout(() => {
                this.connectSignalR(userId).then(() => { success(true); }).catch(() => { error(false); });
              }, 5000);
            });
          }, 5000);
        });
    });
  }

  onClose() {
    this.signalRConnection.onclose = () => {
      setTimeout(() => {
        this.connectSignalR();
      }, 5000);
    };
  }

  disconnectSignalR(): Promise<boolean> {
    return new Promise<boolean>((success, error) => {
      this.signalRConnection.stop()
        .then(() => { success(true); })
        .catch(() => { error(false); });
    });
  }

  emit(event: string, ...args): Promise<boolean> {
    return new Promise<boolean>((success, error) => {
      this.signalRConnection.send(event, ...args)
        .then(() => { success(true); })
        .catch(() => { error(false); });
    });
  }

  listen(event: string): Observable<any> {
    return new Observable<any>(observale => {
      this.signalRConnection.off(event);
      this.signalRConnection.on(event, (...data) => {
        observale.next(data);
      });
    });
  }

  stopListen(event: string): Promise<boolean> {
    return new Promise<boolean>((success, error) => {
      this.signalRConnection.off(event);
      success(true);
    });
  }

  invoke(method, ...args): Promise<any> {
    return this.signalRConnection.invoke(method, ...args);
  }
}
