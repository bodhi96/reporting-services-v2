﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using NewReportingService.Helper;
using NewReportingService.Models;
using NewReportingService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Controllers
{
    public class PayoutReportController : Controller
    {
        private readonly IConfiguration config;
        private readonly string connStr;

        Utility UtilityObj;

        //ErrorHelper errorhelper;
        public PayoutReportController(IConfiguration config)
        {
            this.config = config;
            connStr = config.GetConnectionString("DatabaseConnection");

            UtilityObj = new Utility(config);
        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "PayoutReport/index"))
            {
                ViewBag.header = "PayoutReport";
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        [HttpPost]
        public JsonResult getPayoutReport(DateTime StartDate, DateTime EndDate,string Agent,string Client,string Sport,string Match)
        {


            var HomeRepo = new PayoutRepository(connStr);


            var result = HomeRepo.GetPayOutReport(StartDate, EndDate,Agent,Client,Sport,Match);

            if (result.Item1.Count > 0)
            {





                var data = new PayOutReportViewModel()
                {
                    payoutreport= result.Item1


                };

                return Json(data);
            }
            else
            {
                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    data = ResultArrNull
                });
            }
        }
        public JsonResult GetAgentList()
        {

            var HomeRepo = new PayoutRepository(connStr);
           
                var result = HomeRepo.GetAgentList();
                if (result.Item1 == null)
                {
                    return Json("0");
                }
                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> AgentList = new List<SelectListItem>();
                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        AgentList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].PartnerID.ToString(),
                            Text = result.Item1[i].Name,

                        });
                        // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                    }

                    return Json(AgentList);
                

            }
           

            return Json("0");
        }

        public JsonResult GetClientList( string AgentUserId)
        {
            if (AgentUserId == "ALL")
            {
                return Json("0");
            }
            var payoutRepo = new PayoutRepository(connStr);
          

                var result = payoutRepo.GetClienttList( AgentUserId);
                if (result.Item1 == null)
                {
                    return Json("0");
                }
                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> AgentList = new List<SelectListItem>();
                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        AgentList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].ClientID.ToString(),
                            Text = result.Item1[i].ClientName,

                        });
                        // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                    }

                    return Json(AgentList);
                }

            
            
            return Json("0");
        }
        public JsonResult GetMatchList(string customcondition)
        {
            if (customcondition == "ALL")
            {
                return Json("0");
            }
            var payoutRepo = new PayoutRepository(connStr);


            var result = payoutRepo.GetMatchList(customcondition);
            if (result.Item1 == null)
            {
                return Json("0");
            }
            if (result.Item1.Count > 0)
            {
                List<SelectListItem> MatchList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    MatchList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].MatchID.ToString(),
                        Text = result.Item1[i].MatchName,

                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }

                return Json(MatchList);
            }



            return Json("0");
        }
        public JsonResult GetSportList()
        {

            var HomeRepo = new PayoutRepository(connStr);

            var result = HomeRepo.GetSportList();
            if (result.Item1 == null)
            {
                return Json("0");
            }
            if (result.Item1.Count > 0)
            {
                List<SelectListItem> SportList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    SportList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].SportsID.ToString(),
                        Text = result.Item1[i].SportName,

                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }

                return Json(SportList);


            }


            return Json("0");
        }
    }
}
