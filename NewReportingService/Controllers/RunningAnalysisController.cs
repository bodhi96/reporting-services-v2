﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using NewReportingService.Helper;
using NewReportingService.Models;
using NewReportingService.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Controllers
{
    public class RunningAnalysisController : Controller
    {
        private readonly IConfiguration config;
        private readonly string connStr;

        private static List<SatSportPlatformModel> SatSportPlatformList = new List<SatSportPlatformModel>();
        private static List<SignalRPlatformModel> SignalRPlatformList = new List<SignalRPlatformModel>();
        private static List<SatSportBetModel> SatSportBetList = new List<SatSportBetModel>();
        private static List<SatSportBetListModel> lstBet = new List<SatSportBetListModel>();

        Utility UtilityObj;
        string[] names;
        //ErrorHelper errorhelper;
        public RunningAnalysisController(IConfiguration config)
        {

            this.config = config;
            connStr = config.GetConnectionString("DatabaseConnection");
            UtilityObj = new Utility(config);
        }

        public ActionResult SatSport()
        {
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "RunningAnalysis/SatSport"))
            {
                ViewBag.header = "Exchange Live BetList";
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult SatSportDailyReport()
        {
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "RunningAnalysis/SatSportDailyReport"))
            {
                ViewBag.header = "Daily Report";
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public JsonResult getDailyReport(int PlatformId, string PlatformName, string SelectedOption, int UserId, DateTime StartDate, DateTime EndDate)
        {

            var HomeRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();


            // var ISTFromDate = TimeZoneInfo.ConvertTimeFromUtc(FromDate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
            //  var ISTToDate = TimeZoneInfo.ConvertTimeFromUtc(ToDate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));


            var result = HomeRepo.GetDailyReport(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, UserId, StartDate, EndDate, SelectedOption);
            DailyReportMeta profit = result.Item1;
            if (result.Item2 != null && result.Item2.Count > 0)
            {
               

                
                var data = new DailyReportViewModel()
                {
                    dailylist = result.Item2,
                    creditamount = Math.Round((double)profit.CreditAmount, 2),
                    debitamount = Math.Round((double)profit.DebitAmount, 2),
                    usercount = profit.UserCount,


                };

                return Json(data);

            }
            else
            {
                
                var data = new DailyReportViewModel()
                {
                    dailylist = new List<DailyReportModel>(),
                    creditamount = Math.Round((double)profit.CreditAmount, 2),
                    debitamount = Math.Round((double)profit.DebitAmount, 2),
                    usercount = profit.UserCount,


                };
                return Json(data);
            }




        }

        [HttpPost]
        public JsonResult getUserBySearch(string search)
        {
            Object obj = new object();
            var HomeRepo = new RunningAnalysisRepository(connStr);





            var result = HomeRepo.GetUserBySearch("", "", search);
            List<UserViewModel> customers = new List<UserViewModel>();
            foreach (var n in result.Item1)
            {
                UserViewModel cust = new UserViewModel();
                cust.label = n.Username;
                cust.val = n.UserID;
                customers.Add(cust);
            }


            return Json(customers);


        }


        public JsonResult GetSatSportPlatformList()
        {

            var RunningRepo = new RunningAnalysisRepository(connStr);

            var result = RunningRepo.GetSatSportPlatformList();

            SatSportPlatformList = result.Item1;

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> PlatformList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    PlatformList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].SSID.ToString(),
                        Text = result.Item1[i].SSName,
                    });
                }
                return Json(PlatformList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSignalRPlatformList()
        {

            var RunningRepo = new RunningAnalysisRepository(connStr);

            var result = RunningRepo.GetSignalRPlatformList();

            SignalRPlatformList = result.Item1;

            if (result.Item1.Count > 0)
            {
                return Json(SignalRPlatformList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetSatSportMarketList(DateTime FromDate, DateTime ToDate, int PlatformId, string PlatformName, string SportID, string TournamentID, string MatchID, string Fancy, string term)
        {
            if (TournamentID == "undefined")
            {
                TournamentID = "0";
            }
            if (MatchID == "undefined")
            {
                MatchID = "0";
            }
            if (TournamentID == null)
            {
                TournamentID = "0";
            }
            if (MatchID == null)
            {
                MatchID = "0";
            }


            var RunningRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = RunningRepo.GetSatSportMarketList(FromDate, ToDate, SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, SportID, TournamentID, MatchID, Fancy, term);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> MarketList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    MarketList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].MarketID.ToString() + "(" + result.Item1[i].CentralID + ")",
                        Text = result.Item1[i].MatchName + " > " + result.Item1[i].MarketName + " > (" + result.Item1[i].CentralID + ")",
                        Selected = result.Item1[i].IsInPlay,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(MarketList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportMarketListALL(DateTime FromDate, DateTime ToDate, string SportID, string TournamentID, string MatchID, string Fancy, string term)
        {

            if (term == null)
            {
                term = "";
            }
            if (TournamentID == null)
            {
                TournamentID = "0";
            }
            if (TournamentID == "undefined")
            {
                TournamentID = "0";
            }
            if (MatchID == "undefined")
            {
                MatchID = "0";
            }
            if (TournamentID == null)
            {
                TournamentID = "0";
            }
            if (MatchID == null)
            {
                MatchID = "0";
            }
            if (MatchID == null)
            {
                MatchID = "0";
            }
            var RunningRepo = new RunningAnalysisRepository(connStr);

            //  var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = RunningRepo.GetSatSportMarketListALL(FromDate, ToDate, SportID, TournamentID, MatchID, Fancy, term);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> MarketList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    MarketList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].BfMarketID.ToString() + "(" + result.Item1[i].CentralID + ")",
                        Text = result.Item1[i].MatchName + " > " + result.Item1[i].MarketName + " > (" + result.Item1[i].CentralID + ")",
                        Selected = result.Item1[i].IsInPlay,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(MarketList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportSportList(int PlatformId, string PlatformName)
        {

            var satsportRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportSportList(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> SportList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    SportList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].SportID.ToString(),
                        Text = result.Item1[i].Name,

                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(SportList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetSatSportSportListALL()
        {

            var satsportRepo = new RunningAnalysisRepository(connStr);



            var result = satsportRepo.GetSatSportSportList();

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> SportList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    SportList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].BfSportID.ToString(),
                        Text = result.Item1[i].Name,

                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(SportList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportTournamentList(DateTime FromDate, DateTime ToDate, int PlatformId, string PlatformName, string sportID, string term, string Offset)
        {

            if (term == null)
            {
                term = "";
            }

            var satsportRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportTournamentList(FromDate, ToDate, SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, sportID, term, Offset);
            int count = result.Item1.Count;

            if (result.Item1.Count > 0)
            {

                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {

                    TounamentList.Add(new SelectListItem
                    {

                        Value = result.Item1[i].TournamentID.ToString(),
                        Text = result.Item1[i].Name + "(" + result.Item1[i].OpenDate.ToString() + ")",
                        Selected = result.Item1[i].IsActive
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }

                //  return Ok(await names);
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetSatSportTournamentListALL(DateTime FromDate, DateTime ToDate, string sportID, string term, string Offset)
        {

            if (term == null)
            {
                term = "";
            }

            var satsportRepo = new RunningAnalysisRepository(connStr);



            var result = satsportRepo.GetSatSportTournamentList(FromDate, ToDate, sportID, term, Offset);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].BfTournamentID.ToString(),
                        Text = result.Item1[i].Name + "(" + result.Item1[i].OpenDate.ToString() + ")",
                        Selected = result.Item1[i].IsActive
                    }); ;
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportMatchList(DateTime FromDate, DateTime ToDate, int PlatformId, string PlatformName, string tournamentID, string term)
        {
            if (term == null)
            {
                term = "";
            }
            if (tournamentID == null)
            {
                tournamentID = "0";
            }
            var satsportRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportMatchList(FromDate, ToDate, SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, tournamentID, term);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].MatchID.ToString(),
                        Text = result.Item1[i].Name,
                        Selected = result.Item1[i].IsActive,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetSatSportMatchListALL(DateTime FromDate, DateTime ToDate, string tournamentID, string term)
        {
            if (term == null)
            {
                term = "";
            }
            if (tournamentID == null)
            {
                tournamentID = "0";
            }

            var satsportRepo = new RunningAnalysisRepository(connStr);



            var result = satsportRepo.GetSatSportMatchList(FromDate, ToDate, tournamentID, term);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].BfMatchID.ToString(),
                        Text = result.Item1[i].Name,
                        Selected = result.Item1[i].IsActive,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportBetList(int PlatformId, string PlatformName, string SelectedOption, string MarketID, string Offset, string SportID, string TournamentID, string MatchID, string Fancy, string FromDate, string ToDate)
        {
            if (TournamentID == "undefined")
            {
                TournamentID = "0";
            }
            if (MatchID == "undefined")
            {
                MatchID = "0";
            }
            if (TournamentID == null)
            {
                TournamentID = "0";
            }
            if (MatchID == null)
            {
                MatchID = "0";
            }
            var RunningRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = RunningRepo.GetSatSportBetList(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, MarketID, Offset, SportID, TournamentID, MatchID, Fancy, FromDate, ToDate, PlatformName, SelectedOption);

            SatSportBetList = result.Item1;

            if (result.Item1.Count > 0)
            {
                var ResultArra = from x in result.Item1
                                 select new[]
                                 {
                                Convert.ToString(x.BetID),
                                Convert.ToString(result.Item2 +"/"+x.Users+"/"+x.MatchName),
                                //   Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                                Convert.ToString(x.Selection),
                                // Convert.ToString(x.IsSettled),
                                //Convert.ToString(x.IsMatchBet),
                                //Convert.ToString(x.Run),
                                Convert.ToString(x.Rate),
                                Convert.ToString(x.Stake),
                                Convert.ToString(CalculatePL(x.RunnerId,x.RunnerPL,x.IsFancy,x.MarketType)),
                                string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.BetTaken),
                                Convert.ToString(x.IsBack),
                         };

                return Json(new
                {
                    aaData = ResultArra
                });
            }
            else
            {
                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    aaData = ResultArrNull
                });
            }

        }

        // EHTAN HUNT -> New method to display all bet list 
        public JsonResult GetSatSportBetListALL(int MarketID, string Offset, string SportID, string TournamentID, string MatchID, string Fancy)
        {
            if (TournamentID == "undefined")
            {
                TournamentID = "0";
            }
            if (MatchID == "undefined")
            {
                MatchID = "0";
            }
            if (TournamentID == null)
            {
                TournamentID = "0";
            }
            if (MatchID == null)
            {
                MatchID = "0";
            }

            var RunningRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.ToList();

            var result = RunningRepo.GetSatSportBetListALL(MarketID, Offset, SportID, TournamentID, MatchID, Fancy);

            lstBet = result.Item1;

            if (result.Item1.Count > 0)
            {
                var ResultArra = from x in result.Item1
                                 select new[]
                                 {
                                Convert.ToString(x.BetID),
                                Convert.ToString(x.SSName+"/"+x.Users+"/"+x.MatchName+"/"+x.MarketType),
                                Convert.ToString(x.Selection),
                                Convert.ToString(x.Rate),
                                Convert.ToString(x.Stake),
                                Convert.ToString(CalculatePL(x.RunnerId,x.RunnerPL,x.IsFancy,x.MarketType)),
                                string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.BetTaken),
                                Convert.ToString(x.IsBack),
                         };

                return Json(new
                {
                    aaData = ResultArra
                });
            }
            else
            {
                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    aaData = ResultArrNull
                });
            }

        }
        public JsonResult GetSatSportBetDetail(int Ssid, string BetId, string MarketId, string Offset, string SportID, string TournamentID, string MatchID, string Fancy)
        {
            if (TournamentID == "undefined")
            {
                TournamentID = "0";
            }
            if (MatchID == "undefined")
            {
                MatchID = "0";
            }
            if (TournamentID == null)
            {
                TournamentID = "0";
            }
            if (MatchID == null)
            {
                MatchID = "0";
            }

            var RunningRepo = new RunningAnalysisRepository(connStr);
            string linkServerName = "";
            string dbName = "";


            if (SatSportPlatformList.Any(x => x.SSID == Ssid))
            {
                var SelectedPlatform = SatSportPlatformList.Where(x => x.SSID == Ssid).SingleOrDefault();
                linkServerName = SelectedPlatform.SSLinkServerName;
                dbName = SelectedPlatform.SSDBName;

            }
            var result = RunningRepo.GetSatSportBetDetail(linkServerName, dbName, BetId, MarketId, Offset, SportID, TournamentID, MatchID, Fancy);

            if (result.Item1.Count > 0)
            {
                lstBet = result.Item1;
                foreach (SatSportBetListModel x in lstBet)
                {

                    x.PL = Convert.ToString(CalculatePL(x.RunnerId, x.RunnerPL, x.IsFancy, x.MarketType));
                    x.FormattedBetTaken = string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.BetTaken);
                    x.RunnerPL = "";
                }

                return Json(lstBet);
            }
            else
            {
                string[] ResultArrNull = new string[8];
                return Json(ResultArrNull);
            }
        }


        public decimal CalculatePL(int RunnerId, string RunnerPL, bool IsFancy, string MarketType)
        {
            if (IsFancy && MarketType != "Bookmakers")
            //if (IsFancy)
            {
                try
                {
                    SatSportRunnerPLModel dataObj = JsonConvert.DeserializeObject<SatSportRunnerPLModel>(RunnerPL);
                    decimal PL = 0 - (dataObj.YesPL);
                    return PL;
                }
                catch
                {
                    SatSportRunnerPLModel[] dataObj = JsonConvert.DeserializeObject<SatSportRunnerPLModel[]>(RunnerPL);
                    decimal PL = 0;
                    for (int i = 0; i < dataObj.Length; i++)
                    {
                        if (dataObj[i].RunnerId == RunnerId)
                        {
                            PL = 0 - (dataObj[i].RunnerPL);
                        }
                    }
                    return PL;
                }
            }
            else
            {
                SatSportRunnerPLModel[] dataObj = JsonConvert.DeserializeObject<SatSportRunnerPLModel[]>(RunnerPL);
                decimal PL = 0;
                for (int i = 0; i < dataObj.Length; i++)
                {
                    if (dataObj[i].RunnerId == RunnerId)
                    {
                        PL = 0 - (dataObj[i].RunnerPL);
                    }
                }
                return PL;
            }
        }

        public JsonResult GetSatSportRunnerPLList(int PlatformId, string PlatformName, string MarketID)
        {
            var RunningRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = RunningRepo.GetSatSportRunnerList(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, MarketID);

            if (result.Item1.Count > 0)
            {
                List<SatSportRunnerModel> RunnerList = result.Item1;

                for (int i = 0; i < RunnerList.Count; i++)
                {
                    int RunnerID = RunnerList[i].RunnerID;
                    decimal RunnerPL = 0;
                    for (int j = 0; j < SatSportBetList.Count; j++)
                    {
                        if (RunnerID == SatSportBetList[j].RunnerId)
                        {
                            RunnerPL = RunnerPL + CalculatePL(RunnerID, SatSportBetList[j].RunnerPL, SatSportBetList[j].IsFancy, SatSportBetList[j].MarketType);
                        }
                    }
                    RunnerList[i].RunnerPL = RunnerPL;
                }


                return Json(RunnerList);
            }
            else
            {
                return Json("0");
            }

        }



        public JsonResult GetSatSportRunnerPLListALL(string MarketID)
        {
            var RunningRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.ToList();

            var result = RunningRepo.GetSatSportRunnerListALL(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, MarketID);

            if (result.Item1.Count > 0)
            {
                List<SatSportRunnerModel> RunnerList = result.Item1;

                for (int i = 0; i < RunnerList.Count; i++)
                {
                    int RunnerID = RunnerList[i].RunnerID;
                    decimal RunnerPL = 0;
                    for (int j = 0; j < SatSportBetList.Count; j++)
                    {
                        if (RunnerID == SatSportBetList[j].RunnerId)
                        {
                            RunnerPL = RunnerPL + CalculatePL(RunnerID, SatSportBetList[j].RunnerPL, SatSportBetList[j].IsFancy, SatSportBetList[j].MarketType);
                        }
                    }
                    RunnerList[i].RunnerPL = RunnerPL;
                }


                return Json(RunnerList);
            }
            else
            {
                return Json("0");
            }

        }
    }
}
