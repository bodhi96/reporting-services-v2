﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NewReportingService.Helper;
using NewReportingService.Models;
using NewReportingService.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NewReportingService.Controllers
{
    public class HomeController : Controller
    {

        private readonly IConfiguration config;
        private readonly string connStr;

        private static List<PlatformModel> PlatformList = new List<PlatformModel>();
        private static List<GameDdlModel> GameList = new List<GameDdlModel>();
        private static List<AgentPLModel> AgentLIst = new List<AgentPLModel>();
        private static List<ClientModel> ClientLIst = new List<ClientModel>();
        private static List<SatSportPlatformModel> SatSportPlatformList = new List<SatSportPlatformModel>();

        Utility UtilityObj;

        //ErrorHelper errorhelper;
        public HomeController(IConfiguration config)
        {
            this.config = config;
            connStr = config.GetConnectionString("DatabaseConnection");

            UtilityObj = new Utility(config);
        }

        public ActionResult Index()
        {
            ViewBag.header = "Card View";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "")
            {
                if (UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/Index"))
                {
                    return View();
                }
                else
                {
                    List<string> ViewList = UtilityObj.UserPermissionPageList(HttpContext.Session.GetString("userid"));
                    if (ViewList.Count > 0)
                    {
                        string[] viewname = ViewList[0].Split("/");
                        return RedirectToAction(viewname[1], viewname[0]);
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult AgentPLReport()
        {
            ViewBag.header = "GGR";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/AgentPLReport"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult BetList()
        {
            ViewBag.header = "Bet List";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/BetList"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }


        public ActionResult Login()
        {//Admin@333
            HttpContext.Session.Clear();
            return View();
        }

        public ActionResult SecondDashboard()
        {
            ViewBag.header = "Line Chart";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/SecondDashboard"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        public ActionResult Invoice()
        {
            ViewBag.header = "Invoice";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/Invoice"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult ThirdDashboard()
        {
            ViewBag.header = "Pie Chart";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/ThirdDashboard"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult WithdrawalRequest()
        {
            ViewBag.header = "Withdrawal Request";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/WithdrawalRequest"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult AddUser()
        {
            ViewBag.header = "Add User";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/AddUser"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult AddUserRole()
        {
            ViewBag.header = "User Permission";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/AddUserRole"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public JsonResult CheckLogin(string Username, string Psw)
        {

            var HomeRepo = new HomeRepository(connStr);

            var result = HomeRepo.CheckLogin(Username, EncryptionDecryption.Encrypt(Psw));

            if (result.Item1.Count > 0)
            {
                HttpContext.Session.SetString("userid", result.Item1[0].userid.ToString());
                HttpContext.Session.SetString("username", result.Item1[0].username);

                if (result.Item1[0].PageIds == null)
                {
                    HttpContext.Session.SetString("PageIds", "");
                    return Json("2");
                }
                else
                {
                    HttpContext.Session.SetString("PageIds", result.Item1[0].PageIds);
                    return Json("1");
                }
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSupernowaGameList(int PlatformId, string PlatformName, string SelectedOption, DateTime FromDate, DateTime ToDate)
        {
            var HomeRepo = new HomeRepository(connStr);
            if (PlatformName == "PowerGame")
            {
                var result = HomeRepo.GetGameList(PlatformName, "", SelectedOption, FromDate, ToDate);
                if (result.Item1.Count > 0)
                {
                    List<GameModel> PowerGameList = result.Item1;

                    for (int i = 0; i < PowerGameList.Count; i++)
                    {
                        string[] array = PowerGameList[i].Name.Split('\\');
                        PowerGameList[i].Name = array[array.Length - 1];
                        PowerGameList[i].ImagePath = "/assets/GameImage/" + PowerGameList[i].Name + ".png";
                        PowerGameList[i].GameCode = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                    return Json(PowerGameList);
                }
                else
                {
                    return Json("0");
                }
            }
            else
            {

                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
                var result = HomeRepo.GetSupernowaGameList(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption);

                GameList = result.Item1;
                if (result.Item1 == null)
                {
                    return Json("0");
                }
                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> GameList = new List<SelectListItem>();
                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        GameList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].GameID.ToString(),
                            Text = result.Item1[i].Name,
                        });
                    }
                    return Json(GameList);
                }
                else
                {
                    return Json("0");
                }
            }
        }
        public JsonResult GetPlatformList()
        {

            var HomeRepo = new HomeRepository(connStr);

            var result = HomeRepo.GetPlatformList();

            PlatformList = result.Item1;

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> PlatformList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    PlatformList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].id.ToString(),
                        Text = result.Item1[i].PlatName,
                    });
                }
                return Json(PlatformList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetAgentList(int PlatformId, string PlatformName, string SelectedOption)
        {

            var HomeRepo = new HomeRepository(connStr);
            if (SelectedOption == "PowerGame")
            {

                var result = HomeRepo.GetAgentList("", "", SelectedOption);
                if (result.Item1 == null)
                {
                    return Json("0");
                }
                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> AgentList = new List<SelectListItem>();
                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        AgentList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].UserID.ToString(),
                            Text = result.Item1[i].AgentName,

                        });
                        // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                    }

                    return Json(AgentList);
                }

            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();

                var result = HomeRepo.GetAgentList(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption);
                if (result.Item1 == null)
                {
                    return Json("0");
                }
                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> AgentList = new List<SelectListItem>();
                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        AgentList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].UserID.ToString(),
                            Text = result.Item1[i].AgentName,

                        });
                        // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                    }

                    return Json(AgentList);

                }
                else
                {
                    return Json("0");
                }
            }

            return Json("0");
        }

        public JsonResult GetClientList(int PlatformId, string PlatformName, string AgentUserId, string SelectedOption)
        {
            if (AgentUserId == "ALL")
            {
                return Json("0");
            }
            var HomeRepo = new HomeRepository(connStr);
            if (PlatformName == "PowerGame")
            {

                var result = HomeRepo.GetClienttList(PlatformName, "", AgentUserId, SelectedOption);
                if (result.Item1 == null)
                {
                    return Json("0");
                }
                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> AgentList = new List<SelectListItem>();
                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        AgentList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].UserID.ToString(),
                            Text = result.Item1[i].ClientName,

                        });
                        // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                    }

                    return Json(AgentList);
                }

            }
            else
            {

                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();

                var result = HomeRepo.GetClienttList(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, AgentUserId, SelectedOption);

                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> ClientList = new List<SelectListItem>();
                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        ClientList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].UserID.ToString(),
                            Text = result.Item1[i].ClientName,

                        });
                        // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                    }
                    return Json(ClientList);
                }
                else
                {
                    return Json("0");
                }
            }
            return Json("0");
        }
        public JsonResult GetGameList(int PlatformId, string PlatformName, string SelectedOption, DateTime FromDate, DateTime ToDate)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (PlatformName == "Power Games")
            {
                var result = HomeRepo.GetGameList(PlatformName, "", SelectedOption, FromDate, ToDate);
                if (result.Item1.Count > 0)
                {
                    List<GameModel> PowerGameList = result.Item1;

                    for (int i = 0; i < PowerGameList.Count; i++)
                    {
                        string[] array = PowerGameList[i].Name.Split('\\');
                        PowerGameList[i].Name = array[array.Length - 1];
                        PowerGameList[i].ImagePath = "/assets/GameImage/" + PowerGameList[i].Name + ".png";
                        PowerGameList[i].GameCode = "";//DateTime.Now.ToString("dd-MM-yyyy");
                    }
                    return Json(PowerGameList);
                }
                else
                {
                    return Json("0");
                }
            }
            if (PlatformName == "ALL")
            {
                var result = HomeRepo.GetGameList(PlatformName, "", SelectedOption, FromDate, ToDate);
                if (result.Item1.Count > 0)
                {
                    JsonResult jResult = null;
                    List<GameModel> PowerGameList = result.Item1;
                    Dictionary<String, JsonResult> finalResult = new Dictionary<String, JsonResult>();

                    for (int i = 0; i < PowerGameList.Count; i++)
                    {
                        string[] array = PowerGameList[i].Name.Split('\\');
                        PowerGameList[i].Name = array[array.Length - 1];
                        PowerGameList[i].ImagePath = "/assets/GameImage/" + PowerGameList[i].Name + ".png";
                        PowerGameList[i].GameCode = "";// DateTime.Now.ToString("dd-MM-yyyy");
                    }
                    finalResult.Add("Power Games", Json(PowerGameList));


                    List<PlatformModel> SelectedPlatform = new List<PlatformModel>();

                    SelectedPlatform.AddRange(PlatformList.Where(val => (val.DBName != null)).ToList());
                    foreach (PlatformModel pfm in SelectedPlatform)
                    {
                        var resultOther = HomeRepo.GetGameList(pfm.LinkServerName, pfm.DBName, SelectedOption, FromDate, ToDate);
                        if (resultOther.Item1.Count > 0)
                        {
                            finalResult.Add(pfm.PlatName, Json(resultOther.Item1));
                        }
                    }
                    return Json(finalResult);
                }
                else
                {
                    return Json("0");
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
                var result = HomeRepo.GetGameList(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption, FromDate, ToDate);
                if (result.Item1.Count > 0)
                {
                    return Json(result.Item1);
                }
                else
                {
                    return Json("0");
                }
            }
        }
        public JsonResult GetGameListforPieChart(int PlatformId, string PlatformName, string SelectedOption)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (PlatformName == "Power Games")
            {
                var result = HomeRepo.GetGameListPieChart(PlatformName, "", SelectedOption);
                if (result.Item1.Count > 0)
                {
                    List<GameModel> PowerGameList = result.Item1;

                    for (int i = 0; i < PowerGameList.Count; i++)
                    {
                        string[] array = PowerGameList[i].Name.Split('\\');
                        PowerGameList[i].Name = array[array.Length - 1];
                        PowerGameList[i].ImagePath = "/assets/GameImage/" + PowerGameList[i].Name + ".png";
                        PowerGameList[i].GameCode = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                    return Json(PowerGameList);
                }
                else
                {
                    return Json("0");
                }
            }
            if (PlatformName == "ALL")
            {
                var result = HomeRepo.GetGameListPieChart(PlatformName, "", SelectedOption);
                if (result.Item1.Count > 0)
                {
                    JsonResult jResult = null;
                    List<GameModel> PowerGameList = result.Item1;
                    Dictionary<String, JsonResult> finalResult = new Dictionary<String, JsonResult>();

                    for (int i = 0; i < PowerGameList.Count; i++)
                    {
                        string[] array = PowerGameList[i].Name.Split('\\');
                        PowerGameList[i].Name = array[array.Length - 1];
                        PowerGameList[i].ImagePath = "/assets/GameImage/" + PowerGameList[i].Name + ".png";
                        PowerGameList[i].GameCode = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                    finalResult.Add("Power Games", Json(PowerGameList));


                    List<PlatformModel> SelectedPlatform = new List<PlatformModel>();

                    SelectedPlatform.AddRange(PlatformList.Where(val => (val.DBName != null)).ToList());
                    foreach (PlatformModel pfm in SelectedPlatform)
                    {
                        var resultOther = HomeRepo.GetGameListPieChart(pfm.LinkServerName, pfm.DBName, SelectedOption);
                        if (resultOther.Item1.Count > 0)
                        {
                            finalResult.Add(pfm.PlatName, Json(resultOther.Item1));
                        }
                    }
                    return Json(finalResult);
                }
                else

                {
                    return Json("0");
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
                var result = HomeRepo.GetGameListPieChart(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption);
                if (result.Item1.Count > 0)
                {
                    return Json(result.Item1);
                }
                else
                {
                    return Json("0");
                }
            }
        }
        public JsonResult GetAgentWisePLList(int PlatformId, string PlatformName, string SelectedOption, DateTime FromDate, DateTime ToDate)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (SelectedOption == "PowerGame")
            {
                // var ISTFromDate = TimeZoneInfo.ConvertTimeFromUtc(FromDate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                //  var ISTToDate = TimeZoneInfo.ConvertTimeFromUtc(ToDate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));


                var result = HomeRepo.GeAgentPLList("", "", SelectedOption, FromDate, ToDate);
                AgentPLModelMetadata profit = result.Item2;
                if (result.Item1.Count > 0)
                {
                    // return Json(result.Item1);





                    var data = new AgentPLViewModel()
                    {
                        agentPLModelList = result.Item1,
                        netpl = Math.Round((Double)profit.NetPL, 2),
                        tloss = Math.Round((Double)profit.TotalLoss, 2),
                        tprofit = Math.Round((Double)profit.TotalProfit, 2),


                        type = "pw"


                    };

                    return Json(data);
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();

                if (SelectedPlatform.Count > 0)
                {
                    if (SelectedPlatform[0].LinkServerName != null)
                    {
                        var result = HomeRepo.GeAgentPLList(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption, FromDate, ToDate);
                        AgentPLModelMetadata profit = result.Item2;


                        if (result.Item1.Count > 0)
                        {
                            //  return Json(result.Item1);

                            var data = new AgentPLViewModel()
                            {
                                agentPLModelList = result.Item1,
                                netpl = Math.Round((Double)profit.NetPL, 2),
                                tloss = Math.Round((Double)profit.TotalLoss, 2),
                                tprofit = Math.Round((Double)profit.TotalProfit, 2)


                            };

                            return Json(data);

                        }
                    }
                }
            }

            string[] ResultArrNull = new string[0];
            return Json(new
            {
                data = ResultArrNull
            });


        }


        public JsonResult GetAgentWiseBetList(int PlatformId, string PlatformName, string GameID, string AgentUserId, string ClientUserId, string SelectedOption, string Settled, DateTime FromDate, DateTime ToDate,string Offset)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (SelectedOption == "PowerGame")
            {
                var ISTFromDate = TimeZoneInfo.ConvertTimeFromUtc(FromDate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                var ISTToDate = TimeZoneInfo.ConvertTimeFromUtc(ToDate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                var result = HomeRepo.GeAgentBetList("", "", GameID, AgentUserId, ClientUserId, SelectedOption, Settled, FromDate, ToDate,Offset);

                foreach (var n in result.Item1)
                {
                    string[] array = n.GameName.Split('\\');
                    n.GameName = array[array.Length - 1];

                    n.AgentName = n.AgentName + "\\" + n.ClientName;
                    n.betDetails = "Stake :" + n.Stake + " CR :" + n.CR + " DR :" + n.DR ;
                    if (n.IsBetWon == false)
                    {
                        n.WinOrLoss = "Loss";

                    }
                    else
                    {
                        n.WinOrLoss = "Win";
                    }
                }
                if (result.Item1.Count > 0)
                {
                    // return Json(result.Item1);
                    var ResultArr = from x in result.Item1
                                    select new[]
                                    {
                                Convert.ToString(x.BetID),
                                  Convert.ToString(x.AgentName),


                                  Convert.ToString(x.GameName),



                                  Convert.ToString(x.betDetails),
                                   Convert.ToString(x.Winner),
                                   Convert.ToString(x.WinOrLoss),
                                   Convert.ToString(x.BetID),
                                     Convert.ToString("")
                         };

                    return Json(new
                    {
                        aaData = ResultArr
                    });
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();

                if (SelectedPlatform.Count > 0)
                {
                    if (SelectedPlatform[0].LinkServerName != null)
                    {
                        var result = HomeRepo.GeAgentBetList(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, GameID, AgentUserId, ClientUserId, SelectedOption, Settled, FromDate, ToDate,Offset);

                        foreach (var a in result.Item1)
                        {
                            a.AgentName = a.AgentName + " / " + a.ClientName;
                            if (a.IsBack == true)
                                a.Runner = "Runner :" + a.Runner + " Type : Back" + "  Rate :" + a.Rate + " Stake :" + a.Stake + " PL :" + a.BetPL;
                            else
                                a.Runner = "Runner :" + a.Runner + " Type : Lay" + "  Rate :" + a.Rate + " Stake :" + a.Stake + " PL :" + a.BetPL;
                            if (a.IsBetWon == true)
                            {
                                a.WinOrLoss = "Won";
                            }
                            else
                            {
                                a.WinOrLoss = "Loss";
                            }


                        }


                        if (result.Item1.Count > 0)
                        {
                            //  return Json(result.Item1);
                            var ResultArra = from x in result.Item1
                                             select new[]
                                             {

                                  Convert.ToString(x.BetID),
                                  Convert.ToString(x.AgentName),



                                 Convert.ToString(x.GameName +"("+string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.RoundCreatedDate)+")"),




                                  Convert.ToString(x.Runner),
                                   Convert.ToString(x.Winner),
                                   Convert.ToString(x.WinOrLoss),
                                   Convert.ToString(x.RoundSummaryID),
                                   Convert.ToString(x.IsSettled)
                                  
                         };

                            return Json(new
                            {
                                aaData = ResultArra
                            });

                        }
                    }
                }
            }

            string[] ResultArrNull = new string[0];
            return Json(new
            {
                aaData = ResultArrNull
            });


        }


        public JsonResult GetGameListForChart(int PlatformId, string PlatformName, string SelectedOption)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (SelectedOption == "PowerGame")
            {
                var result = HomeRepo.GetGameListForChart("", "", SelectedOption);
                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> GameList = new List<SelectListItem>();

                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        string[] array = result.Item1[i].Name.Split('\\');
                        GameList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].Name,
                            Text = array[array.Length - 1],
                        });
                    }
                    return Json(GameList);

                }
                else
                {
                    return Json("0");
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
                var result = HomeRepo.GetGameListForChart(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption);
                if (result.Item1.Count > 0)
                {
                    List<SelectListItem> GameList = new List<SelectListItem>();
                    for (int i = 0; i < result.Item1.Count; i++)
                    {
                        string[] array = result.Item1[i].Name.Split('\\');
                        GameList.Add(new SelectListItem
                        {
                            Value = result.Item1[i].GameID.ToString(),
                            Text = array[array.Length - 1],
                        });
                    }
                    return Json(GameList);
                }
                else
                {
                    return Json("0");
                }
            }
        }

        public JsonResult GetLineChartData(int PlatformId, string PlatformName, string SelectedOption, string GameID)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (SelectedOption == "PowerGame")
            {
                var result = HomeRepo.GetLineChartData("", "", SelectedOption, GameID);
                if (result.Item1.Count > 0)
                {

                    List<GameModel> PowerGameList = result.Item1;

                    for (int i = 0; i < PowerGameList.Count; i++)
                    {
                        PowerGameList[i].CreatedDate = Convert.ToDateTime(PowerGameList[i].CreatedDate).ToString("dd-MM-yyyy");
                    }
                    return Json(PowerGameList);


                }
                else
                {
                    return Json("0");
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
                var result = HomeRepo.GetLineChartData(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption, GameID);
                if (result.Item1.Count > 0)
                {

                    List<GameModel> GameList = result.Item1;

                    for (int i = 0; i < GameList.Count; i++)
                    {
                        GameList[i].CreatedDate = Convert.ToDateTime(GameList[i].CreatedDate).ToString("dd-MM-yyyy");
                    }
                    return Json(GameList);
                }
                else
                {
                    return Json("0");
                }
            }
        }
        public JsonResult GetGroupInvoiceList(int PlatformId, string PlatformName, string SelectedOption, string GameID, String RAgentUserIds, DateTime FromDate, DateTime ToDate)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (SelectedOption == "PowerGame")
            {
                var result = HomeRepo.GetGroupInvoiceList("", "", SelectedOption, GameID, RAgentUserIds, FromDate,ToDate);
                if (result.Item1.Count > 0)
                {

                    List<InvoicePLModel> invoice = result.Item1;
                    var data = new InvoiceViewModel
                    {
                        invoice = result.Item1,
                        invoicedate = string.Format("{0:dd MMM yyyy}", ToDate)

                    };

                    return Json(data);
                }
                else
                {
                    return Json("0");
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
                var result = HomeRepo.GetGroupInvoiceList(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption, GameID, RAgentUserIds,FromDate,ToDate);
                if (result.Item1.Count > 0)
                {

                    List<InvoicePLModel> invoice = result.Item1;
                    var data = new InvoiceViewModel
                    {
                        invoice = result.Item1,
                        invoicedate = string.Format("{0:dd MMM yyyy}", ToDate) 

                    };
                  
                    return Json(data);
                }
                else
                {
                    return Json("0");
                }
            }
        }

        public IActionResult GetInvoice(int id, string name)
        {
            return View();

        }
            public JsonResult GetGroupList(int PlatformId, string PlatformName, string SelectedOption, string GameID,string Search,int PageNo,int PageSize)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (SelectedOption == "PowerGame")
            {
                var result = HomeRepo.GetGroupList("", "", SelectedOption, GameID, Search, PageNo, PageSize);
                if (result.Item1.Count > 0)
                {
                    string sm = "";
                    List<GroupListModel> GroupListModel = result.Item1;
                    var reg = new Regex("\".*?\"");
                    foreach (var item1 in result.Item1)
                    {
                        var matches = reg.Matches(item1.AgentUserIds);
                        item1.AgentUserIds = "";
                        int i = 0;
                        foreach (var item in matches)
                        {
                            if (i == 0)
                                item1.AgentUserIds += item.ToString().Replace('"', ' ') ;
                            else
                                item1.AgentUserIds += ","+item.ToString().Replace('"', ' ');
                            i = i + 1;
                        
                          
                        }
                        
                    }
                   
                  
                    var data = new GroupListViewModel { groupLists = GroupListModel };

                    return Json(data);
                }
                else
                {
                    return Json("0");
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
                var result = HomeRepo.GetGroupList(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption, GameID,Search,PageNo,PageSize);
                if (result.Item1.Count > 0)
                {

                    List<GroupListModel> GroupListModel = result.Item1;
                    var data = new GroupListViewModel { groupLists= GroupListModel };
                   
                    return Json(data);
                }
                else
                {
                    return Json("0");
                }
            }
        }
        public JsonResult GetPieChartDataPlatformWise(string SelectedOption)
        {
            var HomeRepo = new HomeRepository(connStr);
            var result = HomeRepo.GetPieChartDataPlatformWise(SelectedOption);
            if (result.Item1.Count > 0)
            {
                return Json(result.Item1);
            }
            else
            {
                return Json("0");
            }
        }
         public JsonResult GetPieChartDataGamesWise(int PlatformId, string PlatformName, string SelectedOption)
        {

            var HomeRepo = new HomeRepository(connStr);

            if (SelectedOption == "PowerGame")
            {
                var result = HomeRepo.GetPieChartDataGamesWise("", "", SelectedOption);
                if (result.Item1.Count > 0)
                {
                    List<GameModel> PowerGameList = result.Item1;

                    for (int i = 0; i < PowerGameList.Count; i++)
                    {
                        string[] array = PowerGameList[i].Name.Split('\\');
                        PowerGameList[i].Name = array[array.Length - 1];
                        PowerGameList[i].ImagePath = "/assets/GameImage/" + PowerGameList[i].Name + ".png";
                        PowerGameList[i].GameCode = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                    return Json(PowerGameList);
                }
                else
                {
                    return Json("0");
                }
            }
            else
            {
                var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
                var result = HomeRepo.GetPieChartDataGamesWise(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption);
                if (result.Item1.Count > 0)
                {
                    return Json(result.Item1);
                }
                else
                {
                    return Json("0");
                }
            }
        }



        //User Operations

        public JsonResult InsertUserData(LoginModel obj)
        {
            var HomeRepo = new HomeRepository(connStr);

            obj.createdby = Convert.ToInt32(HttpContext.Session.GetString("userid"));

            obj.userpassword = EncryptionDecryption.Encrypt(obj.userpassword);


            var result = HomeRepo.CheckLogin(obj.username, obj.userpassword);

            if (result.Item1.Count > 0)
            {
                return Json("2");
            }
            else
            {
                var res = HomeRepo.AddUpdateUser(obj);
                if (res != "")
                {
                    return Json("1");
                }
                else
                {
                    return Json("0");
                }
            }
        }
        public JsonResult GetUserList()
        {

            var HomeRepo = new HomeRepository(connStr);

            var result = HomeRepo.GetUserList();

            if (result.Item1.Count > 0)
            {
                //  return Json(result.Item1);
                var ResultArra = from x in result.Item1
                                 select new[]
                                 {
                                Convert.ToString(x.userid),
                                Convert.ToString(x.username),
                                  Convert.ToString(EncryptionDecryption.Decrypt(x.userpassword)),
                                Convert.ToString(x.usertype),
                         };

                return Json(new
                {
                    aaData = ResultArra
                });

            }
            else
            {

                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    aaData = ResultArrNull
                });
            }

        }

        public JsonResult UpdateUserData(LoginModel obj)
        {
            var HomeRepo = new HomeRepository(connStr);

            obj.createdby = Convert.ToInt32(HttpContext.Session.GetString("userid"));

            obj.userpassword = EncryptionDecryption.Encrypt(obj.userpassword);

            var result = HomeRepo.CheckLogin(obj.username, obj.userpassword);

            if (result.Item1.Count > 0 && result.Item1[0].usertype == obj.usertype)
            {
                return Json("2");
            }
            else
            {
                var res = HomeRepo.AddUpdateUser(obj);
                if (res != "")
                {
                    return Json("1");
                }
                else
                {
                    return Json("0");
                }
            }
        }


        public JsonResult DeleteUser(int ID)
        {
            var HomeRepo = new HomeRepository(connStr);

            var res = HomeRepo.DeleteUser(ID);
            if (res != "")
            {
                return Json("1");
            }
            else
            {
                return Json("0");
            }

        }
        public JsonResult UserViews(int PlatformId, string PlatformName, string SelectedOption)
        {
            var HomeRepo = new HomeRepository(connStr);
            var SelectedPlatform = PlatformList.Where(val => (val.id == PlatformId) && (val.PlatName == PlatformName)).ToList();
            try
            {

                var res = HomeRepo.UserViews(SelectedPlatform[0].LinkServerName, SelectedPlatform[0].DBName, SelectedOption);
                if (res != 0)
                {
                    return Json(res);
                }
                else
                {
                    return Json(0);
                }
            }
            catch { return Json(0); }
        }
        //User Permission

        public JsonResult GetUserDataList()
        {
            var HomeRepo = new HomeRepository(connStr);

            var result = HomeRepo.GetUserList();

            var PageList = HomeRepo.GetPageList().Item1;

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> UserList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    UserList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].userid.ToString(),
                        Text = result.Item1[i].username,
                    });
                }
                return Json(new { UserList, PageList });
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetUserPermissionList(int userid)
        {
            var HomeRepo = new HomeRepository(connStr);

            var result = HomeRepo.GetUserPermissionList(userid);

            if (result.Item1.Count > 0)
            {
                return Json(result.Item1);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult AddUpdateUserPermission(UserPermissionModel obj)
        {
            var HomeRepo = new HomeRepository(connStr);

            var res = HomeRepo.AddUpdateUserPermission(obj);
            if (res != "")
            {
                return Json("1");
            }
            else
            {
                return Json("0");
            }
        }


        public JsonResult GetSatSportPlatformList()
        {

            var RunningRepo = new RunningAnalysisRepository(connStr);

            var result = RunningRepo.GetSatSportPlatformList();

            SatSportPlatformList = result.Item1;

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> PlatformList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    PlatformList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].SSID.ToString(),
                        Text = result.Item1[i].SSName,
                    });
                }
                return Json(PlatformList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetWithdrawalRequestList(int PlatformId, string PlatformName,String Status,DateTime FromDate,DateTime ToDate,string Offset)
        {
            ViewBag.total = 0;

            var HomeRepo = new HomeRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = HomeRepo.GetWithdrawalRequestList(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName,Status,FromDate,ToDate,Offset);
            ViewBag.total = result.Item2;
            if (result.Item1.Count > 0)
            {


                //  return Json(result.Item1);
                //var ResultArra = from x in result.Item1
                //                 select new []
                //                 {
                //                   // x.UserWithdrawalId,
                //                    x.Username,
                //                    x.Balance,
                //                    x.WithdrawalAmount,

                //                    x.BankName,
                //                    x.BankAccNo,
                //                    x.AccountHolderName,
                //                    x.BankIFSC,
                //                    x.MobileNo,
                //                    x.WithdrawalType,
                //                    x.Status,
                //                    x.Description,
                //                    x.DeclineReason,

                //                    string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.CreatedDate),
                //                   (x.ModifiedDate==DateTime.MinValue)?"":string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.ModifiedDate)
                //         };



                return Json(new
                {
                    aaData = result.Item1,
                    total=result.Item2
                });
            }
            else
            {

                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    aaData = ResultArrNull
                });
            }

        }
    }
}
