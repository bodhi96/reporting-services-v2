﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using NewReportingService.Helper;
using NewReportingService.Models;
using NewReportingService.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Controllers
{
    public class SatsportController : Controller
    {
        private readonly IConfiguration config;
        private readonly string connStr;

        private static List<PlatformModel> PlatformList = new List<PlatformModel>();
        private static List<SatSportPlatformModel> SatSportPlatformList = new List<SatSportPlatformModel>();
        private static List<SatSportBetModel> SatSportBetList = new List<SatSportBetModel>();

        Utility UtilityObj;
        public SatsportController(IConfiguration config)
        {
            this.config = config;
            connStr = config.GetConnectionString("DatabaseConnection");

            UtilityObj = new Utility(config);
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/SecondDashboard"))
            {
                ViewBag.header = "Exchange BetList";
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        public IActionResult BetList()
        {
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Home/SecondDashboard"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public IActionResult FraudDetection()
        {
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Satsport/FraudDetection"))
            {
                ViewBag.header = "Fraud Detection";
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        [HttpPost]
        public IActionResult Test()
        {
            var data = new ReportViewModel()
            {
                MList = new List<ReportingDataViewModel>()
        {
            new ReportingDataViewModel(){CN="AC_CN1", RUrl="AC_RUrl1",StartTime="2019-8-9", EndTime="2019-8-13",Status="Good"},
            new ReportingDataViewModel(){CN="AC_CN2", RUrl="AC_RUrl2",StartTime="2019-9-4", EndTime="2019-9-15",Status="Nice"},
            new ReportingDataViewModel(){CN="AC_CN3", RUrl="AC_RUrl3",StartTime="2019-8-3", EndTime="2019-8-5",Status="Ok"}
        },
                AList = new List<ReportingDataViewModel>()
        {
            new ReportingDataViewModel(){CN="ACN1", RUrl="ARUrl1",StartTime="2019-3-9", EndTime="2019-3-13",Status="Nice"},
            new ReportingDataViewModel(){CN="ACN2", RUrl="ARUrl2",StartTime="2019-4-4", EndTime="2019-4-15",Status="Nice"},
            new ReportingDataViewModel(){CN="ACN3", RUrl="ARUrl3",StartTime="2019-5-3", EndTime="2019-5-5",Status="Ok"}
        },
                profit = "2000"
            };
            return Json(data);
        }


        public IActionResult GetFraudDetails(int id, string name)
        {
            ViewBag.name = name;
            ViewBag.header = "Fraud Details";
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "Satsport/GetFraudDetails"))
            {
                var satrepo = new SatsportRepository(connStr);

                String[] back = null;
                String[] lay = null;
                string[] backval = null;
                string[] layval = null;

                var result = satrepo.GetFraudDetail(id);
                //   AgentPLModelMetadata profit = result.Item2;
                var data = new FraudViewModel();
                foreach (var n in result.Item1)
                {
                    back = n.BackRateJson.Split('[');

                    back = back.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                    lay = n.LayRateJson.Split('[');//.Select(a => a.Trim()).ToArray();
                    lay.ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    lay = lay.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                    backval = back[0].ToString().Split(',');

                    layval = lay[0].ToString().Split(',');
                    n.backrate1 = backval[1];
                    n.backvolume1 = backval[2].Replace("[", string.Empty).Replace("]", string.Empty);
                    n.layrate1 = layval[1];
                    n.layvolume1 = layval[2].Replace("[", string.Empty).Replace("]", string.Empty);
                    backval = back[1].ToString().Split(',');
                    layval = lay[1].ToString().Split(',');
                    n.layrate2 = layval[1];
                    n.layvolume2 = layval[2].Replace("[", string.Empty).Replace("]", string.Empty);
                    n.backrate2 = backval[1];
                    n.backvolume2 = backval[2].Replace("[", string.Empty).Replace("]", string.Empty);
                    backval = back[2].ToString().Split(',');
                    layval = lay[2].ToString().Split(',');
                    n.backrate3 = backval[1];
                    n.backvolume3 = backval[2].Replace("[", string.Empty).Replace("]", string.Empty);
                    n.layrate3 = layval[1];
                    n.layvolume3 = layval[2].Replace("[", string.Empty).Replace("]", string.Empty);


                }

                if (result.Item1.Count > 0)
                {
                    //  return Json(result.Item1);

                    data = new FraudViewModel()
                    {

                        details = result.Item1,

                    };




                }

                return View(data);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }



        }
        public JsonResult GetFraudList(int PlatformId, string PlatformName, string MarketID, string appStartDate, string appEndDate, string Offset, int appBFSportID, int appBFTournamentID, int appBFMatchID, int appBFCentralID)
        {
            var SatsportRepo = new SatsportRepository(connStr);

            var result = SatsportRepo.GetFraudList(PlatformId, MarketID, appStartDate, appEndDate, Offset, appBFSportID, appBFTournamentID, appBFMatchID, appBFCentralID);

            List<FraudDetectionClass> SatSportBetList = result.Item1;

            if (result.Item1.Count > 0)
            {

                var ResultArra = from x in result.Item1
                                 select new[]
                                 {
                                  Convert.ToString(x.appCheatBetID),
                                  Convert.ToString(x.appSportName),
                                  Convert.ToString(x.appSportName)+"|"+Convert.ToString(x.appTournamentName)+"|("+Convert.ToString(x.appMatchName)+")|"+ string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.appBetDate),
                                  Convert.ToString(x.appRunnerName),
                                  Convert.ToString(x.appRate),
                                  Convert.ToString(x.appIsBack),
                                  Convert.ToString(x.appIsValid),
                                  Convert.ToString(x.appIsValid),
                                

                             //   Convert.ToString(CalculatePL(x.appRunnerID,x.,x.IsFancy,x.MarketType)),
                             //    Convert.ToString(x.IsSettled==true?"YES":"NO"),
                                // Convert.ToString(x.BetTaken)
                                string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.appBetDate)


                         };

                return Json(new
                {
                    aaData = ResultArra
                });
            }
            else
            {
                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    aaData = ResultArrNull
                });
            }

        }
        public JsonResult GetSatSportBetList(int PlatformId, string PlatformName, string SelectedOption, string MarketID, string Settled, string FromDate, string ToDate, string Offset, string SportID, string TournamentID, string MatchID, string Fancy)
        {

            if (Fancy == "ALL")
            {
                Fancy = "NULL";
            }
            var SatsportRepo = new SatsportRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = SatsportRepo.GetSatSportBetList(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, MarketID, Settled, FromDate, ToDate, Offset, SportID, TournamentID, MatchID, Fancy, SelectedOption);

            SatSportBetList = result.Item1;

            if (result.Item1.Count > 0)
            {

                var ResultArra = from x in result.Item1
                                 select new[]
                                 {
                                  Convert.ToString(x.BetID),
                                  Convert.ToString(x.Users),
                                  Convert.ToString(x.SportName)+">"+Convert.ToString(x.TournamentName)+">("+Convert.ToString(x.MatchName)+">"+ string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.OpenDate),
                                  Convert.ToString(x.Selection),
                                //Convert.ToString(x.IsMatchBet),
                                //Convert.ToString(x.Run),
                                  Convert.ToString(x.Rate),
                                  Convert.ToString(x.Stake),

                                  Convert.ToString(CalculatePL(x.RunnerId,x.RunnerPL,x.IsFancy,x.MarketType)),
                                  Convert.ToString(x.IsSettled==true?"YES":"NO"),
                                // Convert.ToString(x.BetTaken)
                                string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.BetTaken)


                         };

                return Json(new
                {
                    aaData = ResultArra
                });
            }
            else
            {
                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    aaData = ResultArrNull
                });
            }

        }

        public decimal CalculatePL(int RunnerId, string RunnerPL, bool IsFancy, string MarketType)
        {
            if (IsFancy && MarketType != "Bookmakers")
            {
                try
                {
                    SatSportRunnerPLModel dataObj = JsonConvert.DeserializeObject<SatSportRunnerPLModel>(RunnerPL);
                    decimal PL = 0 - (dataObj.YesPL);
                    return PL;

                }
                catch
                {
                    SatSportRunnerPLModel[] dataObj = JsonConvert.DeserializeObject<SatSportRunnerPLModel[]>(RunnerPL);
                    decimal PL = 0;
                    for (int i = 0; i < dataObj.Length; i++)
                    {
                        if (dataObj[i].RunnerId == RunnerId)
                        {
                            PL = 0 - (dataObj[i].RunnerPL);
                        }
                    }
                    return PL;
                }
            }
            else
            {
                SatSportRunnerPLModel[] dataObj = JsonConvert.DeserializeObject<SatSportRunnerPLModel[]>(RunnerPL);
                decimal PL = 0;
                for (int i = 0; i < dataObj.Length; i++)
                {
                    if (dataObj[i].RunnerId == RunnerId)
                    {
                        PL = 0 - (dataObj[i].RunnerPL);
                    }
                }
                return PL;

            }
        }
        public JsonResult GetSatSportSportList(int PlatformId, string PlatformName)
        {

            var satsportRepo = new SatsportRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportSportList(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> SportList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    SportList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].SportID.ToString(),
                        Text = result.Item1[i].Name,

                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(SportList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportBFSportList(int PlatformId, string PlatformName)
        {

            var satsportRepo = new SatsportRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportSportList(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> SportList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    SportList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].BfSportID.ToString(),
                        Text = result.Item1[i].Name,

                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(SportList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportTournamentList(DateTime FromDate, DateTime ToDate, int PlatformId, string PlatformName, string sportID, string term)
        {

            var satsportRepo = new SatsportRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportTournamentList(FromDate, ToDate, SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, sportID, term);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].TournamentID.ToString(),
                        Text = result.Item1[i].Name,
                        Selected = result.Item1[i].IsActive
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportBfTournamentList(DateTime FromDate, DateTime ToDate, int PlatformId, string PlatformName, string sportID, string term)
        {

            var satsportRepo = new SatsportRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportBfTournamentList(FromDate, ToDate, SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, sportID, term);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].TournamentID.ToString(),
                        Text = result.Item1[i].Name,
                        Selected = result.Item1[i].IsActive
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportBfMatchList(DateTime FromDate, DateTime ToDate, int PlatformId, string PlatformName, string tournamentID)
        {

            var satsportRepo = new SatsportRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportBfMatchList(FromDate, ToDate, SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, tournamentID);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].MatchID.ToString(),
                        Text = result.Item1[i].Name,
                        Selected = result.Item1[i].IsActive,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetSatSportMatchList(DateTime FromDate, DateTime ToDate, int PlatformId, string PlatformName, string tournamentID)
        {

            var satsportRepo = new SatsportRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = satsportRepo.GetSatSportMatchList(FromDate, ToDate, SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, tournamentID);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].MatchID.ToString(),
                        Text = result.Item1[i].Name,
                        Selected = result.Item1[i].IsActive,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportMarketListByMatchID(int PlatformId, string PlatformName, string MatchID, string Fancy)
        {
            if (Fancy == "ALL")
            {
                Fancy = "NULL";
            }

            var RunningRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = RunningRepo.GetSatSportMarketListByMatchID(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, MatchID, Fancy);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> MarketList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    MarketList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].MarketID.ToString() + "(" + result.Item1[i].CentralID + ")",
                        Text = result.Item1[i].MatchName + " > " + result.Item1[i].MarketName + " > (" + result.Item1[i].CentralID + ")",
                        Selected = result.Item1[i].IsInPlay,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(MarketList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetSatSportBetListALL(int MarketID, string Offset, string SportID, string TournamentID, string MatchID, string Fancy)
        {

            var RunningRepo = new RunningAnalysisRepository(connStr);

            var SelectedPlatform = SatSportPlatformList.ToList();

            var result = RunningRepo.GetSatSportBetListALL(MarketID, Offset, SportID, TournamentID, MatchID, Fancy);

            var lstBet = result.Item1;

            if (result.Item1.Count > 0)
            {
                var ResultArra = from x in result.Item1
                                 select new[]
                                 {
                                Convert.ToString(x.BetID),
                                Convert.ToString(x.Users),
                                Convert.ToString(x.SSName+"/"+x.MatchName+"/"+x.MarketType),
                                Convert.ToString(x.Selection),
                                Convert.ToString(x.Rate),
                                Convert.ToString(x.Stake),

                                Convert.ToString(CalculatePL(x.RunnerId,x.RunnerPL,x.IsFancy,x.MarketType)),
                                  Convert.ToString(""),
                                string.Format("{0:dd MMM yyyy hh:mm:ss tt}", x.BetTaken),

                         };

                return Json(new
                {
                    aaData = ResultArra
                });
            }
            else
            {
                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    aaData = ResultArrNull
                });
            }

        }

        public JsonResult GetSatSportSportListALL()
        {

            var satsportRepo = new SatsportRepository(connStr);



            var result = satsportRepo.GetSatSportSportList();

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> SportList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    SportList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].BfSportID.ToString(),
                        Text = result.Item1[i].Name,

                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(SportList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportTournamentListALL(DateTime FromDate, DateTime ToDate, string sportID)
        {

            var satsportRepo = new SatsportRepository(connStr);



            var result = satsportRepo.GetSatSportTournamentList(FromDate, ToDate, sportID);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].BfTournamentID.ToString(),
                        Text = result.Item1[i].Name,
                        Selected = result.Item1[i].IsActive
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportMatchListALL(DateTime FromDate, DateTime ToDate, string tournamentID)
        {

            var satsportRepo = new SatsportRepository(connStr);



            var result = satsportRepo.GetSatSportMatchList(FromDate, ToDate, tournamentID);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> TounamentList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    TounamentList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].BfMatchID.ToString(),
                        Text = result.Item1[i].Name,
                        Selected = result.Item1[i].IsActive,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(TounamentList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetSatSportMarketListALL(string SportID, string TournamentID, string MatchID, string Fancy, DateTime FromDate, DateTime ToDate)
        {


            var RunningRepo = new SatsportRepository(connStr);

            //  var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = RunningRepo.GetSatSportMarketListALL(SportID, TournamentID, MatchID, Fancy, FromDate, ToDate);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> MarketList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    MarketList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].MarketID.ToString() + "(" + result.Item1[i].CentralID + ")",
                        Text = result.Item1[i].MatchName + " > " + result.Item1[i].MarketName + " > (" + result.Item1[i].CentralID + ")",
                        Selected = result.Item1[i].IsInPlay,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(MarketList);
            }
            else
            {
                return Json("0");
            }
        }

        public JsonResult GetSatSportMarketList(int PlatformId, string PlatformName, string SportID, string TournamentID, string MatchID, string Fancy, string FromDate, string ToDate)
        {

            var SatsportRepo = new SatsportRepository(connStr);
            if (Fancy == "undefined")
            {
                Fancy = "NULL";
            }

            if (Fancy == "ALL")
            {
                Fancy = "NULL";
            }
            var SelectedPlatform = SatSportPlatformList.Where(val => (val.SSID == PlatformId) && (val.SSName == PlatformName)).ToList();

            var result = SatsportRepo.GetSatSportMarketList(SelectedPlatform[0].SSLinkServerName, SelectedPlatform[0].SSDBName, SportID, TournamentID, MatchID, Fancy, FromDate, ToDate);

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> MarketList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    MarketList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].MarketID.ToString() + "(" + result.Item1[i].CentralID + ")",
                        Text = result.Item1[i].MatchName + " > " + result.Item1[i].MarketName + " > (" + result.Item1[i].CentralID + ")",
                        Selected = result.Item1[i].IsInPlay,
                    });
                    // Convert.ToString(x.MatchName)+">"+Convert.ToString(x.MarketName)+">("+Convert.ToString(x.CentralID)+")",
                }
                return Json(MarketList);
            }
            else
            {
                return Json("0");
            }
        }
        public JsonResult GetSatSportPlatformList()
        {

            var RunningRepo = new RunningAnalysisRepository(connStr);

            var result = RunningRepo.GetSatSportPlatformList();

            SatSportPlatformList = result.Item1;

            if (result.Item1.Count > 0)
            {
                List<SelectListItem> PlatformList = new List<SelectListItem>();
                for (int i = 0; i < result.Item1.Count; i++)
                {
                    PlatformList.Add(new SelectListItem
                    {
                        Value = result.Item1[i].SSID.ToString(),

                        Text = result.Item1[i].SSName,
                    });
                }
                return Json(PlatformList);
            }
            else
            {
                return Json("0");
            }
        }
    }
}
