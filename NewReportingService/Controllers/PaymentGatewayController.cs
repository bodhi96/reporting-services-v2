﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NewReportingService.Helper;
using NewReportingService.Models;
using NewReportingService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Controllers
{
    public class PaymentGatewayController : Controller
    {
        private readonly IConfiguration config;
        private readonly string connStr;
        Utility UtilityObj;
        public PaymentGatewayController(IConfiguration config)
        {

            this.config = config;
            connStr = config.GetConnectionString("DatabaseConnection");
            UtilityObj = new Utility(config);
        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("userid") != null && HttpContext.Session.GetString("userid") != "" && UtilityObj.CheckUserPermission(HttpContext.Session.GetString("userid"), "PaymentGateway/index"))
            {
                ViewBag.header = "PaymentGateway";
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        [HttpPost]
        public JsonResult getDailyReport( DateTime StartDate, DateTime EndDate)
        {

            
            var HomeRepo = new PaymentGateWayRepository(connStr);

         
            var result = HomeRepo.GetDailyReport(StartDate, EndDate);

            if (result.Item1.Count > 0)
            {





                var data = new PaymentGViewModel()
                {
                    pgmodel = result.Item1


                };

                return Json(data);
            }
            else
            {
                string[] ResultArrNull = new string[0];
                return Json(new
                {
                    data = ResultArrNull
                });
            }
            }



        


    }
}


