﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Helper
{
    public class DapperHelper
    {
		private readonly string connStr;

		public class Result
		{
			public Result(int affectedRows, string success, string responseCode)
			{
				AffectedRows = affectedRows;
				Success = success ?? string.Empty;
				ResponseCode = responseCode ?? string.Empty;
			}

			public int AffectedRows { get; private set; }
			public string Success { get; private set; }
			public string ResponseCode { get; private set; }
		}

		public DapperHelper(string connStr)
		{
			this.connStr = connStr;
			Dapper.SqlMapper.Settings.CommandTimeout = 0;
		}

		public Result Execute(string spName, object parameters)
		{
			if (string.IsNullOrEmpty(spName))
			{
				throw new ArgumentException("message", nameof(spName));
			}

			DynamicParameters dynamicParameters = new DynamicParameters(parameters);
			dynamicParameters.Add("@SUCCESS", dbType: DbType.String, direction: ParameterDirection.Output, size: 5215585);
			dynamicParameters.Add("@RESPONSECODE", dbType: DbType.String, direction: ParameterDirection.Output, size: 5215585);
			using (var conn = new SqlConnection(connStr))
			{
				int recordCount = conn.Execute(spName, dynamicParameters, commandType: CommandType.StoredProcedure);
				return new Result(
					recordCount
					, dynamicParameters.Get<string>("@SUCCESS")
					, dynamicParameters.Get<string>("@RESPONSECODE")
				);
			}
		}

		public Result QueryMultiple(string spName, object parameters, Action<SqlMapper.GridReader> readFromResult)
		{
			if (string.IsNullOrEmpty(spName))
			{
				throw new ArgumentException("message", nameof(spName));
			}


			DynamicParameters dynamicParameters = new DynamicParameters(parameters); ;
			dynamicParameters.Add("@SUCCESS", dbType: DbType.String, direction: ParameterDirection.Output, size: 5215585);
			dynamicParameters.Add("@RESPONSECODE", dbType: DbType.String, direction: ParameterDirection.Output, size: 5215585);
			using (var conn = new SqlConnection(connStr))
			{
				var reader = conn.QueryMultiple(spName, dynamicParameters, commandType: CommandType.StoredProcedure);
				var responseCode = dynamicParameters.Get<string>("@RESPONSECODE");
				if (responseCode != "-1")
				{
					readFromResult?.Invoke(reader);
				}
				return new Result(
					-1
					, dynamicParameters.Get<string>("@SUCCESS")
					, responseCode
				);
			}
		}



		public Result QueryWithoutStatus(string spName, object parameters, Action<SqlMapper.GridReader> readFromResult)
		{
			if (string.IsNullOrEmpty(spName))
			{
				throw new ArgumentException("message", nameof(spName));
			}


			using (var conn = new SqlConnection(connStr)) 
			{
				var reader = conn.QueryMultiple(spName, parameters, commandType: CommandType.StoredProcedure);


				readFromResult?.Invoke(reader);

				return new Result(1,"","");
			}
		}

		public Result ExecuteWithoutStatus(string spName, object parameters)
		{
			if (string.IsNullOrEmpty(spName))
			{
				throw new ArgumentException("message", nameof(spName));
			}
			using (var conn = new SqlConnection(connStr))
			{
				int recordCount = conn.Execute(spName, parameters, commandType: CommandType.StoredProcedure);
				return new Result(
					recordCount
					, "success"
					, "success"
				);
			}
		}

	}
}
