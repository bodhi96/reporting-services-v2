﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NewReportingService.Helper
{
    public class EncryptionDecryption
    {
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "B145D2A9EDB6FAB7AF7757B1913A5";
            byte[] clearBytes = Encoding.UTF8.GetBytes(clearText);
            byte[] clearBytes1;
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                    clearBytes1 = Encoding.UTF8.GetBytes(clearText);
                }
            }
            return Convert.ToBase64String(clearBytes1);
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "B145D2A9EDB6FAB7AF7757B1913A5";

            cipherText = cipherText.Replace(" ", "+");
            var base64EncodedBytes = System.Convert.FromBase64String(cipherText);
            string clearstring = Encoding.UTF8.GetString(base64EncodedBytes);
            cipherText = clearstring;

            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.UTF8.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
}
