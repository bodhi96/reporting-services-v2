﻿using Microsoft.Extensions.Configuration;
using NewReportingService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Helper
{
    public class Utility
    {
        private readonly IConfiguration config;
        private readonly string connStr;

        //ErrorHelper errorhelper;
        public Utility(IConfiguration config)
        {
            this.config = config;
            connStr = config.GetConnectionString("DatabaseConnection");
        }

        public bool CheckUserPermission(string UserId,string Viewname)
        {
            var HomeRepo = new HomeRepository(connStr);

            var result = HomeRepo.GetUserPermissionList(Convert.ToInt32(UserId)).Item1;

            var PageList = HomeRepo.GetPageList().Item1;

            if (result.Count > 0)
            {
                //return Json(result.Item1);
                string[] PageIDS=result[0].PageIds.Split(',');
                for(int i=0;i< PageIDS.Length; i++)
                {
                    string id= PageIDS[i];

                    for (int j=0;j<PageList.Count;j++)
                    {
                        if(id==PageList[j].Pid.ToString() && Viewname== PageList[j].PViewname)
                        {
                            return true;
                        }
                    }
                }
            }   
            return false;     
        }


        public List<string> UserPermissionPageList(string UserId)
        {
            var HomeRepo = new HomeRepository(connStr);

            var result = HomeRepo.GetUserPermissionList(Convert.ToInt32(UserId)).Item1;

            var PageList = HomeRepo.GetPageList().Item1;

            List<string> ViewpageList = new List<string>();

            if (result.Count > 0)
            {
                //return Json(result.Item1);
                string[] PageIDS = result[0].PageIds.Split(',');
                for (int i = 0; i < PageIDS.Length; i++)
                {
                    string id = PageIDS[i];

                    for (int j = 0; j < PageList.Count; j++)
                    {
                        if (id == PageList[j].Pid.ToString())
                        {
                            ViewpageList.Add(PageList[j].PViewname);
                        }
                    }
                }
            }

            return ViewpageList;
        }

    }
}
