﻿using NewReportingService.Helper;
using NewReportingService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Repository
{
    public class RunningAnalysisRepository
    {
        private readonly string connStr;
        public RunningAnalysisRepository(string connStr)
        {
            this.connStr = connStr;
        }
        public Tuple<DailyReportMeta, List<DailyReportModel>, string> GetDailyReport(string LinkServerName, string DBName, int UserId, DateTime StartDate, DateTime EndDate, string SelectedOption)
        {
            List<DailyReportModel> DailyReportList = null;
            DailyReportMeta Daily = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            //  LinkServerName = "LNK_SATSPORT247";
            //  DBName = "Satsport247_Live";
            var result = db.QueryWithoutStatus("USP_GetExchangeDailyReportLog", new { LinkServerName, DBName, UserId, StartDate, EndDate, SelectedOption }, reader =>
            {
                try
                {
                    Daily = reader.Read<DailyReportMeta>().FirstOrDefault();
                    DailyReportList = reader.Read<DailyReportModel>().ToList();
                }
                catch { }


                responsemessage = "success";
            });




            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                  Daily,
                DailyReportList,


                responsemessage
            );


        }
        public Tuple<List<UserModel>, string> GetUserBySearch(string LinkServerName, string DBName, string search)
        {
            List<UserModel> UserList = null;
            DailyReportMeta Daily = null;
            string responsemessage = string.Empty;
            search = search + '%';
            var db = new DapperHelper(connStr);
            LinkServerName = "LNK_SATSPORT247";
            DBName = "Satsport247_Live";
            var result = db.QueryWithoutStatus("USP_GetSatSportUserList", new { LinkServerName, DBName, search }, reader =>
            {

                UserList = reader.Read<UserModel>().ToList();


                responsemessage = "success";
            });




            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(

                UserList,


                responsemessage
            );


        }


        public Tuple<List<SatSportPlatformModel>, string> GetSatSportPlatformList()
        {
            List<SatSportPlatformModel> SatSportPlatformList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("USP_GetSatSportPlatformList", null, reader =>
            {
                SatSportPlatformList = reader.Read<SatSportPlatformModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportPlatformList,
                responsemessage
            );
        }

        public Tuple<List<SignalRPlatformModel>, string> GetSignalRPlatformList()
        {
            List<SignalRPlatformModel> SignalRPlatformList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("USP_GetSignalRPlatformList", null, reader =>
            {
                SignalRPlatformList = reader.Read<SignalRPlatformModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(SignalRPlatformList, responsemessage);
        }

        public Tuple<List<SportsModel>, string> GetSatSportSportList()
        {
            List<SportsModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            var result = db.QueryWithoutStatus("USP_GetSatSportSportsListALL", null, reader =>
            {
                SatSportMarketList = reader.Read<SportsModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportMarketList,
                responsemessage
            );

        }
        public Tuple<List<SportsModel>, string> GetSatSportSportList(string LinkServerName, string DBName)
        {
            List<SportsModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportSportsList", null, reader =>
                {
                    SatSportMarketList = reader.Read<SportsModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportSportsList", new { LinkServerName, DBName }, reader =>
                {
                    SatSportMarketList = reader.Read<SportsModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }
        public Tuple<List<TournamentModel>, string> GetSatSportTournamentList(DateTime FromDate, DateTime ToDate, string sportsId, string term, string Offset)
        {
            List<TournamentModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);


            var result = db.QueryWithoutStatus("USP_GetSatSportTounamentListALL", new { sportsId, term, FromDate, ToDate, Offset }, reader =>
            {
                SatSportMarketList = reader.Read<TournamentModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportMarketList,
                responsemessage
            );

        }

        public Tuple<List<TournamentModel>, string> GetSatSportTournamentList(DateTime FromDate, DateTime ToDate, string LinkServerName, string DBName, string sportsId, string term, string Offset)
        {
            List<TournamentModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportTounamentList", null, reader =>
                {
                    SatSportMarketList = reader.Read<TournamentModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportTounamentList", new { LinkServerName, DBName, sportsId, term, FromDate, ToDate, Offset }, reader =>
                {
                    SatSportMarketList = reader.Read<TournamentModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }

        public Tuple<List<MatchModel>, string> GetSatSportMatchList(DateTime FromDate, DateTime ToDate, string tournamentId, string term)
        {
            List<MatchModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);


            var result = db.QueryWithoutStatus("USP_GetSatSportMatchListALL", new { tournamentId, term, FromDate, ToDate }, reader =>
            {
                SatSportMarketList = reader.Read<MatchModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportMarketList,
                responsemessage
            );

        }

        public Tuple<List<MatchModel>, string> GetSatSportMatchList(DateTime FromDate, DateTime ToDate, string LinkServerName, string DBName, string tournamentId, string term)
        {
            List<MatchModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportMatchList", null, reader =>
                {
                    SatSportMarketList = reader.Read<MatchModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportMatchList", new { LinkServerName, DBName, tournamentId, term, FromDate, ToDate }, reader =>
                {
                    SatSportMarketList = reader.Read<MatchModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }
        public Tuple<List<SatSportMarketModel>, string> GetSatSportMarketList(DateTime FromDate, DateTime ToDate, string LinkServerName, string DBName, string SportID, string TournamentID, string MatchID, string IsFancy, string term)
        {
            List<SatSportMarketModel> SatSportMarketList = null;
            string responsemessage = string.Empty;
            if (TournamentID == "undefined")
            {
                TournamentID = "0";
            }
            if (term == null)
            {
                term = "";
            }
            var db = new DapperHelper(connStr);
            if (IsFancy == "ALL")
            {
                IsFancy = "NULL";
            }

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportMarketList_V1", null, reader =>
                {
                    SatSportMarketList = reader.Read<SatSportMarketModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportMarketList_V1", new { LinkServerName, DBName, SportID, TournamentID, MatchID, IsFancy, term, FromDate, ToDate }, reader =>
                {
                    SatSportMarketList = reader.Read<SatSportMarketModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }


        public Tuple<List<SatSportMarketModel>, string> GetSatSportMarketListALL(DateTime FromDate, DateTime ToDate, string SportID, string TournamentID, string MatchID, string IsFancy, string term)
        {
            List<SatSportMarketModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            if (TournamentID == "undefined")
            {
                TournamentID = "0";
            }

            if (IsFancy == "ALL")
            {
                IsFancy = "NULL";
            }
            if (term == null)
            {
                term = "";
            }

            var result = db.QueryWithoutStatus("USP_GetSatSportMarketList_ALL", new { SportID, TournamentID, MatchID, IsFancy, term, FromDate, ToDate }, reader =>
            {
                SatSportMarketList = reader.Read<SatSportMarketModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportMarketList,
                responsemessage
            );

        }

        public Tuple<List<SatSportMarketModel>, string> GetSatSportMarketListByMatchID(string LinkServerName, string DBName, string MatchID, string Fancy)
        {

            List<SatSportMarketModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportMarketListByMatchId", null, reader =>
                {
                    SatSportMarketList = reader.Read<SatSportMarketModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportMarketListByMatchId", new { LinkServerName, DBName, MatchID, Fancy }, reader =>
                {
                    SatSportMarketList = reader.Read<SatSportMarketModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }
        public Tuple<List<SatSportBetModel>, string> GetSatSportBetList(string LinkServerName, string DBName, string MarketID, string Offset, string SportID, string TournamentID, string MatchID, string IsFancy, string FromDate, string ToDate, string PlatformName, string SelectedOption)
        {
            List<SatSportBetModel> SatSportBetList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            if (IsFancy == "ALL")
            {
                IsFancy = "NULL";
            }
            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportBetList", null, reader =>
                {
                    SatSportBetList = reader.Read<SatSportBetModel>().ToList();
                    responsemessage = PlatformName;
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportBetList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportBetList", new { LinkServerName, DBName, MarketID, Offset, SportID, TournamentID, MatchID, IsFancy, SelectedOption }, reader =>
                {
                    SatSportBetList = reader.Read<SatSportBetModel>().ToList();
                    responsemessage = PlatformName;
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportBetList,
                    responsemessage
                );
            }
        }
        public Tuple<List<SatSportBetListModel>, string> GetSatSportBetListALL(int MarketID, string Offset, string SportID, string TournamentID, string MatchID, string IsFancy)
        {
            List<SatSportBetListModel> SatSportBetList = null;
            string responsemessage = string.Empty;
            if (IsFancy == "ALL")
            {
                IsFancy = "NULL";
            }
            var db = new DapperHelper(connStr);


            var result = db.QueryWithoutStatus("USP_GetSatSportBetList_ALL_V1", new { MarketID, Offset, SportID, TournamentID, MatchID, IsFancy, }, reader =>
            {
                SatSportBetList = reader.Read<SatSportBetListModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportBetList,
                responsemessage
            );

        }
        public Tuple<List<SatSportBetListModel>, string> GetSatSportBetDetail(string LinkServerName, string DBName, string BetId, string MarketID, string Offset, string SportID, string TournamentID, string MatchID, string IsFancy)
        {
            List<SatSportBetListModel> SatSportBetList = null;
            string responsemessage = string.Empty;

            if (IsFancy == "ALL")
            {
                IsFancy = "NULL";
            }

            var db = new DapperHelper(connStr);

            var result = db.QueryWithoutStatus("USP_GetSatSportBetDetail_V1", new { LinkServerName, DBName, BetId, MarketID, Offset, SportID, TournamentID, MatchID, IsFancy }, reader =>
            {
                SatSportBetList = reader.Read<SatSportBetListModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportBetList,
                responsemessage
            );
        }
        public Tuple<List<SatSportRunnerModel>, string> GetSatSportRunnerList(string LinkServerName, string DBName, string MarketID)
        {
            List<SatSportRunnerModel> SatSportBetList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportRunnerList", null, reader =>
                {
                    SatSportBetList = reader.Read<SatSportRunnerModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportBetList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportRunnerList", new { LinkServerName, DBName, MarketID }, reader =>
                {
                    SatSportBetList = reader.Read<SatSportRunnerModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportBetList,
                    responsemessage
                );
            }
        }

        public Tuple<List<SatSportRunnerModel>, string> GetSatSportRunnerListALL(string LinkServerName, string DBName, string MarketID)
        {
            List<SatSportRunnerModel> SatSportBetList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);


            var result = db.QueryWithoutStatus("USP_GetSatSportRunnerListALL", new { MarketID }, reader =>
            {
                SatSportBetList = reader.Read<SatSportRunnerModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportBetList,
                responsemessage
            );

        }

    }
}
