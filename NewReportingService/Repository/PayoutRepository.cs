﻿using NewReportingService.Helper;
using NewReportingService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Repository
{
    public class PayoutRepository
    {
        private readonly string connStr;
     public   PayoutRepository(string connStr)
        {
            this.connStr = connStr;
        }

        public Tuple<List<PayoutReportModel>, string> GetPayOutReport(DateTime FromDate, DateTime ToDate,string PartnerID, string ClientID, string SportsID, string MatchID)
        {
            List<PayoutReportModel> DailyReportList = null;
           
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
           
            var result = db.QueryWithoutStatus("PayOutReportFulltoz", new { FromDate, ToDate, PartnerID,ClientID,SportsID,MatchID}, reader =>
            {
                // Daily = reader.Read<DailyReportMeta>().FirstOrDefault();
                DailyReportList = reader.Read<PayoutReportModel>().ToList();


                responsemessage = "success";
            });




            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(

                DailyReportList,


                responsemessage
            );


        }
        public Tuple<List<PartnerModel>, string> GetAgentList()
        {
            List<PartnerModel> AgentList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("PartnerListFulltoz", new { }, reader =>
            {
                try
                {
                    AgentList = reader.Read<PartnerModel>().ToList();
                }
                catch { }
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                AgentList,
                responsemessage
            );
        }

        public Tuple<List<ClientFultozModel>, string> GetClienttList( string PartnerID)
        {
            List<ClientFultozModel> ClientList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("ClientListFulltoz", new {  PartnerID }, reader =>
            {
                ClientList = reader.Read<ClientFultozModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                ClientList,
                responsemessage
            );
        }

        public Tuple<List<MatchlistFulltozModel>, string> GetMatchList(string CustomCondition)
        {
            List<MatchlistFulltozModel> MatchList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            var result = db.QueryWithoutStatus("MatchListFulltoz", new { CustomCondition }, reader =>
            {
                MatchList = reader.Read<MatchlistFulltozModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                MatchList,
                responsemessage
            );
        }


        public Tuple<List<SportListFulltozModel>, string> GetSportList()
        {
            List<SportListFulltozModel> SportList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("SportListFulltoz", new {  }, reader =>
            {
                SportList = reader.Read<SportListFulltozModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SportList,
                responsemessage
            );
        }
    }
}
