﻿using NewReportingService.Helper;
using NewReportingService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Repository
{
    public class HomeRepository
    {
        private readonly string connStr;
        public HomeRepository(string connStr)
        {
            this.connStr = connStr;
        }

        /* public string CheckLogin(string username, string userpass)
         {
             var db = new DapperHelper(connStr);
             var result = db.Execute("sp_login", new { username, userpass });
             return result.Success;
         }*/

        public Tuple<List<LoginModel>, string> CheckLogin(string username, string userpassword)
        {
            List<LoginModel> LoginList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("sp_login", new { username, userpassword }, reader =>
            {
                LoginList = reader.Read<LoginModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                LoginList,
                responsemessage
            );
        }


        public Tuple<List<PlatformModel>, string> GetPlatformList()
        {
            List<PlatformModel> PlatformList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("sp_GetPlatformList", null, reader =>
            {
                PlatformList = reader.Read<PlatformModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                PlatformList,
                responsemessage
            );
        }

        public Tuple<List<AgentPLModel>, string> GetAgentList(string LinkServerName, string DBName, string SelectedOption)
        {
            List<AgentPLModel> AgentList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("USP_GetSupernowaAgentListV1", new { LinkServerName, DBName, SelectedOption }, reader =>
              {
                  try
                  {
                      AgentList = reader.Read<AgentPLModel>().ToList();
                  }
                  catch { }
                  responsemessage = "success";
              });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                AgentList,
                responsemessage
            );
        }

        public Tuple<List<ClientModel>, string> GetClienttList(string LinkServerName, string DBName, string AgentUserId, string SelectedOption)
        {
            List<ClientModel> ClientList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("USP_GetSupernowaClientListV1", new { LinkServerName, DBName, AgentUserId, SelectedOption }, reader =>
             {
                 ClientList = reader.Read<ClientModel>().ToList();
                 responsemessage = "success";
             });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                ClientList,
                responsemessage
            );
        }



        public Tuple<List<GameDdlModel>, string> GetSupernowaGameList(string LinkServerName, string DBName, string SelectedOption)
        {
            List<GameDdlModel> GameList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if ((LinkServerName == null || DBName == null) && SelectedOption != null)
            {
                var result = db.QueryWithoutStatus("USP_GetSupernowaGameListV1", null, reader =>
                {
                    GameList = reader.Read<GameDdlModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSupernowaGameList", new { LinkServerName, DBName, SelectedOption }, reader =>
                {
                    try
                    {
                        GameList = reader.Read<GameDdlModel>().ToList();
                    }
                    catch { }
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
        }
        public Tuple<List<GameModel>, string> GetGameList(string LinkServerName, string DBName, string SelectedOption,DateTime FromDate,DateTime ToDate)
        {
            List<GameModel> GameList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (DBName == null && SelectedOption != null)
            {
                var result = db.QueryWithoutStatus("USP_GetGamesList_V1", null, reader =>
                 {
                     GameList = reader.Read<GameModel>().ToList();
                     responsemessage = "success";
                 });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetGamesList_V1", new { LinkServerName, DBName, SelectedOption,FromDate,ToDate }, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
        }


        public Tuple<List<GameModel>, string> GetGameListPieChart(string LinkServerName, string DBName, string SelectedOption)
        {
            List<GameModel> GameList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (DBName == null && SelectedOption != null)
            {
                var result = db.QueryWithoutStatus("USP_GetGamesList", null, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetGamesList", new { LinkServerName, DBName, SelectedOption }, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
        }
        public Tuple<List<SupernowaBetList>, string> GeAgentBetList(string LinkServerName, string DBName, string GameID, string AgentUserId, string ClientUserId, string SelectedOption, string Settled, DateTime FROMDATE, DateTime TODATE,string Offset)
        {
            List<SupernowaBetList> AgentPLList = null;
            string responsemessage = string.Empty;
            if (ClientUserId == null)
            {
                ClientUserId = "ALL";
            }
            var db = new DapperHelper(connStr);
            if (GameID == null)
            {
                GameID = "";
            }
            var result = db.QueryWithoutStatus("Sp_Get_ClientBetsV1", new { LinkServerName, DBName, FROMDATE, TODATE, AgentUserId, ClientUserId, GameID, SelectedOption, Settled,Offset }, reader =>
               {
                   AgentPLList = reader.Read<SupernowaBetList>().ToList();


                   responsemessage = "success";
               }); ;

            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                AgentPLList,
                responsemessage
            );
        }


        public Tuple<List<AgentPLAModel>, AgentPLModelMetadata, string> GeAgentPLList(string LinkServerName, string DBName, string SelectedOption, DateTime FROMDATE, DateTime TODATE)
        {
            List<AgentPLAModel> AgentPLList = null;
            AgentPLModelMetadata AgentMetaPLList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            if (SelectedOption == "PowerGame")
            {
                var result = db.QueryWithoutStatus("USP_GetAgentPLList_V2", new { LinkServerName, DBName, SelectedOption, FROMDATE, TODATE }, reader =>
                {
                    AgentPLList = reader.Read<AgentPLAModel>().ToList();
                    AgentMetaPLList = reader.Read<AgentPLModelMetadata>().FirstOrDefault();

                    responsemessage = "success";
                });




                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    AgentPLList,
                    AgentMetaPLList,
                    responsemessage
                );

            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetAgentPLList_V2", new { LinkServerName, DBName, SelectedOption, FROMDATE, TODATE }, reader =>
                {
                    AgentPLList = reader.Read<AgentPLAModel>().ToList();
                    AgentMetaPLList = reader.Read<AgentPLModelMetadata>().FirstOrDefault();

                    responsemessage = "success";
                });




                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    AgentPLList,
                    AgentMetaPLList,
                    responsemessage
                );
            }
        }



        public Tuple<List<GameModel>, string> GetGameListForChart(string LinkServerName, string DBName, string SelectedOption)
        {
            List<GameModel> GameList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if ((LinkServerName == null || DBName == null) && SelectedOption != null)
            {
                var result = db.QueryWithoutStatus("USP_GetGamesListForChart", null, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetGamesListForChart", new { LinkServerName, DBName, SelectedOption }, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
        }
        //[USP_GetLineChartData]


        public Tuple<List<GameModel>, string> GetLineChartData(string LinkServerName, string DBName, string SelectedOption, string GameID)
        {
            List<GameModel> GameList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if ((LinkServerName == null || DBName == null) && SelectedOption != null)
            {
                var result = db.QueryWithoutStatus("USP_GetLineChartData", null, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetLineChartData", new { LinkServerName, DBName, SelectedOption, GameID }, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
        }
        public Tuple<List<GroupListModel>, string> GetGroupList(string LinkServerName, string DBName, string SelectedOption, string GameID, string Search, int PageNo, int PageSize)
        {
            List<GroupListModel> GroupList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if ((LinkServerName == null || DBName == null) && SelectedOption != null)
            {
                var result = db.QueryWithoutStatus("USP_GetGroupList", null, reader =>
                {
                    GroupList = reader.Read<GroupListModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GroupList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetGroupList", new { LinkServerName, DBName, SelectedOption,Search,PageNo, PageSize }, reader =>
                {
                    GroupList = reader.Read<GroupListModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GroupList,
                    responsemessage
                );
            }
        }

        public Tuple<List<InvoicePLModel>, string> GetGroupInvoiceList(string LinkServerName, string DBName, string SelectedOption, string GameID,String RAgentUserIds,DateTime FromDate,DateTime ToDate )
        {
            List<InvoicePLModel> GroupList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if ((LinkServerName == null || DBName == null) && SelectedOption != null)
            {
                var result = db.QueryWithoutStatus("USP_GetAllAgentInvoicePLReport", new {LinkServerName,DBName,SelectedOption,GameID,RAgentUserIds,FromDate,ToDate}, reader =>
                {
                    GroupList = reader.Read<InvoicePLModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GroupList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetAllAgentInvoicePLReport", new { LinkServerName, DBName,SelectedOption, GameID, RAgentUserIds,FromDate,ToDate }, reader =>
                {
                    GroupList = reader.Read<InvoicePLModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GroupList,
                    responsemessage
                );
            }
        }

        public Tuple<List<GameModel>, string> GetPieChartDataPlatformWise(string SelectedOption)
        {
            List<GameModel> GameList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            var result = db.QueryWithoutStatus("USP_GetPieChartDataPlatformWise", new { SelectedOption }, reader =>
            {
                GameList = reader.Read<GameModel>().ToList();
                responsemessage = "success";
            });

            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                GameList,
                responsemessage
            );

        }


        public Tuple<List<GameModel>, string> GetPieChartDataGamesWise(string LinkServerName, string DBName, string SelectedOption)
        {
            List<GameModel> GameList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if ((LinkServerName == null || DBName == null) && SelectedOption != null)
            {
                var result = db.QueryWithoutStatus("USP_GetPieChartGamesWise", null, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetPieChartGamesWise", new { LinkServerName, DBName, SelectedOption }, reader =>
                {
                    GameList = reader.Read<GameModel>().ToList();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    GameList,
                    responsemessage
                );
            }
        }

        public string AddUpdateUser(LoginModel obj)
        {
            var db = new DapperHelper(connStr);
            var result = db.ExecuteWithoutStatus("USP_INUPUser", new { obj.userid, obj.username, obj.userpassword, obj.usertype, obj.createdby });
            return result.Success;
        }

        public Tuple<List<LoginModel>, string> GetUserList()
        {
            List<LoginModel> UserList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            var result = db.QueryWithoutStatus("USP_GetUsersList", null, reader =>
            {
                UserList = reader.Read<LoginModel>().ToList();
                responsemessage = "success";
            });

            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                UserList,
                responsemessage
            );

        }

        public string DeleteUser(int ID)
        {
            var db = new DapperHelper(connStr);
            var result = db.ExecuteWithoutStatus("USP_DeleteUser", new { ID });
            return result.Success;
        }
        public  int UserViews(string LinkServerName,string DBName,string SelectedOption)
        {
            int userView = 0;
            string responsemessage = "";
            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("USP_GetLiveUserCount", new { LinkServerName, DBName, SelectedOption }, reader =>
            {
                userView = reader.Read<int>().ToList().First();
                responsemessage = "success";
            });

          
            return userView;
        }
        public Tuple<List<PageModel>, string> GetPageList()
        {
            List<PageModel> PageList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            var result = db.QueryWithoutStatus("USP_GetPageList", null, reader =>
            {
                PageList = reader.Read<PageModel>().ToList();
                responsemessage = "success";
            });

            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                PageList,
                responsemessage
            );

        }

        public Tuple<List<UserPermissionModel>, string> GetUserPermissionList(int userid)
        {
            List<UserPermissionModel> PermissionList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            var result = db.QueryWithoutStatus("USP_GetUserPermissionList", new { userid }, reader =>
            {
                PermissionList = reader.Read<UserPermissionModel>().ToList();
                responsemessage = "success";
            });

            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                PermissionList,
                responsemessage
            );

        }

        public string AddUpdateUserPermission(UserPermissionModel obj)
        {
            var db = new DapperHelper(connStr);
            var result = db.ExecuteWithoutStatus("USP_INUPUserPermission", new { obj.UserId, obj.PageIds });
            return result.Success;
        }


        public Tuple<List<WithdrawalRequestModel>,double, string> GetWithdrawalRequestList(string LinkServerName, string DBName,String Status,DateTime FromDate,DateTime ToDate,string Offset)
        {
            List<WithdrawalRequestModel> WithdrawalRequestList = null;
            string responsemessage = string.Empty;
            double total = 0.0;
            var db = new DapperHelper(connStr);

            if ((LinkServerName == null || DBName == null))
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportWithdrawalRequestList", null, reader =>
                {
                    WithdrawalRequestList = reader.Read<WithdrawalRequestModel>().ToList();
                    total = reader.Read<double>().ToList().FirstOrDefault();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    WithdrawalRequestList,
                    total,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportWithdrawalRequestList_V1", new { LinkServerName, DBName,FromDate,ToDate,Status,Offset }, reader =>
                {
                    WithdrawalRequestList = reader.Read<WithdrawalRequestModel>().ToList();
                    total = reader.Read<double>().ToList().FirstOrDefault();
                    responsemessage = "success";
                });

                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    WithdrawalRequestList,
                    total,
                    responsemessage
                );
            }
        }
    }
}
