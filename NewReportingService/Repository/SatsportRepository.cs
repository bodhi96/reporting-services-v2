﻿using NewReportingService.Helper;
using NewReportingService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Repository
{
    public class SatsportRepository
    {
        private readonly string connStr;

        public SatsportRepository(string connStr)
        {
            this.connStr = connStr;
        }
        public Tuple<List<SatSportMarketModel>, string> GetSatSportMarketList(string LinkServerName, string DBName, string SportID, string TournamentID, string MatchID, string IsFancy, string FromDate, string ToDate)
        {
            List<SatSportMarketModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportMarketList", null, reader =>
                {
                    SatSportMarketList = reader.Read<SatSportMarketModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                if (IsFancy == "ALL")
                {
                    IsFancy = "NULL";
                }
                var result = db.QueryWithoutStatus("USP_GetSatSportMarketList", new { LinkServerName, DBName, SportID, TournamentID, MatchID, IsFancy, FromDate, ToDate }, reader =>
                {
                    SatSportMarketList = reader.Read<SatSportMarketModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }
        public Tuple<List<SportsModel>, string> GetSatSportSportList(string LinkServerName, string DBName)
        {
            List<SportsModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportSportsList", null, reader =>
                {
                    SatSportMarketList = reader.Read<SportsModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportSportsList", new { LinkServerName, DBName }, reader =>
                {
                    SatSportMarketList = reader.Read<SportsModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }



        public Tuple<List<SatSportBetModel>, string> GetSatSportBetList(string LinkServerName, string DBName, string MarketID, string Settled, string FromDate, string ToDate, string Offset, string SportID, string TournamentID, string MatchID, string IsFancy, string SelectedOption)
        {
            List<SatSportBetModel> SatSportBetList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportBetList_V1", null, reader =>
                {
                    SatSportBetList = reader.Read<SatSportBetModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportBetList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportBetList_V2", new { LinkServerName, DBName, Settled, MarketID, FromDate, ToDate, Offset, SportID, TournamentID, MatchID, IsFancy, SelectedOption }, reader =>
                {
                    SatSportBetList = reader.Read<SatSportBetModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportBetList,
                    responsemessage
                );
            }
        }

        public Tuple<List<SatSportBetListModel>, string> GetSatSportBetListALL(int MarketID, string Offset, string SportID, string TournamentID, string MatchID, string IsFancy)
        {
            List<SatSportBetListModel> SatSportBetList = null;
            string responsemessage = string.Empty;
            if (IsFancy == "ALL")
            {
                IsFancy = "NULL";
            }
            var db = new DapperHelper(connStr);


            var result = db.QueryWithoutStatus("USP_GetSatSportBetList_ALL_V1", new { MarketID, Offset, SportID, TournamentID, MatchID, IsFancy, }, reader =>
            {
                SatSportBetList = reader.Read<SatSportBetListModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportBetList,
                responsemessage
            );

        }

        public Tuple<List<SatSportMarketModel>, string> GetSatSportMarketListALL(string SportID, string TournamentID, string MatchID, string IsFancy, DateTime FromDate, DateTime ToDate)
        {
            List<SatSportMarketModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (IsFancy == "ALL")
            {
                IsFancy = "NULL";
            }

            var result = db.QueryWithoutStatus("USP_GetSatSportMarketList_notLive_ALL", new { SportID, TournamentID, MatchID, IsFancy, FromDate, ToDate }, reader =>
            {
                SatSportMarketList = reader.Read<SatSportMarketModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportMarketList,
                responsemessage
            );

        }
        public Tuple<List<TournamentModel>, string> GetSatSportBfTournamentList(DateTime FromDate, DateTime ToDate, string LinkServerName, string DBName, string sportsID, string term)
        {
            List<TournamentModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportBFTounamentList", null, reader =>
                {
                    SatSportMarketList = reader.Read<TournamentModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportTounamentList", new { LinkServerName, DBName, sportsID, FromDate, ToDate }, reader =>
                {
                    SatSportMarketList = reader.Read<TournamentModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }

        public Tuple<List<TournamentModel>, string> GetSatSportTournamentList(DateTime FromDate, DateTime ToDate, string LinkServerName, string DBName, string sportsID, string term)
        {
            List<TournamentModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportTounamentList", null, reader =>
                {
                    SatSportMarketList = reader.Read<TournamentModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportTounamentList", new { LinkServerName, DBName, sportsID, FromDate, ToDate }, reader =>
                {
                    SatSportMarketList = reader.Read<TournamentModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }
        public Tuple<List<TournamentModel>, string> GetSatSportTournamentList(DateTime FromDate, DateTime ToDate, string sportsId)
        {
            List<TournamentModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);


            var result = db.QueryWithoutStatus("USP_GetSatSportTounamentListALL", new { sportsId, FromDate, ToDate }, reader =>
            {
                SatSportMarketList = reader.Read<TournamentModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportMarketList,
                responsemessage
            );

        }
        public Tuple<List<MatchModel>, string> GetSatSportMatchList(DateTime FromDate, DateTime ToDate, string tournamentId)
        {
            List<MatchModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);


            var result = db.QueryWithoutStatus("USP_GetSatSportMatchListALL", new { FromDate, ToDate, tournamentId }, reader =>
            {
                SatSportMarketList = reader.Read<MatchModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportMarketList,
                responsemessage
            );

        }
        public Tuple<List<SportsModel>, string> GetSatSportSportList()
        {
            List<SportsModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            var result = db.QueryWithoutStatus("USP_GetSatSportSportsListALL", null, reader =>
            {
                SatSportMarketList = reader.Read<SportsModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportMarketList,
                responsemessage
            );

        }
        public Tuple<List<MatchModel>, string> GetSatSportBfMatchList(DateTime FromDate, DateTime ToDate, string LinkServerName, string DBName, string tournamentId)
        {
            List<MatchModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportBfMatchList", null, reader =>
                {
                    SatSportMarketList = reader.Read<MatchModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportMatchList", new { FromDate, ToDate, LinkServerName, DBName, tournamentId }, reader =>
                {
                    SatSportMarketList = reader.Read<MatchModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }

        public Tuple<List<MatchModel>, string> GetSatSportMatchList(DateTime FromDate, DateTime ToDate, string LinkServerName, string DBName, string tournamentId)
        {
            List<MatchModel> SatSportMarketList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);

            if (LinkServerName == null || DBName == null)
            {

                var result = db.QueryWithoutStatus("USP_GetSatSportMatchList", null, reader =>
                {
                    SatSportMarketList = reader.Read<MatchModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
            else
            {
                var result = db.QueryWithoutStatus("USP_GetSatSportMatchList", new { FromDate, ToDate, LinkServerName, DBName, tournamentId }, reader =>
                {
                    SatSportMarketList = reader.Read<MatchModel>().ToList();
                    responsemessage = "success";
                });
                responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
                return Tuple.Create(
                    SatSportMarketList,
                    responsemessage
                );
            }
        }

        public Tuple<List<FraudDetectionClass>, string> GetFraudList(int appSiteID, string MarketID, string appStartDate, string appEndDate, string Offset, int appBFSportID, int appBFTournamentID, int appBFMatchID, int appBFCentralID)
        {
            List<FraudDetectionClass> SatSportPlatformList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("Report_AllBets", new { appSiteID, appStartDate, appEndDate, appBFSportID, appBFTournamentID, appBFMatchID, appBFCentralID }, reader =>
            {
                SatSportPlatformList = reader.Read<FraudDetectionClass>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportPlatformList,
                responsemessage
            );
        }

        public Tuple<List<FraudDetail>, string> GetFraudDetail(int appCheatBetID)
        {

            List<FraudDetail> fraudDetail = null;

            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("Report_CheatingBet_V1", new { appCheatBetID }, reader =>
            {

                fraudDetail = reader.Read<FraudDetail>().ToList();

                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(

                fraudDetail,

                responsemessage
            );
        }
        public Tuple<List<SatSportPlatformModel>, string> GetSatSportPlatformList()
        {
            List<SatSportPlatformModel> SatSportPlatformList = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            var result = db.QueryWithoutStatus("USP_GetSatSportPlatformList", null, reader =>
            {
                SatSportPlatformList = reader.Read<SatSportPlatformModel>().ToList();
                responsemessage = "success";
            });
            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                SatSportPlatformList,
                responsemessage
            );
        }
    }
}
