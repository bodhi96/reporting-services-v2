﻿using NewReportingService.Helper;
using NewReportingService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewReportingService.Repository
{
    public class PaymentGateWayRepository
    {

        private readonly string connStr;

        public PaymentGateWayRepository(string connStr)
        {
            this.connStr = connStr;
        }

        public Tuple<List<PaymentGatewayDetailModel>, string> GetDailyReport( DateTime StartDate, DateTime EndDate)
        {
            List<PaymentGatewayDetailModel> DailyReportList = null;
          //  DailyReportMeta Daily = null;
            string responsemessage = string.Empty;

            var db = new DapperHelper(connStr);
            //  LinkServerName = "LNK_SATSPORT247";
            //  DBName = "Satsport247_Live";
            var result = db.QueryWithoutStatus("Get_PaymentGatewayDetails", new {  StartDate, EndDate }, reader =>
            {
               // Daily = reader.Read<DailyReportMeta>().FirstOrDefault();
                DailyReportList = reader.Read<PaymentGatewayDetailModel>().ToList();


                responsemessage = "success";
            });




            responsemessage = string.IsNullOrEmpty(result.Success) ? responsemessage : result.Success;
            return Tuple.Create(
                  
                DailyReportList,


                responsemessage
            );


        }


    }
}
